Installation
============

Executer le fichier jar  
Connecter les machines au même réseau wifi  
Pour le serveur, récupérer l'adresse IP de sa machine  
  * sous linux et mac, ouvrir un terminal et taper **ifconfig**.
  * sous windows, ouvrir une invite de commande (Windows+R, taper **cmd**), puis taper **ipconfig /all**
Pour les machines clientes, indiquer l'ip du serveur.  
C'est tout!
