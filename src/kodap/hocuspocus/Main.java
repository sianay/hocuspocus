package kodap.hocuspocus;

import kodap.hocuspocus.tools.WindowsContainer;

/**
 * The Main class start the program by launching the first window
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
        new WindowsContainer();
        WindowsContainer.get("ChoiceWindow").setVisible(true);
    }
}