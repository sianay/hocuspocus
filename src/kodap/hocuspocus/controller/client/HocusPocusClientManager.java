package kodap.hocuspocus.controller.client;


import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.container.Spellbook;
import kodap.hocuspocus.model.cards.hocus.HocusCard;
import kodap.hocuspocus.model.cards.pocus.PocusCard;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.listener.MouseCardListener;
import kodap.hocuspocus.view.window.BoardWindow;
import kodap.hocuspocus.view.window.ChoiceTargetedCards;

/**
 * The HocusPocusClientManager class
 * private
 * @author Vlad <vlad.marin@icloud.com>
 * @version 1.0
 */
public class HocusPocusClientManager {

    /**
     * The client board
     */
    public static Board board = ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();

    /**
     * The player playing a card
     */
    public static Player sourcePlayer = null;

    /**
     * whether or not the hocus player must be change
     */
    public static boolean changeTheHocusPlayer = false;

    /**
     * The list of targetedCards
     */
    public static List<Card> targetedCards = new ArrayList<Card>();

    /**
     * This method places a card in the appropriate zone and removing it from its initial location.
     * It removes the card from the Player's Hand or Spellbook.
     * 
     * @param sourcePlayer
     * @param destinationPlayer
     * @param sourceCard
     * @param destinationCard
     * @param sourceZone
     * @param destinationZone
     * @param targetedPlayer
     */
    public static void placeCard(Player sourcePlayer, Player destinationPlayer, Card sourceCard, Card destinationCard, Zone sourceZone, Zone destinationZone, String targetedPlayer) {

        int tempPositionSource      = -10;
        int tempPositionDestination = -10;
        Card tempCardSource         = null;
        Card tempCardDestination    = null;
        HocusPocusClientManager.sourcePlayer = sourcePlayer;
       
        switch (destinationZone) {
            case GAMEZONE_HOCUS:
                board.getGameZone().addCard(board.getPlayer(sourcePlayer.getPseudo()).removeCard(sourceCard));
                addAction(sourcePlayer, destinationPlayer, sourceCard, destinationCard, sourceZone, destinationZone, targetedPlayer);
                Client.sendMessage("freeNoPocusButton");
                break;
            case GAMEZONE_POCUS:
                board.getGameZone().addCard(board.getPlayer(sourcePlayer.getPseudo()).removeCard(sourceCard));
                addAction(sourcePlayer, destinationPlayer, sourceCard, destinationCard, sourceZone, destinationZone, targetedPlayer);
                HocusPocusClientManager.pocusWasPlaced();
                break;
            case SPELLBOOK:
                // Do not authorize card exchange in the same cathegory.
                if (sourceZone.equals(Zone.SPELLBOOK)) {
                    break;
                }
                // If there is a free space left in the Spellbook.
                if (destinationCard == null) {
                    board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().add(board.getPlayer(sourcePlayer.getPseudo()).getHand().removeCardById(sourceCard.getId()));
                    break;
                }
                // Get the initial positions of the cards.
                for (int i = 0; i < board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().size(); i++) {
                    if (board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().get(i).getId() == sourceCard.getId())
                        tempPositionSource = i;
                }                
                for (int i = 0; i < board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().size(); i++) {
                    if (board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().get(i).getId() == destinationCard.getId())
                        tempPositionDestination = i;
                }
                // Remove the cards from both zones - not the most elegant.
                tempCardSource      = board.getPlayer(sourcePlayer.getPseudo()).getHand().removeCardById(sourceCard.getId());
                tempCardDestination = board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().removeCardById(destinationCard.getId());
                // Add the cards to both zone, switching positions.
                board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().add(tempPositionDestination, tempCardSource);
                board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().add(tempPositionSource, tempCardDestination);
                break;
            case HAND:
                // Do not authorize card exchange in the same category.
                if (sourceZone.equals(Zone.HAND)) {
                    break;
                }
                // If there is a free space left in the Hand.
                if (destinationCard == null) {
                    board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().add(board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().removeCardById(sourceCard.getId()));
                    break;
                }
                // Get the initial positions of the cards.
                for (int i = 0; i < board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().size(); i++) {
                    if (board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().get(i).getId() == sourceCard.getId())
                        tempPositionSource = i;
                }
                for (int i = 0; i < board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().size(); i++) {
                    if (board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().get(i).getId() == destinationCard.getId())
                        tempPositionDestination = i;
                }
                // Remove the cards from both zones - not the most elegant.
                tempCardSource      = board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().removeCardById(sourceCard.getId());
                tempCardDestination = board.getPlayer(sourcePlayer.getPseudo()).getHand().removeCardById(destinationCard.getId());
                // Add the cards to both zone, switching positions.
                board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().add(tempPositionDestination, tempCardSource);
                board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().add(tempPositionSource, tempCardDestination);
                break;
            default:
                break;
        }
        // Notifying the Server.
        Client.sendModel(board);
        // Update the chat history.
        updateChatHistory(sourceCard, destinationCard, sourceZone, destinationZone);
    }

    /**
     * Removes all the Pocus cards in the GameZone and plac(action.getTargetZone().equals(Zone.LIBRARY))es them in the Graveyard. 
     */
    private static void throwGameZonePocusCardsToGraveyard() {
        while (board.getGameZone().getNumberOfPocusCards() > 0){
            board.getGraveyard().addCard(board.getGameZone().removeTopPocusCard());
        }
    }

    /**
     * Removes the Hocus card in the GameZone and place it in the Graveyard. 
     */
    private static void throwGameZoneHocusCardToGraveyard() {
        board.getGraveyard().addCard(board.getGameZone().removeHocusCard());
    }

    /**
     * Removes all cards in the game zone and place it in the Graveyard. 
     */
    private static void throwAllGameZoneCardsToGraveyard() {
         HocusPocusClientManager.throwGameZonePocusCardsToGraveyard();
         HocusPocusClientManager.throwGameZoneHocusCardToGraveyard();
    }

    /**
     * Adds the action and update the last one if it exists
     *
     * @param player
     * @param card
     * @param source
     * @param destination
     */
    private static void addAction(Player sourcePlayer, Player destinationPlayer, Card sourceCard, Card destinationCard, Zone sourceZone, Zone destinationZone, String targetedPlayer) {
        if (sourceCard instanceof HocusCard) {
            if (board.getOriginalAction() == null) {
                board.setOriginalAction(((HocusCard) sourceCard).createAction(
                    sourceCard.getId(),
                    ((HocusCard) sourceCard).getPowerNumber(),
                    sourcePlayer.getPseudo(),
                    "_NO_"
                ));
                System.out.println(targetedPlayer);
                if (targetedPlayer != null) {
                    board.getOriginalAction().setTargetPlayer(targetedPlayer);
                }
                board.setEvaluatedAction(((HocusCard) sourceCard).createAction(
                    sourceCard.getId(),
                    ((HocusCard) sourceCard).getPowerNumber(),
                    sourcePlayer.getPseudo(),
                    "_NO_"
                ));
                if (targetedPlayer != null) {
                    board.getEvaluatedAction().setTargetPlayer(targetedPlayer);
                }
                return;
            }
            System.err.println("Original action shouldn't be null here!");
        }
        if (sourceCard instanceof PocusCard) {
            // Update the evaluated Action
            ((PocusCard) sourceCard).UpdateAction(board.getEvaluatedAction(), sourcePlayer.getPseudo());
            //EXCEPTION FOR ECLAIR : all pocus can't work
            
            if (!board.getEvaluatedAction().isImmunized()) {
            // If the played Pocus Card ends the current Pocus tour (Amulette for example)
            if (board.getEvaluatedAction().isEndPocusTurn()) {
                
                // --------------------
                // Contre-Sort
                if ((sourceCard.getId() == 62) || (sourceCard.getId() == 63)) {
                    HocusPocusClientManager.throwAllGameZoneCardsToGraveyard();
                    clearActions();
                    MouseCardListener.targetedPlayer = null;
                    Client.sendModel(board);
                    //Client.sendMessage("disableNoPocusButton");
                    return;
                }
                // ChatNoir
                if ((sourceCard.getId() == 66) || (sourceCard.getId() == 67)) {
                    HocusPocusClientManager.board.getPlayer(HocusPocusClientManager.sourcePlayer.getPseudo()).getHand().addCard(board.getGameZone().removeHocusCard());
                    HocusPocusClientManager.throwGameZonePocusCardsToGraveyard();
                    clearActions();
                    MouseCardListener.targetedPlayer = null;
                    Client.sendModel(board);
                    //Client.sendMessage("disableNoPocusButton");
                    return;
                }
                // Citrouille
                if ((sourceCard.getId() == 59) || (sourceCard.getId() == 60) || (sourceCard.getId() == 61)) {
                    HocusPocusClientManager.board.getPlayingPlayer().getHand().addCard(board.getGameZone().removeHocusCard());
                    HocusPocusClientManager.throwGameZonePocusCardsToGraveyard();
                    clearActions();
                    MouseCardListener.targetedPlayer = null;
                    Client.sendModel(board);
                    //Client.sendMessage("disableNoPocusButton");
                    return;
                }

                // --------------------
                evaluateCards();
                return;
            }
            // If the played Pocus Card ends the current Hocus tour.
            if (board.getEvaluatedAction().isEndHocusTurn()) {
                if ((sourceCard.getId() == 64) || (sourceCard.getId() == 65)) {
                    HocusPocusClientManager.board.setChangeTheHocusPlayer(true);
                    Client.sendModel(board);
                    //Client.sendMessage("disableNoPocusButton");
                    return;
                }
            }
           }
            else //if immmunized
            {
                //To take the last power number
                int numberBecauseOfBaguetteMagique = board.getEvaluatedAction().getNumber();
                Action originalAction = board.getOriginalAction();
                originalAction.setNumber(numberBecauseOfBaguetteMagique);
                originalAction.setImmunized(true);
                board.setEvaluatedAction(originalAction);
            
                Client.sendModel(board);
                
                //if card is not Baguette or Eclair : evaluate !
                if ((sourceCard.getId() != 52) && (sourceCard.getId() != 55) &&
                        (sourceCard.getId() != 69) && (sourceCard.getId() != 68)) {
                    evaluateCards();
                    return;    
                }
            }

        }
    }

    /**
     * Evaluate the cards in the game zone at the end of a "sub round"
     */
    public static void evaluateCards() {
        Client.sendMessage("addMessageInChat: Evaluation de la carte Hocus et distribution des gains");
        Action action = board.getEvaluatedAction();
        if (action == null) {
            return;
        }        
        
        /*if (MouseCardListener.targetedPlayer != null) {
            action.setTargetPlayer(MouseCardListener.targetedPlayer.getPseudo());
        }*/
        HocusPocusClientManager.throwAllGameZoneCardsToGraveyard();
        /*if (!evaluatedAction.getFinalDestinationCardPlayed().equals("NO")) {
            switch (evaluatedAction.getFinalDestinationCardPlayed()) {
                case GAMEZONE_HOCUS:
                    board.getGameZone().addCard(board.getCardById(evaluatedAction.getCardPlayed()));
                    break;
                case GAMEZONE_POCUS:
                    board.getGameZone().addCard(board.getCardById(evaluatedAction.getCardPlayed()));
                    break;
                case GRAVEYARD:
                    board.getGraveyard().addCard(board.getCardById(evaluatedAction.getCardPlayed()));
                    break;
                default:
                    break;
            }
        } else {
            
        }*/
        if (action.getActionVerb().equals(ActionVerb.NONE)) {
            // do nothing
        } else if (action.getActionVerb().equals(ActionVerb.CHANGE_ORDER)) { // BOULE DE CRISTAL
            changeLibraryTopCardsOrder();
        } else if (action.getActionVerb().equals(ActionVerb.TAKE)) {
            int k = 0, j = 0, l = 0;
            for (int i = 0; i < action.getNumber(); i++) {
                // distribute gains
                switch (action.getTargetZone()) {
                case LIBRARY: // INSPIRATION
                    if (board.getPlayer(action.getDestinationContainerPlayer()).getHand().getCardList().size() < 5) {
                        board.getPlayer(action.getDestinationContainerPlayer()).getHand().addCard(board.getLibrary().removeTopCard());
                    } else {
                        k++;
                        if (k==1) {
                            Tools.displayErrorWindow("Vous n'avez plus de place dans votre main");
                        }
                    }
                    break;
                case CAULDRON: // SORTILEGE
                    if (action.getTargetPlayer().equals("_NO_")) {
                        if (board.getCauldron().getSize() == 0) {
                            j++;
                            if (j==1) {
                                Tools.displayErrorWindow("Vous ne pouvez plus prendre de gemmes dans le chaudron car il n'y en a plus");
                            }
                        } else {
                            board.getPlayer(action.getDestinationContainerPlayer()).getCauldron().addGem(board.getCauldron().removeOneGem());
                        }
                    } else if (action.getDestinationContainerPlayer() == null) { // SACRIFICE
                        if (board.getPlayer(action.getTargetPlayer()).getCauldron().getSize() == 0) {
                            j++;
                            if (j==1) {
                                Tools.displayErrorWindow("Vous ne pouvez plus prendre de gemmes à "+ action.getTargetPlayer() +" car il n'en a plus");
                            }
                        } else {
                            board.getCauldron().addGem(
                                board.getPlayer(action.getTargetPlayer()).getCauldron().removeOneGem());
                        }
                    } else { // VOLEUR
                        if (board.getPlayer(action.getTargetPlayer()).getCauldron().getSize() == 0) {
                            j++;
                            if (j==1) {
                                Tools.displayErrorWindow("Vous ne pouvez plus enlever de gemmes à "+ action.getTargetPlayer() +" car il n'en a plus");
                            }
                        } else {
                        board.getPlayer(action.getPlayerSource()).getCauldron().addGem(
                            board.getPlayer(action.getTargetPlayer()).getCauldron().removeOneGem());
                        }
                    }
                    break;
                case SPELLBOOK:
                    if (!action.getDestinationContainerPlayer().equals("_NO_")) { // HIBOU
                        if (board.getPlayer(action.getDestinationContainerPlayer()).getHand().getCardList().size() < 5) {
                            board.getPlayer(action.getDestinationContainerPlayer()).getHand().addCard(targetedCards.get(i));
                            board.getPlayer(action.getTargetPlayer()).getSpellbook().removeCardById(targetedCards.get(i).getId());
                        } else {
                            l++;
                            if (l==1) {
                                Tools.displayErrorWindow("Vous n'avez plus de place dans votre main");
                            }
                        }
                    } else { // MALEDICTION
                        board.getGraveyard().addCard(targetedCards.get(0));
                        board.getPlayer(action.getTargetPlayer()).getSpellbook().removeCardById(targetedCards.get(i).getId());
                    }
                    break;
                default:
                    break;
                }
            }
        } else if (action.getActionVerb().equals(ActionVerb.EXCHANGE)) {
            if (action.getTargetPlayer().equals("_ALL_")) { // VORTEX
                int nbPlayers = board.getPlayerList().size();
                int nbCartesP1 = board.getPlayerList().get(0).getSpellbook().getCardList().size();
                int nbCartes = board.getPlayerList().get(nbPlayers-1).getSpellbook().getCardList().size();
                List<Card> tempCardList = new ArrayList<Card>();
                //P1 to temp
                for (int i = 0; i < nbCartesP1; i++) {
                    tempCardList.add(board.getPlayerList().get(0).getSpellbook().getCardList().remove(0));
                }
                
                //P4 to P1
                for (int i = 0; i < nbCartes; i++) {
                    board.getPlayerList().get(0).getSpellbook().addCard
                        (
                                board.getPlayerList().get(nbPlayers-1).getSpellbook().getCardList().remove(0)
                        );
                }         
                
                if (nbPlayers > 2) {
                    //P3 to P4
                    nbCartes = board.getPlayerList().get(nbPlayers-2).getSpellbook().getCardList().size();
                    for (int i = 0; i < nbCartes; i++) {
                        board.getPlayerList().get(nbPlayers-1).getSpellbook().getCardList().add
                            (
                                    board.getPlayerList().get(nbPlayers-2).getSpellbook().getCardList().remove(0)
                            );
                    }        
                    //P2 to P3
                    if (nbPlayers == 4) {
                        nbCartes = board.getPlayerList().get(1).getSpellbook().getCardList().size();
                        for (int i = 0; i < nbCartes; i++) {
                            board.getPlayerList().get(nbPlayers-2).getSpellbook().getCardList().add
                                (
                                        board.getPlayerList().get(1).getSpellbook().getCardList().remove(0)
                                );
                        }        
                    }
                }
                //Temp to P2
                for (int i = 0; i < nbCartesP1; i++) {
                    board.getPlayerList().get(1).getSpellbook().addCard
                        (
                                tempCardList.remove(0)
                        );
                }                    
                
                
            } else { // ABRACADABRA
                List<Card> tempCardList = new ArrayList<Card>();
                // store targeted player cards
                int size = board.getPlayer(action.getTargetPlayer()).getHand().getCardList().size();
                for (int i = 0; i < size; i++) {
                    tempCardList.add(board.getPlayer(action.getTargetPlayer()).getHand().getCardList().remove(0));
                }
                // give player cards to targeted player
                size = board.getPlayer(action.getDestinationContainerPlayer()).getHand().getCardList().size();
                for (int i = 0; i < size; i++) {
                    board.getPlayer(action.getTargetPlayer()).getHand().addCard(
                        board.getPlayer(action.getDestinationContainerPlayer()).getHand().getCardList().remove(0)
                    );
                }
                size = tempCardList.size();
                // give targeted player cards to player
                for (int i = 0; i < size ; i++) {
                    board.getPlayer(action.getDestinationContainerPlayer()).getHand().addCard(
                        tempCardList.remove(0)
                    );
                }
            }
        }
        clearActions();
        MouseCardListener.targetedPlayer = null;
        System.out.println("EVALUATION DONE");
        Client.sendModel(board);
        //Client.sendMessage("freeNoPocusButton");
    }

    private static boolean endGame() {
        System.out.println(board.getCauldron().getSize());
        if (board.getCauldron().getSize() == 0) {
            return true;
        }
        return false;
    }

    private static String getWinner() {
        String winner = "unknown";
        int max = 0;
        for (Player player : board.getPlayerList()) {
            if (player.getCauldron().getSize() > max) {
                max = player.getCauldron().getSize();
                winner = player.getPseudo();
            }
        }

        return winner;
    }

    private static void clearActions() {
        board.setOriginalAction(null);
        board.setEvaluatedAction(null);
    }

    /**
     * Sends a customized string to the IHM in order to update the chat history with the placement of a new Card.
     * @param card
     * @param cardSource
     * @param cardDestination
     */
    private static void updateChatHistory(Card sourceCard, Card destinationCard, Zone sourceZone, Zone destinationZone) {
        if (sourceCard != null) {
            // A card has been played. A corresponding message is broadcasted.
            //Define the player targeted
            switch (destinationZone) {
                case GAMEZONE_HOCUS:  
                    Client.sendMessage("addMessageInChat: a placé une carte Hocus "+ sourceCard.toString() + " de " + sourceZone.toString() + " vers " + destinationZone.toString());
                    break;
                case GAMEZONE_POCUS:  
                    Client.sendMessage("addMessageInChat: a placé une carte Pocus " + sourceCard.toString() + " de " + sourceZone.toString() + " vers " + destinationZone.toString());
                    break;
                case SPELLBOOK:
                    if (destinationCard == null){
                        Client.sendMessage("addMessageInChat: a place une carte de " + sourceZone.toString() + " vers " + destinationZone.toString());
                    }
                    else{
                        Client.sendMessage("addMessageInChat: a échangé une carte entre " + sourceZone.toString() + " et " + destinationZone.toString());
                    }
                    break;
                case HAND:
                    if (destinationCard == null){
                        Client.sendMessage("addMessageInChat: a place une carte de " + sourceZone.toString() + " vers " + destinationZone.toString());
                    }
                    else{
                        Client.sendMessage("addMessageInChat: a échangé une carte entre " + sourceZone.toString() + " et " + destinationZone.toString());
                    }
                    break;
                default:
                    Client.sendMessage("addMessageInChat: ERROR: The system couldn't update the chat history.");
                    break;
            }
        }
    }

    /**
     * Tell the Server a Pocus was placed so every client can play a Pocus Card.
     */
    public static void pocusWasPlaced() {
        // Tell all clients to free the button noPocusButton when a Pocus Card is placed.
        Client.sendMessage("freeNoPocusButton");
    }    /**
     * Signals that the current PlayinPlayer does not want to continue playing a new Hocus Card for a new tour.
     * Notifies the Server to propose the Player with the choice of picking two new Cards or a new Gem.
     * @param sourcePlayer (the Player who pressed the button)
     */

    /**
     * Sets the Hocus Player to the next Player in the game.
     * Updates all the other Players, notifies the Server and updates the chat.
     */
    public static void changePlayingPlayer() {
        board.setPlayingPlayerMustBeChanged(true);
        // Notify the Server.
        Client.sendModel(board);
    }
    
    /**
     * Display a window to choose between gem or card
     */
    public static void chooseGemsOrCards() {

        if (board.getPlayingPlayer().getSpellbook().getNumberOfCards()<Spellbook.SPELLBOOK_NUMBER 
                && board.getPlayingPlayer().getHand().getNumberOfCards()!=0){
            Tools.displayInfoWindow("Veuillez completer votre grimoire avant de terminer votre tour");
        }
        else{
            WindowsContainer.get("GemOrCardsWindow").setVisible(true);
            WindowsContainer.get("BoardWindow").setEnabled(false);  
        }
    }

    /**
     * Display a window to change the library top cards order
     */
    public static void changeLibraryTopCardsOrder() {
        WindowsContainer.get("MoveLibraryCardsWindow").setVisible(true);
        WindowsContainer.get("BoardWindow").setEnabled(false);
    }

    /**
     * Display a window to choose the target player
     */
    public static void chooseTargetedPlayer() {
        WindowsContainer.get("ChoiceTargetedPlayer").setVisible(true);
        WindowsContainer.get("BoardWindow").setEnabled(false);
    }

    /**
     * Display a window to choose the target player
     * 
     * @param cardList
     * @param power
     */
    public static void chooseTargetedCards(List<Card> cardList, int power) {
        System.out.println("in");
        new ChoiceTargetedCards(cardList, power).setVisible(true);
        WindowsContainer.get("BoardWindow").setEnabled(false);
    }

    /**
     * Update the chat with the name of the target
     * @param playerPseudo
     */
    public static void sendPlayerTargetedToChat(String playerPseudo){
        String target = playerPseudo;
        Client.sendMessage("addMessageInChat: la cible est "+target);
    }

    /**
     * Update the chat with the name of the targeted cards
     * 
     * @param cardList
     */
    public static void sendCardsTargetedToChat(List<Card> cardList){
        String cards = "";
        int i = 0;
        for (Card card : cardList) {
            i++;
            if (i == cardList.size()) {
                cards += card.toString();
            } else {
                cards += card.toString() + ", ";
            }
        }
        if (cardList.size() > 1)
            Client.sendMessage("addMessageInChat: les cartes ciblées sont "+cards);
        else
            Client.sendMessage("addMessageInChat: la carte ciblée est "+cards);
    }

    /**
     * Signals that the current PlayinPlayer does not want to continue playing a new Hocus Card for a new tour.
     * Notifies the Server to propose the Player with the choice of picking two new Cards or a new Gem.
     * @param sourcePlayer (the Player who pressed the button)
     */
    public static void endOfTour(Player sourcePlayer) {
        
        // Reactivate Hocus Cards in the Player's Hand.
        for (int i = 0; i < board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().size(); i++) {
            if (board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().get(i) instanceof HocusCard) {
                board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().get(i).setCardEnabled(true);
            }
        }
        // Reactivate Hocus Cards in the Player's Spellbook.
        for (int j = 0; j < board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().size(); j++) {
            if (board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().get(j) instanceof HocusCard) {
                board.getPlayer(sourcePlayer.getPseudo()).getSpellbook().getCardList().get(j).setCardEnabled(true);
            }
        }
        // Reset the Board's pocusCardsHaveBeenReseted to false - it's a new tour.
        board.setPocusCardsToBeReseted(false);
        // The Server is not notified here by the action. It will be notified in the pickAGemOrTwoCards method.
    }

    /**
     * Following the PlayingPlayer's choice, the method gives him One Gem or One/Two Cards.
     * @param sourcePlayer
     * @param choiceNumber (1 = One Gem; 2 = Two Cards)
     */
    public static void pickAGemOrTwoCards(Player sourcePlayer, int choiceNumber) {

        // If the player who pressed the button is not the current PlayinPlayer.
        if (!board.getPlayer(sourcePlayer.getPseudo()).equals(board.getPlayingPlayer())) {
            return;
        }
        // Adds a new Gem to the Player's Cauldron.
        if (choiceNumber == 1) {
            board.getPlayer(sourcePlayer.getPseudo()).getCauldron().addGem(board.getCauldron().removeOneGem());
            Client.sendMessage("addMessageInChat: prend une gemme du chaudron ");
        } 
        // Adds one or two new Cards to the Player's Hand, only if he has less than 4 or 5 cards in his Hand.
        if (choiceNumber == 2) {
            if (board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().size() < 5) {
                board.getPlayer(sourcePlayer.getPseudo()).getHand().addCard(board.getLibrary().removeTopCard());
                if (board.getPlayer(sourcePlayer.getPseudo()).getHand().getCardList().size() < 5) {
                    board.getPlayer(sourcePlayer.getPseudo()).getHand().addCard(board.getLibrary().removeTopCard());
                    Client.sendMessage("addMessageInChat: prend deux cartes de la bibliothèque ");
                }  else {
                        Tools.displayInfoWindow("Vous ne pouvez prendre qu'une seule carte car vous avez déjà 4 cartes sur 5");
                        Client.sendMessage("addMessageInChat: prend une carte de la bibliothèque ");
                    }
            } else {
                Tools.displayInfoWindow("Vous ne pouvez pas prendre de cartes car votre main est deja complète");
            }
            
        }
        // Update the chat.
        Client.sendMessage("addMessageInChat: termine son tour ");
        if (endGame()) {
            Client.sendMessage("endGame:"+getWinner());
        }
        // Changes the current PlayingPlayer. The Server is notified with the changes inside this method.
        changePlayingPlayer();
    }
    
}