package kodap.hocuspocus.controller.client;

import java.util.Vector;

import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.network.server.Server;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.panel.BottomPlayerPanel;
import kodap.hocuspocus.view.window.BoardWindow;
import kodap.hocuspocus.view.window.LaunchGameWindow;

/**
 * The ObjectReceiver class contains functions which know what to do when the
 * client receive an object (A message like a string, or an object like the
 * board)
 * 
 * Every actions message from the server will trigger a method ending by
 * 'Action'. For example, Server.sendMessage("methodName:string",
 * MessageType.ACTION, String ClientPseudo) will trigger the following method :
 * public static void methodNameAction(String string) { ... } Errors method can
 * also be triggered if needed.
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class ObjectReceiver {

    /**
     * Update the player list on JoinServerWindow
     * 
     * @param list
     */
    public static void updatePlayerListAction(String list) {
        String messageParts[] = list.split("/");
        Vector<String> col = new Vector<String>();
        col.add("Joueurs prêt à commencer la partie");
        Vector<Vector<String>> data = new Vector<Vector<String>>();
        for (int i = 0; i < messageParts.length; i++) {
            Vector<String> clientVector = new Vector<String>();
            clientVector.add(messageParts[i]);
            data.add(clientVector);
        }
        WindowsContainer.hideAllWindows();
        LaunchGameWindow launchGameWindow = (LaunchGameWindow) WindowsContainer.get("LaunchGameWindow");
        launchGameWindow.setTitle("Hocus Pocus - Rejoindre une partie : " + Client.getPseudo());
        launchGameWindow.getTableModel().setDataVector(data, col);
        launchGameWindow.setVisible(true);
    }

    /**
     * Send the client pseudo to the server
     */
    public static void sendPseudoAction() {
        Client.sendMessage("pseudo_answer#" + Client.getPseudo());
    }

    /**
     * Launch the boardWindow for the first time
     * 
     * @param board
     */
    public static void initBoardWindow(Board board) {
        HocusPocusClientManager.board = board;
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        boardWindow.init(board);
        WindowsContainer.hideAllWindows();
        boardWindow.setVisible(true);
    }

    /**
     * Update the boardWindow
     * 
     * @param board
     */
    synchronized public static void updateBoardWindow(Board board) {
        HocusPocusClientManager.board =  board;
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        boardWindow.update(board);
    }

    /**
     * Disconnect the client on server Closed. This function is a consequence of
     * the blocking behavior of the accept() method in the
     * ConnectionListenerThread. (the first client to try to connect is
     * connected even if the server was closed before)
     * 
     * @param message
     */
    public static void serverClosedError(String message) {
        Client.disconnect();
    }

    /**
     * Add a message in the chat
     * 
     * @param message
     */
    @SuppressWarnings("unchecked")
    synchronized public static void updateChatAction(String message) {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        boardWindow.getHistoryPanel().getHistoryListModel().addElement(message);
        int lastIndex = boardWindow.getHistoryPanel().getHistoryListModel().getSize() - 1;
        if (lastIndex >= 0) {
            boardWindow.getHistoryPanel().getHistoryList().ensureIndexIsVisible(lastIndex);
        }
    }

    /**
     * Close the server and go back to the choiceWindow if a client deconnected
     * 
     * @param clientPseudo
     */
    public static void deconnectedClientAction(String clientPseudo) {
        if (WindowsContainer.contains("BoardWindow")) {
            if (Client.isHost) {
                Server.connectionListener.close();
            }
            Tools.displayErrorWindow(clientPseudo + " s'est deconnecte, la partie est finie.");
            WindowsContainer.hideAllWindows();
            WindowsContainer.get("ChoiceWindow").setVisible(true);
        }
    }

    /**
     * Evaluate the history pile
     */
    public static void evaluateCardsAction() {
        HocusPocusClientManager.evaluateCards();
    }

    /**
     * Enable the noPocusButton
     */
    public static void freeNoPocusButtonAction() {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        boardWindow.getBottomPlayerPanel().enableButton(BottomPlayerPanel.NO_POCUS, true);
    }
    
}
