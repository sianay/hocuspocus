package kodap.hocuspocus.controller.server;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.network.server.ServerThread;
import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.container.Hand;
import kodap.hocuspocus.model.cards.container.Spellbook;
import kodap.hocuspocus.model.gems.Cauldron;
import kodap.hocuspocus.network.server.MessageType;
import kodap.hocuspocus.network.server.Server;
import kodap.hocuspocus.tools.WindowsContainer;

/**
 * Every actions message from clients will trigger a method ending by 'Action'.
 * For example, Client.sendMessage("methodName:string") will trigger the
 * following method : public static void methodNameAction(String string, String
 * clientPseudo) { ... }.
 * 
 * @author Vlad        <vlad.marin@icloud.com>
 * @author Baptiste       <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class HocusPocusServerManager {

    /**
     * The board in its current state
     */
    public static Board board;
    private static int numberOfPlayers;
    private static int numberOfPlayersDecliningToPlayPocus;

    // CONSTRUCTOR
    /**
     * Default class constructor.
     * 
     * @param playerNumber
     * @param pseudoList
     */
    public HocusPocusServerManager(int playerNumber, List<String> pseudoList) {
        HocusPocusServerManager.numberOfPlayers = playerNumber;
        HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus = 0;
        this.initializeBoard();
        this.setPlayers(pseudoList);
        this.distributeCards();
    }

    // GETTERS
    /**
     * Returns the Game's Board.
     * 
     * @return board
     */
    public Board getBoard() {
        return HocusPocusServerManager.board;
    }

    /**
     * Returns the number of Players in the game.
     * 
     * @return numberOfPlayers
     */
    public int getNumberOfPlayers() {
        return HocusPocusServerManager.numberOfPlayers;
    }

    // METHODS
    /**
     * Initializes the Board.
     */
    public void initializeBoard() {
        HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus = 0;
        HocusPocusServerManager.board = new Board(HocusPocusServerManager.numberOfPlayers);
    }

    /**
     * This method creates all the Players based on the pseudos they have
     * indicated.
     */
    private void setPlayers(List<String> pseudoList) {
        Collections.shuffle(pseudoList);
        for (int i = 0; i < getNumberOfPlayers(); i++) {
            this.getBoard()
                    .getPlayerList()
                    .add(i, new Player(pseudoList.get(i), new Cauldron(),
                            new Spellbook(), new Hand()));
        }
        HocusPocusServerManager.board.getPlayer(pseudoList.get(0)).setIsPlayingHocus(true);
    }

    /**
     * Distributes 6 cards to each Player: 3 for the Hand and 3 for the
     * Spellbook.
     */
    public void distributeCards() {
        getBoard().getLibrary().shuffle();
        for (int i = 0; i < getNumberOfPlayers(); i++) {
            for (int j = 0; j < 3; j++) {
                getBoard().getPlayerList().get(i).getHand()
                        .addCard(getBoard().getLibrary().removeTopCard());
            }
        }
        for (int i = 0; i < getNumberOfPlayers(); i++) {
            for (int j = 0; j < 3; j++) {
                getBoard().getPlayerList().get(i).getSpellbook()
                        .addCard(getBoard().getLibrary().removeTopCard());
            }
        }
    }

    /**
     * Add a message in the client chats when an action was done
     * 
     * @param string
     * @param clientPseudo
     */
    public static void addMessageInChatAction(String string, String clientPseudo)
    {
        Server.sendMessageToAll("updateChat:"+clientPseudo+" -> "+string, MessageType.ACTION);
    }

    /**
     * A player send a message saying he does not want to place a pocus card
     * 
     * @param playerPseudo
     */
    public static void declinePocusAction(String playerPseudo)
    {
        HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus++;
        Server.sendMessageToAll("updateChat:"+playerPseudo+" -> "+"ne souhaite plus jouer de Pocus sur la Hocus en cours", MessageType.ACTION);
        if (HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus == HocusPocusServerManager.numberOfPlayers) {
           // we send a message to the playing player so he evaluate the cards
           Server.sendMessage("evaluateCards", MessageType.ACTION, HocusPocusServerManager.board.getPlayingPlayer().getPseudo());
           HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus = 0;
           //Server.sendMessageToAll("freeNoPocusButton", MessageType.ACTION);
           // Sablier
           if (HocusPocusClientManager.board.getChangeTheHocusPlayer()) {
        	   HocusPocusClientManager.board.setChangeTheHocusPlayer(false);
	           HocusPocusServerManager.changePlayingPlayer();
	           Client.sendModel(board);
	            WindowsContainer.get("GemOrCardsWindow").setVisible(true);
	            WindowsContainer.get("BoardWindow").setEnabled(false);  
               return;
           }
        }
    }

    /**
     * Tell clients to enable the noPocus button
     * 
     * @param clientPseudo
     */
    public static void freeNoPocusButtonAction(String clientPseudo) {
        HocusPocusServerManager.numberOfPlayersDecliningToPlayPocus = 0;
        Server.sendMessageToAll("freeNoPocusButton", MessageType.ACTION);
    }
    

    /**
     * End of the game, winner announcement
     * 
     * @param winner
     * @param clientPseudo
     */
    public static void endGameAction(String winner, String clientPseudo) {
    	System.out.println("ok");
        Server.sendMessageToAll("La partie est terminée, "+winner+" a gagné!", MessageType.INFO);
        for (ServerThread t : Server.serverThreads) {
            try {
                t.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Change the playing player
     */
    public static void changePlayingPlayer() {
        board.setNextPlayingPlayer();
        Server.sendMessageToAll("updateChat:C'est au tour de "+board.getPlayingPlayer().getPseudo()+" de jouer !", MessageType.ACTION);
    }

    /**
     * Send the board to all client. If needed change the playing player before
     * 
     * @param board
     */
    public static void handleBoard(Board board) {
        HocusPocusServerManager.board = board;
        if (HocusPocusServerManager.board.getPlayingPlayerMustBeChanged()) {
            HocusPocusServerManager.board.setPlayingPlayerMustBeChanged(false);
            HocusPocusServerManager.changePlayingPlayer();
        }
        Server.sendModelToAll(board);
    }
}
