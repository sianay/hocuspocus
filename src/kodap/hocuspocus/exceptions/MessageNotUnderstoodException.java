package kodap.hocuspocus.exceptions;

/**
 * The PortAlreadyUsedException class is used to throw an exception when a port
 * is already used.
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
@SuppressWarnings("serial")
public class MessageNotUnderstoodException extends Exception {
    /**
     * Constructor
     */
    public MessageNotUnderstoodException() {
        super();
    }

    /**
     * Constructor
     * 
     * @param message
     */
    public MessageNotUnderstoodException(String message) {
        super("The message " + message + " couldn't be handled");
    }
}
