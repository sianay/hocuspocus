package kodap.hocuspocus.exceptions;

/**
 * The PortAlreadyUsedException class is used to throw an exception when a port
 * is already used.
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PortAlreadyUsedException extends Exception {
    /**
     * Constructor
     */
    public PortAlreadyUsedException() {
        super();
    }

    /**
     * Constructor
     * 
     * @param port
     */
    public PortAlreadyUsedException(int port) {
        super("The port " + port + " is already used by another application");
    }

    /**
     * Constructor
     * 
     * @param message
     */
    public PortAlreadyUsedException(String message) {
        super(message);
    }
}
