package kodap.hocuspocus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.container.GameZone;
import kodap.hocuspocus.model.cards.container.Graveyard;
import kodap.hocuspocus.model.cards.container.Library;
import kodap.hocuspocus.model.gems.Cauldron;

/**
 * The board class
 * 
 * @author baptiste <baptiste.bouchereau@gmail.com>
 */
public class Board implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3488407021462733953L;

    private Cauldron cauldron;
    private Library library;
    private Graveyard graveyard;
    private GameZone gameZone;
    private List<Player> playerList;
    private int nbPlayersNotPlayingPocus = 0;
    private Action originalAction = null;
    private Action evaluatedAction = null;
    private Action lastAction = null;
    private boolean pocusCardsHaveBeenReseted = false;
    private boolean somePocusCardsHaveBeenReseted = false;
    private boolean playingPlayerMustBeChanged = false;
    private boolean changeTheHocusPlayer = false;


    /**
     * Constructor
     * 
     * @param playerNumber
     */
    public Board(int playerNumber) {
        this.cauldron = new Cauldron(playerNumber);
        this.library = new Library(playerNumber);
        this.graveyard = new Graveyard();
        this.gameZone = new GameZone();
        this.playerList = new ArrayList<Player>();
    }

    /**
     * Returns the original Action implied by the first Hocus card played in the current tour.
     * @return originalHocus (Action)originalAction
     */
    public Action getOriginalAction() {
        return this.originalAction;
    }

    /**
     * Sets a new Action for the original Hocus Card played in the current tour.
     * @param originalAction (Action)
     */
    public void setOriginalAction(Action originalAction) {
        this.originalAction = originalAction;
    }

    /**
     * Returns the evaluatedAction in the current tour.
     * @return evaluatedAction (Action)
     */
    public Action getEvaluatedAction() {
        return this.evaluatedAction;
    }

    /**
     * Returns the last Action (last pocus card played)
     * @return lastAction (Action)
     */
    public Action getLastAction() {
        return this.lastAction;
    }

    /**
     * Set the last Action
     * @param lastAction (Action)
     */
    public void setLastAction(Action lastAction) {
        this.lastAction = lastAction;
    }
   
    /**
     * Sets a new evaluatedAction for the current evaluatedAction in the tour.
     * @param evaluatedAction (Action)
     */
    public void setEvaluatedAction(Action evaluatedAction) {
        this.evaluatedAction = evaluatedAction;
    }

    /**
     * Tells if a Pocus Card that has been played has the property to reset all others Pocus Cards.
     * @return boolean
     */
    public boolean havePocusCardsBeenReseted() {
        return this.pocusCardsHaveBeenReseted;
    }

    /**
     * Indicate if a played Pocus Card will reset all others, except a special few.
     * @param b (boolean)
     */
    public void setPocusCardsToBeReseted(boolean b) {
        this.pocusCardsHaveBeenReseted = b;
    }

    /**
     * Tells if a Pocus Card that has been played has the property to reset some other Pocus Cards.
     * @return boolean
     */
    public boolean haveSomePocusCardsBeenReseted() {
        return this.somePocusCardsHaveBeenReseted;
    }

    /**
     * Indicate if a played Pocus Card will reset some other Pocus Cards.
     * @param b (boolean)
     */
    public void setSomePocusCardsToBeReseted(boolean b) {
        this.somePocusCardsHaveBeenReseted = b;
    }

    /**
     * Get cauldron
     * 
     * @return cauldron Caudron
     */
    public Cauldron getCauldron() {
        return cauldron;
    }

    /**
     * Set cauldron
     * 
     * @param cauldron
     */
    public void setCauldron(Cauldron cauldron) {
        this.cauldron = cauldron;
    }

    /**
     * Get library
     * 
     * @return library Library
     */
    public Library getLibrary() {
        return library;
    }

    /**
     * Set library
     * 
     * @param library
     */
    public void setLibrary(Library library) {
        this.library = library;
    }

    /**
     * Get graveyard
     * 
     * @return graveyard Graveyard
     */
    public Graveyard getGraveyard() {
        return graveyard;
    }

    /**
     * Set graveyard
     * 
     * @param graveyard
     */
    public void setGraveyard(Graveyard graveyard) {
        this.graveyard = graveyard;
    }

    /**
     * Get gameZone
     * 
     * @return gameZone GameZone
     */
    public GameZone getGameZone() {
        return gameZone;
    }

    /**
     * Set gameZone
     * 
     * @param gameZone
     */
    public void setGameZone(GameZone gameZone) {
        this.gameZone = gameZone;
    }

    /**
     * Get playerList
     * 
     * @return playerList List<Player>
     */
    public List<Player> getPlayerList() {
        return playerList;
    }

    /**
     * Set playerList
     * 
     * @param playerList
     */
    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    /**
     * Get a player from his pseudo
     * 
     * @param pseudo : the pseudo of the player
     * @return player : a Player
     */
    public Player getPlayer(String pseudo) {
        for (Player player : this.playerList) {
            if (player.getPseudo().equals(pseudo)) {
                return player;
            }
        }
        return null;
    }

    /**
     * Get the player who is currently playing the Hocus card.
     * 
     * @return playerList
     */
    public Player getPlayingPlayer() {
        for (Player player : this.playerList) {
            if (player.isPlayingHocus()) {
                return player;
            }
        }
        return null;
    }

    /**
     * Get all the players ( = pocus) except the current player ( = hocus)
     * 
     * @return player : a Player
     */
    public List<Player> getPlayersPocus() {
        List<Player> players = new ArrayList<Player>();
        for (Player player : this.playerList) {
            if (!player.isPlayingHocus()) {
                players.add(player);
            }
        }
        return players;
    }

    /**
     * Set the next playing player
     */
    public void setNextPlayingPlayer()
    {
        int i = 0;
        int j = -1;
        for (i = 0; i < this.playerList.size(); i++) {
            if (this.playerList.get(i).isPlayingHocus()) {
                this.playerList.get(i).setIsPlayingHocus(false);
                j = i;
            }
        }
        if (j == this.playerList.size()-1) {
            this.playerList.get(0).setIsPlayingHocus(true);
        } else {
            this.playerList.get(j+1).setIsPlayingHocus(true);
        }
    }

    /**
     * Get the card by the id in the visible zones
     * @param idCard
     * @return Card
     */
    public Card getCardById(int idCard){
        for (Player player : this.getPlayerList()) {
            //SPELLBOOK
            for (Card spellBookCard : player.getSpellbook().getCardList()) {
                if (spellBookCard.getId() == idCard) {
                    return spellBookCard;
                }
            }
            //HAND
            for (Card handCard : player.getHand().getCardList()) {
                if (handCard.getId() == idCard) {
                    return handCard;
                }
            }
        }
        //GAMEZONE
        if (this.getGameZone().getHocusCard().getId() == idCard)//HocusCard
            return this.getGameZone().getHocusCard();
        for (Card gameZoneCard : this.getGameZone().getPocusCardList()) {//PocusCards
            if (gameZoneCard.getId() == idCard) {
                    return gameZoneCard;
            }
        }

        return null;
    }

    /**
     * Returns the number of Players that refused to play a Pocus card.
     * @return int
     */
    public int getNbPlayersNotPlayingPocus() {
        return this.nbPlayersNotPlayingPocus;
    }

    /**
     * Increment the number of Players that refused to play a Pocus card.
     */
    public void incrementNbPlayersNotPlayingPocus() {
        this.nbPlayersNotPlayingPocus++;
    }


    /**
     * @return the playingPlayerMustBeChanged
     */
    public boolean getPlayingPlayerMustBeChanged() {
        return playingPlayerMustBeChanged;
    }


    /**
     * @param playingPlayerMustBeChanged the playingPlayerMustBeChanged to set
     */
    public void setPlayingPlayerMustBeChanged(boolean playingPlayerMustBeChanged) {
        this.playingPlayerMustBeChanged = playingPlayerMustBeChanged;
    }

	/**
	 * Get change the hocus player
	 * 
	 * @return changeTheHocusPlayer
	 */
	public boolean getChangeTheHocusPlayer() {
		return changeTheHocusPlayer;
	}

	/**
	 * Set change the hocus player
	 * 
	 * @param changeTheHocusPlayer
	 */
	public void setChangeTheHocusPlayer(boolean changeTheHocusPlayer) {
		this.changeTheHocusPlayer = changeTheHocusPlayer;
	}
}
