package kodap.hocuspocus.model;

import java.io.Serializable;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.container.Hand;
import kodap.hocuspocus.model.cards.container.Spellbook;
import kodap.hocuspocus.model.gems.Cauldron;

/**
 * The Player Class.
 * 
 * This class will represent the Players of the game.
 * 
 * @author Vlad     <vlad.marin@icloud.com>
 * @version 1.0
 */
public class Player implements Serializable {
    // ATTRIBUTES
    private String pseudo;
    private Cauldron cauldron;
    private Spellbook spellbook;
    private Hand hand;
    private boolean isPlayingHocus;
    private boolean wantToPlayPocus;
    private static final long serialVersionUID = -3512094628918865285L;

    // CONSTRUCTOR
    /**
     * Default class constructor.
     */
    public Player() {
        this.setPseudo("this.getClass().hashCode()");
        this.cauldron = new Cauldron();
        this.spellbook = new Spellbook();
        this.hand = new Hand();
        this.setIsPlayingHocus(false);
        this.setWantToPlayPocus(true);
    }

    /**
     * Full class constructor.
     * @param playerPseudo
     * @param playerGems
     * @param playerSpellbook
     * @param playerHand
     */
    public Player(String playerPseudo, Cauldron playerGems,
            Spellbook playerSpellbook, Hand playerHand) {
        this.setPseudo(playerPseudo);
        this.setCauldron(playerGems);
        this.setSpellbook(playerSpellbook);
        this.setHand(playerHand);
    }

    // GETTERS
    /**
     * Returns the unique Player's ID or name.
     * @return the playerID
     */
    public String getPseudo() {
        return this.pseudo;
    }

    /**
     * Returns the Player's Cauldron of Gems.
     * @return the playerGems of Cauldron type
     */
    public Cauldron getCauldron() {
        return this.cauldron;
    }

    /**
     * Returns the Player's Card collection from the Spellbook.
     * @return the playerSpellbook
     */
    public Spellbook getSpellbook() {
        return this.spellbook;
    }

    /**
     * Return the Player's Card collection from his/her hand.
     * @return the playerHand
     */
    public Hand getHand() {
        return this.hand;
    }

    // SETTERS
    /**
     * Sets the Player's Pseudo usign a String passed as parameter.
     * @param string
     */
    public void setPseudo(String string) {
        this.pseudo = string;
    }

    /**
     * Replaces the Player's Gem collection. Deletes the old one. This method
     * requires a Cauldron to be passed as parameter.
     * @param playerGems
     *            the playerGems to set
     */
    public void setCauldron(Cauldron playerGems) {
        this.cauldron = playerGems;
    }

    /**
     * Replaces the Player's Card collection from the Spellbook. This method
     * requires a Spellbook to be passed as parameter.
     * @param playerSpellbook
     *            the playerSpellsBook to set
     */
    public void setSpellbook(Spellbook playerSpellbook) {
        // this.getPlayerSpellbook().clear();
        this.spellbook = playerSpellbook;
    }

    /**
     * Replaces the Player's Card collection from the Spellbook. This method
     * requires to be passed as parameter.
     * @param playerHand the playerHand to set
     */
    public void setHand(Hand playerHand) {
        // this.getPlayerHand().clear();
        this.hand = playerHand;
    }

    // METHODS
    /**
     * This method searches for a Card inside the Players Hand or Spellbook and removes it.
     * @param card
     * @return card
     */
    public Card removeCard(Card card) {
        if (getSpellbook().getCardById(card.getId()) != null) {
            return getSpellbook().removeCardById(card.getId());
        } else {
            return getHand().removeCardById(card.getId());
        }
    }

    /**
     * This method tell whether or not the Player wishes to play a Pocus card.
     * @return boolean
     */
    public boolean hasDeclinedToPlayPocus() {
        return this.wantToPlayPocus;
    }

    /**
     * This method sets wantToPlayPocus to true.
     * @param b
     */
    public void setWantToPlayPocus(boolean b) {
        this.wantToPlayPocus = b;
    }

    /**
     * Check if this Player is playin the current Hocus card.
     * @return boolean
     */
    public boolean isPlayingHocus() {
        return this.isPlayingHocus;
    }

    /**
     * Set the Hocus card playing state of the Player.
     * @param b
     */
    public void setIsPlayingHocus(boolean b) {
        this.isPlayingHocus = b;
    }

    @Override
    /**
     * Returns a string containing the name of the Player.
     * @rturn String
     */
    public String toString() {
        return this.getPseudo();
    }
}
