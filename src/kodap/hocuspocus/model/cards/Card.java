package kodap.hocuspocus.model.cards;

import java.io.Serializable;

/**
 * The Card class
 * 
 * A card is an abstract object which provides all specific card
 * characteristics.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public abstract class Card implements Serializable {
    /**
     * Attributes
     */
    private static final long serialVersionUID = 8879843763212010833L;
    private int id;
    private String description;
    private String image;
    private boolean isCardEnabled;

    /**
     * Constructor Card
     * 
     * @param id
     */
    public Card(int id) {
        this.setIdCard(id);
        this.isCardEnabled = true;
    }

    /**
     * Getter ID
     * 
     * @return id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Getter Description
     * 
     * @return Description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Getter Image
     * 
     * @return Image
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Set image
     * 
     * @param image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Set description
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Set id
     * 
     * @param id
     */
    public void setIdCard(int id) {
        this.id = id;
    }
    
    
    @Override
    /**
     * Returns the name of the card (e.g. Citrouille).
     * @return String
     */
    public String toString() {
        return this.getClass().getSimpleName()/* +" "+ Integer.toString(this.id)*/;
    }

    /**
     * @return boolean
     */
    public boolean isCardEnabled() {
        return isCardEnabled;
    }

    /**
     * @param isCardEnabled
     */
    public void setCardEnabled(boolean isCardEnabled) {
        this.isCardEnabled = isCardEnabled;
    }
}
