package kodap.hocuspocus.model.cards.actions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.cards.Card;

/**
 * @author Francois Modified by Jorys
 * 
 *         BE CAREFUL : if the action is NONE, nothing must be done
 * 
 */
public class Action implements Serializable, Cloneable {

private static final long serialVersionUID = -6920645861588235981L;

    //ATTRIBUTS

    // Play context
    private String playerSource;                            //Player who play the card
    private int cardPlayedId;                                 //Card which is played
    private String targetPlayer;                            //Specify if the target is a player (_NO_, _ALL_ or Player name)
    private Zone targetZone;                                    //if player:      player.cauldron, player.hand, player.spellbook
                                                            //if non player: cauldron, library, GraveHard
    private ObjectType typeTargeted;                        //Type of object: Gem or Card
    private String finalDestinationCardPlayedPlayer;        //Specify if the card played is finally placed in player game (_NO_, _ALL_ or Player name)
    private Zone finalDestinationCardPlayed;                //if player:      player.cauldron, player.hand, player.spellbook
                                                            //if non player: cauldron, library, GraveHard
    private boolean endPocusTurn;                           //If a special card is played, the turn must be finished
    private boolean endHocusTurn;                           //If a special card is played, the turn must be finished
    private boolean resetAction;                            //A card cancel all Pocus powers
    
    // Action
    private List<Card> listCardsToDisable = new ArrayList<Card>();        //List all cards must be disabled
    
    private String sourceContainerPlayer;       //Specify if the source container is a player (_NO_, _ALL_ or Player name) 
    private Zone sourceContainer;               //if player:      player.cauldron, player.hand, player.spellbook
                                                //if non player: cauldron, library, GraveHard
    private int number;                         //Characteristic number of the card
    private String destinationContainerPlayer;  //Specify if the destination container is a player (_NO_, _ALL_ or Player name) 
    private Zone destinationContainer;          //if player:      player.cauldron, player.hand, player.spellbook
                                                //if non player: cauldron, library, GraveHard
    private ActionVerb actionVerb;                  //Action to make: NONE, Exchange, changeOrder, Take

    private boolean immunized;
    
    
    //CONSTRUCTOR
    /**
     * Default Class constructor
     * @param playerSource
     * @param cardPlayed
     * @param targetPlayer
     * @param target
     * @param typeTargeted
     * @param finalDestinationCardPlayedPlayer 
     * @param finalDestinationCardPlayed
     * @param endPocusTurn 
     * @param endHocusTurn 
     * @param resetAction 
     * @param listCardToDisable 
     * @param sourceContainerPlayer
     * @param sourceContainer
     * @param number
     * @param destinationContainerPlayer
     * @param destinationContainer
     * @param action
     */
    public Action(String playerSource, int cardPlayed, String targetPlayer, Zone target, ObjectType typeTargeted,
        String finalDestinationCardPlayedPlayer, Zone finalDestinationCardPlayed, boolean endPocusTurn,
        boolean endHocusTurn, boolean resetAction, List<Card> listCardToDisable, String sourceContainerPlayer,
        Zone sourceContainer, int number, String destinationContainerPlayer, Zone destinationContainer, ActionVerb action)
    {
        this.playerSource = playerSource;
        this.cardPlayedId = cardPlayed;
        this.targetPlayer = targetPlayer;
        this.targetZone = target;
        this.typeTargeted = typeTargeted;
        this.setFinalDestinationCardPlayedPlayer(finalDestinationCardPlayedPlayer);
        this.finalDestinationCardPlayed = finalDestinationCardPlayed;
        this.endPocusTurn= endPocusTurn;
        this.endHocusTurn= endHocusTurn;
        this.setResetAction(resetAction);
        this.listCardsToDisable = listCardToDisable;
        this.sourceContainerPlayer = sourceContainerPlayer;
        this.sourceContainer = sourceContainer;
        this.number = number;
        this.destinationContainerPlayer = destinationContainerPlayer;
        this.destinationContainer = destinationContainer;
        this.actionVerb = action;
        this.immunized = false;
    }



    //METHODS
    // Getters & setters
    /**
     * @return playerSource
     */
    public String getPlayerSource() {
        return this.playerSource;
    }

    /**
     * @param playerSource
     */
    public void setPlayerSource(String playerSource) {
        this.playerSource = playerSource;
    }

    /**
     * @return cardPlayed
     */
    public int getCardPlayed() {
        return this.cardPlayedId;
    }

    /**
     * @param cardPlayed
     */
    public void setCardPlayed(int cardPlayed) {
        this.cardPlayedId = cardPlayed; 
    }

    /**
     * @return targetPlayer
     */
    public String getTargetPlayer() {
        return this.targetPlayer;
    }

    /**
     * @param targetPlayer
     */
    public void setTargetPlayer(String targetPlayer) {
        this.targetPlayer = targetPlayer;
    }

    /**
     * @return target
     */
    public Zone getTargetZone() {
        return this.targetZone;
    }

    /**
     * @param target
     */
    public void setTargetZone(Zone target) {
        this.targetZone = target;
    }

    /**
     * @return typeTargeted
     */
    public ObjectType getTypeTargeted() {
        return this.typeTargeted;
    }

    /**
     * @param typeTargeted
     */
    public void setTypeTargeted(ObjectType typeTargeted) {
        this.typeTargeted = typeTargeted;
    }

    /**
     * @return finalDestinationCardPlayed
     */
    public Zone getFinalDestinationCardPlayed() {
        return this.finalDestinationCardPlayed;
    }

    /**
     * @param finalDestinationCardPlayed
     */
    public void setFinalDestinationCardPlayed(Zone finalDestinationCardPlayed) {
        this.finalDestinationCardPlayed = finalDestinationCardPlayed;
    }

    /**
     * @return sourceContainerPlayer
     */
    public String getSourceContainerPlayer() {
        return this.sourceContainerPlayer;
    }

    /**
     * @param sourceContainerPlayer
     */
    public void setSourceContainerPlayer(String sourceContainerPlayer) {
        this.sourceContainerPlayer = sourceContainerPlayer;
    }

    /**
     * @return sourceContainerTarget
     */
    public Zone getSourceContainer() {
        return this.sourceContainer;
    }

    /**
     * @param sourceContainer
     */
    public void setSourceContainer(Zone sourceContainer) {
        this.sourceContainer = sourceContainer;
    }

    /**
     * @return number
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * @param number
     */
    public void setNumber(int number) {
        this.number = number;
    } 

    /**
     * @return destinationContainerPlayer
     */
    public String getDestinationContainerPlayer() {
        return this.destinationContainerPlayer;
    }

    /**
     * @param destinationContainerPlayer
     */
    public void setDestinationContainerPlayer(String destinationContainerPlayer) {
        this.destinationContainerPlayer = destinationContainerPlayer;
    }

    /**
     * @return destinationContainerTarget
     */
    public Zone getDestinationContainer() {
        return this.destinationContainer;
    }

    /**
     * @param destinationContainer
     */
    public void setDestinationContainer(Zone destinationContainer) {
        this.destinationContainer = destinationContainer;
    }

    /**
     * @return action
     */
    public ActionVerb getActionVerb() {
        return this.actionVerb;
    }

    /**
     * @param action
     */
    public void setActionVerb(ActionVerb action) {
        this.actionVerb = action;
    }

    /**
     * @return serialVersionUID
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return finalDestinationCardPlayedPlayer
     */
    public String getFinalDestinationCardPlayedPlayer() {
        return this.finalDestinationCardPlayedPlayer;
    }

    /**
     * @param finalDestinationCardPlayedPlayer
     */
    public void setFinalDestinationCardPlayedPlayer(
            String finalDestinationCardPlayedPlayer) {
        this.finalDestinationCardPlayedPlayer = finalDestinationCardPlayedPlayer;
    }

    /**
     * @return endPocusTurn
     */
    public boolean isEndPocusTurn() {
        return this.endPocusTurn;			//private List<Card> listCardsToDisable = new ArrayList<Card>();        //List all cards must be disabled

    }

    /**
     * @param endPocusTurn
     */
    public void setEndPocusTurn(boolean endPocusTurn) {
        this.endPocusTurn = endPocusTurn;
    }

    /**
     * @return endHocusTurn
     */
    public boolean isEndHocusTurn() {
        return this.endHocusTurn;
    }

    /**			//private List<Card> listCardsToDisable = new ArrayList<Card>();        //List all cards must be disabled

     * @param endHocusTurn
     */
    public void setEndHocusTurn(boolean endHocusTurn) {
        this.endHocusTurn = endHocusTurn;
    }

    /**
     * @return resetAction
     */
    public boolean isResetAction() {
        return this.resetAction;
    }

    /**
     * @param resetAction
     */
    public void setResetAction(boolean resetAction) {
        this.resetAction = resetAction;
    }

    /**
     * @return list<Card>
     */
    public List<Card> getListCardsToDisable() {
        return this.listCardsToDisable;
    }

    /**
     * @param listCardsToDisable
     */
    public void setListCardsToDisable(List<Card> listCardsToDisable) {
        this.listCardsToDisable = listCardsToDisable;
    }

    @Override
    public Action clone() {
        return new Action(this.destinationContainerPlayer, this.cardPlayedId, this.destinationContainerPlayer, 
            this.destinationContainer, this.typeTargeted, this.destinationContainerPlayer, 
            this.destinationContainer, this.endHocusTurn, this.endHocusTurn, this.endHocusTurn, 
            this.listCardsToDisable, this.destinationContainerPlayer, this.destinationContainer, 
            this.cardPlayedId, this.destinationContainerPlayer, this.destinationContainer, this.actionVerb);
    }
    
    
    public String toString() {
        return
            "-------------------" + "\n" +
            "    Player source : " + this.playerSource + "\n" +
            "    Target player : " + this.targetPlayer + "\n" +
            "    Card played : " + HocusPocusClientManager.board.getCardById(this.cardPlayedId) + "\n" +
            "    Power : " + this.number + "\n" +
            "    Action verb : " + this.actionVerb + "\n" +
            "    Target Zone : " + this.targetZone + "\n" +
            "    Type targeted : " + this.typeTargeted + "\n" +
            "    Destination container player : " + this.destinationContainerPlayer + "\n" +
            "    Destination container : " + this.destinationContainer + "\n" +
            "    Source container player : " + this.sourceContainerPlayer + "\n" +
            "    source container : " + this.sourceContainer + "\n" +
            "    End hocus turn : " + this.endHocusTurn + "\n" +
            "    End pocus turn : " + this.endPocusTurn + "\n" +
            "    Final destination card played player : " + this.finalDestinationCardPlayedPlayer + "\n" +
            "    Reset action : " + this.resetAction
            //private List<Card> listCardsToDisable = new ArrayList<Card>();        //List all cards must be disabled
        ;
    }



	/**
	 * @return the immunized
	 */
	public boolean isImmunized() {
		return immunized;
	}



	/**
	 * @param immunized the immunized to set
	 */
	public void setImmunized(boolean immunized) {
		this.immunized = immunized;
	}
}





