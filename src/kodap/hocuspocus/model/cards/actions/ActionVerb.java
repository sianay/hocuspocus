package kodap.hocuspocus.model.cards.actions;

/**
 * ActionVerb enum :
 * Actions cards : specify all actions doable.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public enum ActionVerb {
    /** Exchange */
    EXCHANGE,
    /** Change Order */
    CHANGE_ORDER,
    /** Take */
    TAKE,
    /** Non active */
    NONE;

    @Override
    public String toString() {
      switch(this) {
        case EXCHANGE: return "exchange";
        case CHANGE_ORDER: return "change_order";
        case TAKE: return "take";
        case NONE: return "none";
        default: throw new IllegalArgumentException();
      }
    }
}