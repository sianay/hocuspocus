package kodap.hocuspocus.model.cards.actions;

/**
 * ObjectType enum :
 * Actions cards : specify all types of objects.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public enum ObjectType {
    /** Card */
    CARD,
    /** Gem */
    GEM;

    @Override
    public String toString() {
      switch(this) {
        case GEM: return "gem";
        case CARD: return "card";
        default: throw new IllegalArgumentException();
      }
    }
}