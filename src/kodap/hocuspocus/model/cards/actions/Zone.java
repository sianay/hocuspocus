package kodap.hocuspocus.model.cards.actions;

/**
 * Zone enum :
 * 
 * Actions cards : specify all types of zones.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public enum Zone {
    /** Cauldron */
    CAULDRON {
        public String toString() {
            return "le chaudron";
        }
    },
    /** GraveGuard */
    GRAVEYARD{
        public String toString() {
            return "le cimetière";
        }
    } ,
    /** Library */
    LIBRARY{
        public String toString() {
            return "la librairie";
        }
    },
    /** Gamezone Hocus */
    GAMEZONE_HOCUS{
        public String toString() {
            return "la zone de jeu hocus";
        }
    },
    /** Gamezone Pocus */
    GAMEZONE_POCUS{
        public String toString() {
            return "la zone de jeu pocus";
        }
    }, 
    /** Gamezone All the Pocus */
    /** Hand */
    HAND {
        public String toString() {
            return "sa main";
        }
    },
    /** SpellBook */
    SPELLBOOK{
        public String toString() {
            return "son grimoire";
        }
    }
}
