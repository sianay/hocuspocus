package kodap.hocuspocus.model.cards.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.hocus.HocusCard;

/**
 * The GameZone class contains the current turn cards
 * 
 * @author Anaïs Payet
 * @version 1.0
 */
public class GameZone implements ICardsContainerBehaviour, Serializable {

    private static final long serialVersionUID = -8381399057405546635L;
    private List<Card> pocusCardList;
    private Card hocusCard;

    /**
     * Constructor
     */
    public GameZone() {
        this.pocusCardList = new ArrayList<Card>();
    }

    /**
     * Add a card to the GameZone and put it to the good container (Hocus or
     * Pocus)
     * 
     * @return false is has already an Hocus Card in the GameZone
     */
    @Override
    public Boolean addCard(Card card) {
        if (card instanceof HocusCard) {
            if (this.hocusCard == null) {
                this.hocusCard = card;
                return true;
            } else
                return false;
        } else {
            return this.pocusCardList.add(card);
        }
    }

    @Override
    public Boolean addCards(ArrayList<Card> cards) {
        return this.pocusCardList.addAll(cards);
    }

    /**
     * Check whether or not the game zone contains at least one card
     * 
     * @return true or false
     */
    public Boolean containsCards() {
        if (this.pocusCardList.isEmpty() && this.hocusCard == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Call to clean the GameZone after Hocus card is resolved The Hocus card is
     * the last in the List
     */
    @Override
    public List<Card> removeAllCards() {
        List<Card> liste = new ArrayList<Card>(this.pocusCardList);
        liste.add(this.hocusCard);
        this.pocusCardList.clear();
        this.hocusCard = null;
        return liste;
    }

    @Override
    public Card getCardById(int id) {
        for (Card card : this.pocusCardList)
            if (card.getId() == id)
                return card;

        return null;
    }

    /**
     * Number of pocus card
     * 
     * @return number of pocus
     */
    public int getNumberOfPocusCards() {
        return this.pocusCardList.size();
    }

    public int getNumberOfCards() {
        return this.pocusCardList.size();
    }
    
    /**
     * Get Pocus card list
     * 
     * @return cardList
     */
    public List<Card> getPocusCardList() {
        return pocusCardList;
    }

    /**
     * Current hocus card played on the gamezone
     * 
     * @return Hocus card if has one, null otherwhise
     */
    public Card getHocusCard() {
        return this.hocusCard;
    }

    /**
     * The last pocus card who has been placed in the gamezone
     * 
     * @return the card of the top pocus
     */
    public Card getTopPocusCard() {
        if (this.pocusCardList.isEmpty()) {
            return null;
        } else {
            return pocusCardList.get(pocusCardList.size() - 1);
        }
    }

    /**
     * Remove the Pocus Card on the top of the container
     * 
     * @return the card being removed from the container
     */
    public Card removeTopPocusCard() {
        return pocusCardList.remove(pocusCardList.size() - 1);
    }

    /**
     * Remove the Hocus Card present on the gameZone
     * 
     * @return current hocus card
     */
    public Card removeHocusCard() {
        Card hocus = hocusCard;
        hocusCard = null;
        return hocus;
    }

    public String toString() {
        return "la Zone de jeu";
    }

    /**
     * Removes the Card that matches the ID parameter.
     * 
     * @param idCard
     *            ID of the card
     * @return the card corresponding to the ID
     */
    public Card removeCardByID(int idCard) {
        Card card = getCardById(idCard);
        return (this.pocusCardList.remove(card) ? card : null);
    }

}
