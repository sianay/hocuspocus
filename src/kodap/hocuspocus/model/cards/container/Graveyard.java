package kodap.hocuspocus.model.cards.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;

/**
 * The Graveyard class contains the trashed cards
 * 
 * @author Anaïs Payet
 * @version 1.0
 */
public class Graveyard implements Serializable, ICardsContainerBehaviour {

    private static final long serialVersionUID = 8718279276135064580L;
    private List<Card> cardList;

    /**
     * Default Constructor
     * The initial list is empty
     */
    public Graveyard() {
        this.cardList = new ArrayList<Card>();
    }

    @Override
    public Boolean addCard(Card card) {
        return this.cardList.add(card);
    }

    @Override
    public Boolean addCards(ArrayList<Card> cards) {
        return this.cardList.addAll(cards);
    }

    @Override
    public List<Card> removeAllCards() {
        List<Card> liste = new ArrayList<Card>(this.cardList);
        this.cardList.clear();

        return liste;

    }

    @Override
    public Card getCardById(int id) {
        for (Card card : this.cardList)
            if (card.getId() == id)
                return card;

        return null;
    }

    @Override
    public int getNumberOfCards() {
        return cardList.size();
    }
    
    public String toString() {
        return "le cimetière";
    }
}
