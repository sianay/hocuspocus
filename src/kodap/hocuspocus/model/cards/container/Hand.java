package kodap.hocuspocus.model.cards.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;

/**
 * Hand class contains the cards that a player have in his hand
 * A Hand can have a maximum of 5 cards
 * 
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class Hand implements ICardsContainerBehaviour, Serializable {

    private final int HAND_NUMBER = 5;
    private static final long serialVersionUID = 2615149489169867559L;
    private List<Card> cardList;

    /**
     * Constructor
     */
    public Hand() {
        super();
        this.cardList = new ArrayList<Card>();
    }

    @Override
    public Boolean addCard(Card card) {
        if (getNumberOfCards()<HAND_NUMBER)
            return cardList.add(card);
        else return false;
    }

    @Override
    public Boolean addCards(ArrayList<Card> cards) {
        if (cards.size() > HAND_NUMBER) return false;
        else return cardList.addAll(cards);
    }

    @Override
    public List<Card> removeAllCards() {
        List<Card> liste = new ArrayList<Card>(this.cardList);
        this.cardList.clear();

        return liste;

    }

    /**
     * Remove a card by given id
     * 
     * @param id ID of the card
     * @return the card removed from the Hand
     */
    public Card removeCardById(int id) {
        Card card = getCardById(id);
        return (this.cardList.remove(card) ? card : null);
    }

    @Override
    public Card getCardById(int id) {
        for (Card card : this.cardList)
            if (card.getId() == id)
                return card;

        return null;
    }

    @Override
    public int getNumberOfCards() {
        return cardList.size();
    }

    /**
     * get the Hand card list
     * @return cardList the hand card list
     */
    public List<Card> getCardList() {
        return cardList;
    }

    public String toString() {
            return "sa Main";
    }
}
