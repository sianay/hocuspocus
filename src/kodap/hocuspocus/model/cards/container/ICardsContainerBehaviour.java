package kodap.hocuspocus.model.cards.container;

import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;

/**
 * Define the methods that every Cards Container should have
 * 
 * @author Anaïs Payet
 */
public interface ICardsContainerBehaviour {

    /**
     * Add a card to the container
     * 
     * @param card the desired card
     * @return true if succeed
     */
    public Boolean addCard(Card card);

    /**
     * Add a list of cards to the container
     * 
     * @param cards the desired list of cards
     * @return true if succeed
     */
    public Boolean addCards(ArrayList<Card> cards);

    /**
     * Found and get a card by his id
     * 
     * @param id ID of the card
     * @return card if found, null if not
     */
    public Card getCardById(int id);

    /**
     * Remove all cards from the Container
     * 
     * @return List<Card> the list removed
     */
    public List<Card> removeAllCards();

    /**
     * Get the number of cards
     * 
     * @return the number of cards inside the container
     */
    public int getNumberOfCards();
}
