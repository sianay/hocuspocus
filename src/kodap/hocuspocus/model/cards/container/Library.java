package kodap.hocuspocus.model.cards.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.hocus.Abracadabra;
import kodap.hocuspocus.model.cards.hocus.BouleDeCristal;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.hocus.Inspiration;
import kodap.hocuspocus.model.cards.hocus.Malediction;
import kodap.hocuspocus.model.cards.hocus.Sacrifice;
import kodap.hocuspocus.model.cards.hocus.Sortilege;
import kodap.hocuspocus.model.cards.hocus.Voleur;
import kodap.hocuspocus.model.cards.hocus.Vortex;
import kodap.hocuspocus.model.cards.pocus.Amulette;
import kodap.hocuspocus.model.cards.pocus.BaguetteMagique;
import kodap.hocuspocus.model.cards.pocus.ChatNoir;
import kodap.hocuspocus.model.cards.pocus.Citrouille;
import kodap.hocuspocus.model.cards.pocus.ContreSort;
import kodap.hocuspocus.model.cards.pocus.Eclair;
import kodap.hocuspocus.model.cards.pocus.MiroirEnchante;
import kodap.hocuspocus.model.cards.pocus.Sablier;

/**
 * The Library class contains the pile of cards
 * 
 * @author Anaïs Payet
 * @version 1.0
 */
public class Library implements ICardsContainerBehaviour, Serializable {

    private static final long serialVersionUID = -8792264163219421073L;
    private List<Card> cardList;

    /**
     * Constructor
     * Init the library with a number of cards corresponding to the number of players
     * 
     * @param playerNumber number of player
     */
    public Library(int playerNumber) {
        super();
        this.cardList = new ArrayList<Card>();
        createInitialPile(playerNumber);
    }

    private void createInitialPile(int nb) {

        // Hocus
        cardList.add(new Vortex(0));
        cardList.add(new Vortex(1));
        cardList.add(new Abracadabra(2));
        cardList.add(new Abracadabra(3));
        cardList.add(new BouleDeCristal(4));
        cardList.add(new BouleDeCristal(5));
        cardList.add(new BouleDeCristal(7));
        cardList.add(new Hibou(8, 1));
        cardList.add(new Hibou(9, 1));
        cardList.add(new Hibou(10, 2));
        cardList.add(new Sacrifice(11, 2));
        cardList.add(new Sacrifice(12, 2));
        cardList.add(new Malediction(13, 1));
        cardList.add(new Malediction(14, 1));
        cardList.add(new Malediction(15, 2));
        cardList.add(new Inspiration(16, 2));
        cardList.add(new Inspiration(17, 2));
        cardList.add(new Inspiration(18, 2));
        cardList.add(new Inspiration(19, 3));
        cardList.add(new Inspiration(20, 3));
        cardList.add(new Voleur(21, 1));
        cardList.add(new Voleur(22, 1));
        cardList.add(new Voleur(23, 1));
        cardList.add(new Voleur(24, 1));
        cardList.add(new Voleur(25, 1));
        cardList.add(new Voleur(26, 2));
        cardList.add(new Voleur(27, 2));
        cardList.add(new Voleur(28, 2));
        cardList.add(new Voleur(29, 2));
        cardList.add(new Voleur(30, 3));
        cardList.add(new Voleur(31, 3));
        cardList.add(new Voleur(32, 3));
        cardList.add(new Voleur(33, 4));
        cardList.add(new Voleur(34, 4));
        cardList.add(new Voleur(35, 5));
        cardList.add(new Sortilege(36, 1));
        cardList.add(new Sortilege(37, 1));
        cardList.add(new Sortilege(38, 1));
        cardList.add(new Sortilege(39, 1));
        cardList.add(new Sortilege(40, 1));
        cardList.add(new Sortilege(41, 2));
        cardList.add(new Sortilege(42, 2));
        cardList.add(new Sortilege(43, 2));
        cardList.add(new Sortilege(44, 2));
        cardList.add(new Sortilege(45, 3));
        cardList.add(new Sortilege(46, 3));
        cardList.add(new Sortilege(47, 3));
        cardList.add(new Sortilege(48, 4));
        cardList.add(new Sortilege(49, 4));
        cardList.add(new Sortilege(50, 5));

        // Pocus
        cardList.add(new Amulette(51));
        cardList.add(new Amulette(52));
        cardList.add(new Amulette(53));
        cardList.add(new Amulette(54));
        if (nb == 2) {
            cardList.add(new Amulette(55));
            cardList.add(new Amulette(56));
            cardList.add(new Amulette(57));
        }
        cardList.add(new BaguetteMagique(52));
        cardList.add(new BaguetteMagique(53));
        cardList.add(new BaguetteMagique(54));
        cardList.add(new BaguetteMagique(55));
        if (nb != 2) {
            cardList.add(new MiroirEnchante(56));
            cardList.add(new MiroirEnchante(57));
            cardList.add(new MiroirEnchante(58));
        }
        cardList.add(new Citrouille(59));
        cardList.add(new Citrouille(60));
        cardList.add(new Citrouille(61));
        cardList.add(new ContreSort(62));
        cardList.add(new ContreSort(63));
        cardList.add(new Sablier(64));
        cardList.add(new Sablier(65));
        cardList.add(new ChatNoir(66));
        cardList.add(new ChatNoir(67));
        cardList.add(new Eclair(68));
        cardList.add(new Eclair(69));

    }

    @Override
    public int getNumberOfCards() {
        return cardList.size();
    }

    @Override
    public Boolean addCard(Card card) {
        return cardList.add(card);
    }
    
    /**
     * Add the card at a specified index
     * @param card card you want to add
     * @param index position on the list
     */
    public void addCard(Card card, int index) {
        cardList.add(index, card);
    }

    @Override
    public Boolean addCards(ArrayList<Card> cards) {
        return cardList.addAll(cards);
    }

    @Override
    public List<Card> removeAllCards() {
        List<Card> liste = new ArrayList<Card>(this.cardList);
        this.cardList.clear();

        return liste;
    }

    @Override
    public Card getCardById(int id) {
        for (Card card : this.cardList)
            if (card.getId() == id)
                return card;

        return null;
    }

    /**
     * Remove the Card on the top of the container
     * 
     * @return the card being removed from the container
     */
    public Card removeTopCard() {
        return cardList.remove(cardList.size() - 1);
    }

    /**
     * Remove a given number of cards of the library
     * 
     * @param number the number you want to remove
     * @return the list of cards removed from the library
     */
    public ArrayList<Card> removeCards(int number) {
        List<Card> liste = new ArrayList<Card>();

        for (int i = 0; i < this.cardList.size(); i++)
            liste.add(this.removeTopCard());

        return (ArrayList<Card>) liste;
    }

    /**
     * Shuffle the library
     */
    public void shuffle() {
        Collections.shuffle(this.cardList);
    }
    
    public String toString() {
        return "la Librairie";
    }

}
