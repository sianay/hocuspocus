package kodap.hocuspocus.model.cards.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.cards.Card;

/**
 * SpellBook class contains the visible cards of the Player
 * The SpellBook can have a number of 3 maximum cards at a time
 * 
 * @author Anaïs Payet
 * @version 1.0
 */
public class Spellbook implements ICardsContainerBehaviour, Serializable {

    /**
     * Maximum spellbook cards number
     */
    public final static int SPELLBOOK_NUMBER = 3;
    private static final long serialVersionUID = -4039520771961411896L;
    private List<Card> cardList;

    /**
     * Constructor Initialize with an empty list
     */
    public Spellbook() {
        super();
        this.cardList = new ArrayList<Card>();
    }

    @Override
    public Boolean addCard(Card card) {
        if (getNumberOfCards() < SPELLBOOK_NUMBER)
            return cardList.add(card);
        else return false;
    }

    @Override
    public Boolean addCards(ArrayList<Card> cards) {
        if (cards.size() > SPELLBOOK_NUMBER) return false;
        else return cardList.addAll(cards);
    }

    /**
     * Remove a card by given id
     * 
     * @param id ID of the card
     * @return the card removed from the SpellBook
     */
    public Card removeCardById(int id) {
        Card card = getCardById(id);
        return (this.cardList.remove(card) ? card : null);
    }

    /**
     * Remove a card
     * 
     * @param card the Card to remove
     * @return the card removed from the SpellBook
     */
    public Card removeCard(Card card) {
        return (this.cardList.remove(card) ? card : null);
    }

    @Override
    public List<Card> removeAllCards() {
        List<Card> liste = new ArrayList<Card>(this.cardList);
        this.cardList.clear();

        return liste;
    }

    @Override
    public Card getCardById(int id) {
        for (Card card : this.cardList)
            if (card.getId() == id) {
                return card;
            }
        return null;
    }

    @Override
    public int getNumberOfCards() {
        return cardList.size();
    }

    /**
     * Get card list
     * 
     * @return cardList the spellbook card list
     */
    public List<Card> getCardList() {
        return cardList;
    }

    
    public String toString() {
        return "son Grimoire";
    }

}
