package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Abracadabra class represents the Hocus card Abracadabra.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Abracadabra extends HocusCard implements Serializable{

    private static final long serialVersionUID = -8141393675561913484L;

    /**
     * Constructor
     * @param idCard
     */
    public Abracadabra(int idCard) {
        super(idCard);
        this.setImage("abracadabra.png");
        this.setAction(new Action(    
                null,                // String playerSource
                idCard,              // int cardPlayed
                null,                // String playerTargeted
                Zone.HAND,           // Zone target
                ObjectType.CARD,     // ObjectType typeTargeted
                "_NO_",              // finalDestinationCardPlayedPlayer;
                Zone.GRAVEYARD,      // Zone finalDestinationCardPlayed
                false,               // endPocusTurn
                false,               // endHocusTurn
                false,
                null,
                null,                // String sourceContainerPlayer
                Zone.HAND,           // Zone sourceContainer
                0,                   // int number
                null,                // String destinationContainerPlayer
                Zone.HAND,           // Zone destinationContainer
                ActionVerb.EXCHANGE  // ActionVerb action
            )
        );
        this.setDescription("Désignez un joueur de votre choix. Echangez votre Main avec celui-ci. Vous n'avez " 
                + "pas besoin d'avoir de cartes en Main pour jouer Abracadabra. Si vous n'avez pas de "
                + "cartes en Main, le joueur désigné doit simplement vous-donner les cartes de sa Main.");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber, String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,        // String playerSource
            idCard,                  // int cardPlayed
            nameTargetedPlayer,      // String playerTargeted
            Zone.HAND,               // Zone target
            ObjectType.CARD,         // ObjectType typeTargeted
            "_NO_",                  // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,          // Zone finalDestinationCardPlayed
            false,                   // endPocusTurn
            false,                   // endHocusTurn
            false,
            null,
            nameTargetedPlayer,      // String sourceContainerPlayer
            Zone.HAND,               // Zone sourceContainer
            0,                       // int number
            nameSourcePlayer,        // String destinationContainerPlayer
            Zone.HAND,               // Zone destinationContainer
            ActionVerb.EXCHANGE      // ActionVerb action
        );

        return result;
    }
}
