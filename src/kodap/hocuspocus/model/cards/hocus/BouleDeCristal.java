package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The BouleDeCristal class represents the Hocus card BouleDeCristal.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class BouleDeCristal extends HocusCard implements Serializable{

    private static final long serialVersionUID = 7906083291395550490L;

    /**
     * Constructor
     * @param idCard
     */
    public BouleDeCristal(int idCard) {
        super(idCard);
        this.setImage("bouledecristal.png");
        this.setAction(new Action(
            null,                      // String playerSource
            idCard,                    // int cardPlayed
            "_NO_",                    // String playerTargeted
            Zone.LIBRARY,              // Zone target
            ObjectType.CARD,           // ObjectType typeTargeted
            "_NO_",                    // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,            // Zone finalDestinationCardPlayed
            false,                     // endPocusTurn
            false,                     // endHocusTurn
            false,
            null,
            "_NO_",                    // String sourceContainerPlayer
            Zone.LIBRARY,              // Zone sourceContainer
            4,                         // int number
            "_NO_",                    // String destinationContainerPlayer
            Zone.LIBRARY,              // Zone destinationContainer
            ActionVerb.CHANGE_ORDER    // ActionVerb action
        ));
        this.setDescription("Regardez les 4 premières cartes de la Bibliothèque et replacez-les dessus, dans l'ordre de votre choix.");    
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber, String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,           // String playerSource
            idCard,                     // int cardPlayed
            "_NO_",                     // String playerTargeted
            Zone.LIBRARY,               // Zone target
            ObjectType.CARD,            // ObjectType typeTargeted
            "_NO_",                     // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,             // Zone finalDestinationCardPlayed
            false,                      // endPocusTurn
            false,                      // endHocusTurn
            false,
            null,
            "_NO_",                     // String sourceContainerPlayer
            Zone.LIBRARY,               // Zone sourceContainer
            4,                          // int number
            "_NO_",                     // String destinationContainerPlayer
            Zone.LIBRARY,               // Zone destinationContainer
            ActionVerb.CHANGE_ORDER     // ActionVerb action
        );

        return result;
    }
}