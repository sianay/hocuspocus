package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Hibou class represents the Hocus card Hibou.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Hibou extends HocusCard implements Serializable {

    private static final long serialVersionUID = -1312025627014852373L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Hibou(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("hibou" + powerNumber + ".png");
        this.setAction(new Action(
            null,               // String playerSource
            idCard,             // int cardPlayed
            null,               // String playerTargeted
            Zone.SPELLBOOK,     // Zone target
            ObjectType.CARD,    // ObjectType typeTargeted
            "_NO_",             // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,     // Zone finalDestinationCardPlayed
            false,              // endPocusTurn
            false,              // endHocusTurn
            false,
            null,
            null,               // String sourceContainerPlayer
            Zone.SPELLBOOK,     // Zone sourceContainer
            powerNumber,        // int number
            null,               // String destinationContainerPlayer
            Zone.HAND,          // Zone destinationContainer
            ActionVerb.TAKE     // ActionVerb action
        ));

        this.setDescription("Désignez un joueur de votre choix. Celui-ci doit vous donner le nombre de carte(s)"
            +" de son Grimoire, indiqué sur la carte Hibou. Si ce joueur ne possède pas assez de "
            +"cartes dans son Grimoire, il doit vous donner tout son Grimoire.");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */

    @Override
    public Action createAction(int idCard, int powerNumber, String nameSourcePlayer,
            String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,         // String playerSource
            idCard,                   // int cardPlayed
            nameTargetedPlayer,       // String playerTargeted
            Zone.SPELLBOOK,           // Zone target
            ObjectType.CARD,          // ObjectType typeTargeted
            "_NO_",                   // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,           // Zone finalDestinationCardPlayed
            false,                    // endPocusTurn
            false,                    // endHocusTurn
            false,
            null,
            nameTargetedPlayer,       // String sourceContainerPlayer
            Zone.SPELLBOOK,           // Zone sourceContainer
            powerNumber,              // int number
            nameSourcePlayer,         // String destinationContainerPlayer
            Zone.HAND,                // Zone destinationContainer
            ActionVerb.TAKE           // ActionVerb action
        );

        return result;
    }
}