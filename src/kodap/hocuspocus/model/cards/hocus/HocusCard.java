package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Action;

/**
 * PocusCard class :
 * 
 * Abstract class to specify Hocus card characteristics.
 * @author Francois
 *
 */
public abstract class HocusCard extends Card implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -5583570404798733520L;
    private int powerNumber;
    private Action action;
    
    /**
     * Constructor
     * 
     * @param idCard int
     * @param powerNumber int
     */
    public HocusCard(int idCard, int powerNumber) {
        super(idCard);
        this.powerNumber = powerNumber;
    }

    /**
     * Constructor
     * 
     * @param idCard int
     */
    public HocusCard(int idCard) {
        super(idCard);
    }

    /**
     * Get power number
     * 
     * @return powerNumber int
     */
    public int getPowerNumber() {
        return this.powerNumber;
    }

    /**
     * Set power number
     * 
     * @param powerNumber
     */
    public void setPowerNumber(int powerNumber) {
        this.powerNumber = powerNumber;
    }

    /**
     * @return action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action
     */
    public void setAction(Action action) {
        this.action = action;
    }
    
    /**
     * @param idCard 
     * @param powerNumber 
     * @param nameSourcePlayer Name of the player who plays the card
     * @param nameTargetedPlayer Name of the targeted player 
     * @return action
     */
    public abstract Action createAction(int idCard, int powerNumber, String nameSourcePlayer, String nameTargetedPlayer);
}
