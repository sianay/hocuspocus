package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Inspiration class represents the Hocus card Inspiration.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */

public class Inspiration extends HocusCard implements Serializable {

    private static final long serialVersionUID = 7827212966883500890L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Inspiration(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("inspiration" + powerNumber + ".png");
        this.setAction(new Action(
            null,                // String playerSource
            idCard,              // int cardPlayed
            "_NO_",              // String playerTargeted
            Zone.LIBRARY,        // Zone target
            ObjectType.CARD,     // ObjectType typeTargeted
            "_NO_",              // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,      // Zone finalDestinationCardPlayed
            false,               // endPocusTurn
            false,               // endHocusTurn
            false,
            null,
            "_NO_",              // String sourceContainerPlayer
            Zone.LIBRARY,        // Zone sourceContainerTarget
            powerNumber,         // int number
            null,                // String destinationContainerPlayer
            Zone.HAND,           // Zone destinationContainerTarget
            ActionVerb.TAKE      // ActionVerb action
        ));
        this.setDescription("Piochez le nombre de cartes de la Bibliothèque, indiqué sur la carte Inspiration."
            + " Si cette carte est jouée avec succès, aucune autre Inspiration ne peut être jouée "
            + "pendant votre Tour."); 
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,      // String playerSource
            idCard,                // int cardPlayed
            "_NO_",                // String playerTargeted
            Zone.LIBRARY,          // Zone target
            ObjectType.CARD,       // ObjectType typeTargeted
            "_NO_",                // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,        // Zone finalDestinationCardPlayed
            false,                 // endPocusTurn
            false,                 // endHocusTurn
            false,
            null,
            "_NO_",                // String sourceContainerPlayer
            Zone.LIBRARY,          // Zone sourceContainer
            powerNumber,           // int number
            nameSourcePlayer,      // String destinationContainerPlayer
            Zone.HAND,             // Zone destinationContainer
            ActionVerb.TAKE        // ActionVerb action
        );
        return result;
    }
}