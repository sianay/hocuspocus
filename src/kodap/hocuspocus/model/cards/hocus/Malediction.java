package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Malediction class represents the Hocus card Malediction.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Malediction extends HocusCard implements Serializable {

    private static final long serialVersionUID = -5894956500248061357L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Malediction(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("malediction" + powerNumber + ".png");
        this.setAction(new Action(
            null,               // String playerSource
            idCard,             // int cardPlayed
            null,               // String playerTargeted
            Zone.SPELLBOOK,     // Zone target
            ObjectType.CARD,    // ObjectType typeTargeted
            "_NO_",             // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,     // Zone finalDestinationCardPlayed
            false,              // endPocusTurn
            false,              // endHocusTurn
            false,
            null,
            null,               // String sourceContainerPlayer
            Zone.SPELLBOOK,     // Zone sourceContainerTarget
            powerNumber,        // int number
            "_NO_",             // String destinati    onContainerPlayer
            Zone.GRAVEYARD,     // Zone destinationContainerTarget
            ActionVerb.TAKE     // ActionVerb action
        ));
        this.setDescription("Désignez un joueur de votre choix. Désignez ensuite, dans le Grimoire de ce joueur "
                        + "autant de cartes qu'indiqué sur la carte Malédiction. Ces cartes doivent être placées, "
                        + "face visible, sur la pile de Défausse.");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,       // String playerSource
            idCard,                 // int cardPlayed
            nameTargetedPlayer,     // String playerTargeted
            Zone.SPELLBOOK,         // Zone target
            ObjectType.CARD,        // ObjectType typeTargeted
            "_NO_",                 // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,         // Zone finalDestinationCardPlayed
            false,                  // endPocusTurn
            false,                  // endHocusTurn
            false,
            null,
            nameTargetedPlayer,     // String sourceContainerPlayer
            Zone.SPELLBOOK,         // Zone sourceContainer
            powerNumber,            // int number
            "_NO_",                 // String destinationContainerPlayer
            Zone.GRAVEYARD,         // Zone destinationContainer
            ActionVerb.TAKE         // ActionVerb action
        );

        return result;
    }
}