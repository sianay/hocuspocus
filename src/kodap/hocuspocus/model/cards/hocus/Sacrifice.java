package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Sacrifice class represents the Hocus card Sacrifice.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Sacrifice extends HocusCard implements Serializable{

    private static final long serialVersionUID = 3323442365848704100L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Sacrifice(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("sacrifice" + powerNumber + ".png");
        this.setAction(new Action(
            null,             // String playerSource
            idCard,           // int cardPlayed
            null,             // String playerTargeted
            Zone.CAULDRON,    // Zone target
            ObjectType.GEM,   // ObjectType typeTargeted
            "_NO_",           // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,   // Zone finalDestinationCardPlayed
            false,            // endPocusTurn
            false,            // endHocusTurn
            false,
            null,
            null,             // String sourceContainerPlayer
            Zone.CAULDRON,    // Zone sourceContainer
            powerNumber,      // int number
            null,             // String destinationContainerPlayer
            Zone.CAULDRON,    // Zone destinationContainer
            ActionVerb.TAKE   // ActionVerb action
        ));
        this.setDescription("Désignez un joueur de votre choix. Celui-ci doit remettre 2 de ses Gemmes dans le "
            + "Chaudron. Si ce joueur ne possède qu'une seule Gemme, il doit quand même la "
            + "remettre dans le Chaudron. Astuce : cette carte prend toute son importance "
            + "vers la fin de la partie. En effet, en rajoutant des Gemmes dans le Chaudron il devient plus difficile "
            + "pour le joueur qui est en tête de retirer la dernière Gemme.");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,      // String playerSource
            idCard,                // int cardPlayed
            nameTargetedPlayer,    // String playerTargeted
            Zone.CAULDRON,         // Zone target
            ObjectType.GEM,        // ObjectType typeTargeted
            "_NO_",                // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,        // Zone finalDestinationCardPlayed
            false,                 // endPocusTurn
            false,                 // endHocusTurn
            false,
            null,
            nameTargetedPlayer,    // String sourceContainerPlayer
            Zone.CAULDRON,         // Zone sourceContainer
            powerNumber,           // int number
            null            ,      // String destinationContainerPlayer
            Zone.CAULDRON,         // Zone destinationContainer
            ActionVerb.TAKE        // ActionVerb action
        );

        return result;
    }
}