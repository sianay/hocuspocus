package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Sortilege class represents the Hocus card Sortilege.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Sortilege extends HocusCard implements Serializable{

    private static final long serialVersionUID = 8772430770318837977L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Sortilege(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("sortilege" + powerNumber + ".png");
        this.setAction(new Action(
            null,           // String playerSource
            idCard,         // int cardPlayed
            "_NO_",         // String playerTargeted
            Zone.CAULDRON,  // Zone target
            ObjectType.GEM, // ObjectType typeTargeted
            "_NO_",         // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD, // Zone finalDestinationCardPlayed
            false,          // endPocusTurn
            false,          // endHocusTurn
            false,
            null,
            "_NO_",         // String sourceContainerPlayer
            Zone.CAULDRON,  // Zone sourceContainer
            powerNumber,    // int number
            null,           // String destinationContainerPlayer
            Zone.CAULDRON,  // Zone destinationContainer
            ActionVerb.TAKE // ActionVerb action
        ));
        this.setDescription("Prenez le nombre de Gemme(s), indiqué sur la carte Sortilège, dans le Chaudron. "
            + "Placez-les devant vous.");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,    // String playerSource
            idCard,              // int cardPlayed
            "_NO_",              // String playerTargeted
            Zone.CAULDRON,       // Zone target
            ObjectType.GEM,      // ObjectType typeTargeted
            "_NO_",              // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,      // Zone finalDestinationCardPlayed
            false,               // endPocusTurn
            false,               // endHocusTurn
            false,
            null,
            "_NO_",              // String sourceContainerPlayer
            Zone.CAULDRON,       // Zone sourceContainer
            powerNumber,         // int number
            nameSourcePlayer,    // String destinationContainerPlayer
            Zone.CAULDRON,       // Zone destinationContainer
            ActionVerb.TAKE      // ActionVerb action
        );

        return result;
    }
}