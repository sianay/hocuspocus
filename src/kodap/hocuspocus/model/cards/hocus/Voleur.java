package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Voleur class represents the Hocus card Voleur.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Voleur extends HocusCard implements Serializable{

    private static final long serialVersionUID = 1610858703036563534L;

    /**
     * Constructor
     * @param idCard
     * @param powerNumber
     */
    public Voleur(int idCard, int powerNumber) {
        super(idCard, powerNumber);
        this.setImage("voleur" + powerNumber + ".png");
        this.setAction(new Action(
            null,                // String playerSource
            idCard,              // int cardPlayed
            null,                // String playerTargeted
            Zone.CAULDRON,       // Zone target
            ObjectType.GEM,      // ObjectType typeTargeted
            "_NO_",              // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,      // Zone finalDestinationCardPlayed
            false,               // endPocusTurn
            false,               // endHocusTurn
            false,
            null,
            null,                // String sourceContainerPlayer
            Zone.CAULDRON,       // Zone sourceContainer
            powerNumber,         // int number
            null,                // String destinationContainerPlayer
            Zone.CAULDRON,       // Zone destinationContainer
            ActionVerb.TAKE      // ActionVerb action
        ));

        this.setDescription("Désignez un joueur de votre choix. Celui-ci doit vous donner le nombre de "
                    + "Gemme(s) indiqué sur la carte Voleur. Si ce joueur ne possède pas assez de "
                    + "Gemmes, il doit vous donner toutes ses Gemmes.");        
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,           //String playerSource
            idCard,                     //int cardPlayed
            nameTargetedPlayer,         //String playerTargeted
            Zone.CAULDRON,              //Zone target
            ObjectType.GEM,             //ObjectType typeTargeted
            "_NO_",                     //finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,             //Zone finalDestinationCardPlayed
            false,                      //endPocusTurn
            false,                      //endHocusTurn
            false,
            null,
            nameTargetedPlayer,         //String sourceContainerPlayer
            Zone.CAULDRON,              //Zone sourceContainer
            powerNumber,                //int number
            nameSourcePlayer,           //String destinationContainerPlayer
            Zone.CAULDRON,              //Zone destinationContainer
            ActionVerb.TAKE             //ActionVerb action
        );

        return result;
    }
}