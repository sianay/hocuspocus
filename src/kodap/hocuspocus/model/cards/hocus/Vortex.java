package kodap.hocuspocus.model.cards.hocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;


/**
 * The Vortex class represents the Hocus card Vortex.
 * 
 * @author Francois <francois.huy@gmail.com>
 * @version 1.0
 */
public class Vortex extends HocusCard implements Serializable{

    private static final long serialVersionUID = -5168392436564687434L;

    /**
     * Constructor
     * 
     * @param idCard
     */
    public Vortex(int idCard) {
        super(idCard);
        this.setImage("vortex.png");
        this.setAction( new Action(
            null,               // String playerSource
            idCard,             // int cardPlayed
            "_ALL_",            // String playerTargeted
            Zone.SPELLBOOK,     // Zone target
            ObjectType.CARD,    // ObjectType typeTargeted
            "_NO_",             // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,     // Zone finalDestinationCardPlayed
            false,              // endPocusTurn
            false,              // endHocusTurn
            false,
            null,
            null,               // String sourceContainerPlayer
            Zone.SPELLBOOK,     // Zone sourceContainer
            0,                  // int number
            null,               // String destinationContainerPlayer
            Zone.SPELLBOOK,     // Zone destinationContainer
            ActionVerb.EXCHANGE // ActionVerb action
        ));
        this.setDescription("Tous les joueurs donnent leur Grimoire a leur voisin de gauche. Un Vortex ne peut "
            + "être arrêté que par un Contre-sort ou une Citrouille jouée a partir de la Main d'un "
            + "joueur. Les joueurs sans carte dans leur Grimoire ne donne rien à leur Voisin de gauche. "
            + "Astuce : utilisez un Vortex après avoir joué toutes vos cartes pour ne pas avoir à "
            + "donner des sorts à vos adversaires");
    }

    /**
     * Method witch returns a new action
     * @param idCard 
     * @return a new action
     */
    @Override
    public Action createAction(int idCard, int powerNumber,
            String nameSourcePlayer, String nameTargetedPlayer) {
        Action result = new Action(
            nameSourcePlayer,        // String playerSource
            idCard,                  // int cardPlayed
            "_ALL_",                 // String playerTargeted
            Zone.SPELLBOOK,          // Zone target
            ObjectType.CARD,         // ObjectType typeTargeted
            "_NO_",                  // finalDestinationCardPlayedPlayer;
            Zone.GRAVEYARD,          // Zone finalDestinationCardPlayed
            false,                   // endPocusTurn
            false,                   // endHocusTurn
            false,
            null,
            "_ALL_",                 // String sourceContainerPlayer
            Zone.SPELLBOOK,          // Zone sourceContainer
            0,                       // int number
            "_ALL_",                 // String destinationContainerPlayer
            Zone.SPELLBOOK,          // Zone destinationContainer
            ActionVerb.EXCHANGE      // ActionVerb action
        );

        return result;
    }
}