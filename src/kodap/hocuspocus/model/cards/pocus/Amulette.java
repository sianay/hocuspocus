package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;

/**
 * Amulette Class :
 * 
 * Amulette card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class Amulette extends PocusCard implements Serializable {

    private static final long serialVersionUID = 297583613019690901L;

    /**
     * Constructor Amulette card
     * @param idCard
     */
    public Amulette(int idCard) {
        super(idCard);
        this.setImage("amulette.png");
        this.setDescription("La carte Hocus, dirigée contre vous, sur laquelle vous jouez la carte Amulette, est détruite."
            + " Placez-la, face visible, sur la pile de Défausse. Toutes les cartes Pocus présentes dans"
            + " la Zone de jeu sont détruites. Placez-les, face visible, sur la pile de Défausse.");
    }
    
    
    public int UpdateAction(Action action, String player) {
        //A player can play this card only if the Hocus card is played against him
        if(!player.equals(action.getTargetPlayer()))
            return -1;
        else
        {    
            action.setActionVerb(ActionVerb.NONE);
            action.setEndPocusTurn(false);
            return 0;
        }
    }
}
