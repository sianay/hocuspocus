package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;

/**
 * BaguetteMagique class :
 * 
 * BaguetteMagique card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class BaguetteMagique extends PocusCard implements Serializable {

    private static final long serialVersionUID = 4782840344376947554L;

    /**
     * Constructor BaguetteMagique card
     * @param idCard
     */
    public BaguetteMagique(int idCard) {
        super(idCard);
        this.setImage("baguettemagique.png");
        this.setDescription("Le chiffre de la carte Hocus, sur laquelle vous jouez la carte Baguette Magique,"
            + " est double. Par exemple : Christophe joue une carte Sortilège 3 puis, tout de suite, place"
            + " par-dessus une carte Pocus Baguette Magique. Aucun autre joueur ne joue de carte Pocus."
            + " Christophe prend donc dans le Chaudron 6 Gemmes.");
    }

    public int UpdateAction(Action action, String player) {
        action.setNumber(action.getNumber()*2);
        action.setEndPocusTurn(false);
        return 0;
    }
}
