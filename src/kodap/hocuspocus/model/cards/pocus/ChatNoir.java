package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.Zone;

/**
 * ChatNoir class :
 * 
 * ChatNoir card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class ChatNoir extends PocusCard implements Serializable {

    private static final long serialVersionUID = 4949438880676163362L;

    /**
     * Constructor ChatNoir card
     * @param idCard
     */
    public ChatNoir(int idCard) {
        super(idCard);
        this.setImage("chatnoir.png");
        this.setDescription("Prenez la carte Hocus, sur laquelle vous jouez la carte Chat Noir. Mettez-la dans votre Main."
            + " Toutes les cartes Pocus présentes dans la Zone de jeu sont détruites. Placez-les, face"
            + " visible, sur la pile de Défausse.");
    }

    public int UpdateAction(Action action, String player) {
        action.setActionVerb(ActionVerb.TAKE);
        action.setFinalDestinationCardPlayedPlayer(player);
        action.setFinalDestinationCardPlayed(Zone.HAND);
        action.setEndPocusTurn(true);
        return 0;
    }
}
