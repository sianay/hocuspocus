package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * Citrouille class :
 * 
 * Citrouille card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class Citrouille extends PocusCard implements Serializable {

     private static final long serialVersionUID = -6814790363247790963L;
     
     /**
      * Constructor Citrouille card
      * @param idCard
      */
     public Citrouille(int idCard) {
          super(idCard);
          this.setImage("citrouille.png");
          this.setDescription("La carte Hocus, sur laquelle vous jouez la carte Citrouille, retourne dans la Main du"
               + " joueur qui vient de la jouer. Il ne pourra la jouer a nouveau qu'a partir de son"
               + " prochain Tour. Toutes les cartes Pocus présentes dans la Zone de jeu sont détruites."
               + " Placez-les, face visible, sur la pile de Défausse. Astuce : vous pouvez jouer une"
               + " Citrouille sur votre propre carte Hocus pour y enlever les cartes Pocus jouées par"
               + " les autres joueurs.");
     }

     public int UpdateAction(Action action, String player) {
          //Cards list to disable
          Board board = ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();
          List<Card> cardsToDisable = new ArrayList<Card>();

          cardsToDisable.add(board.getCardById(action.getCardPlayed()));
          action.setListCardsToDisable(cardsToDisable);
          action.setActionVerb(ActionVerb.TAKE);
          action.setFinalDestinationCardPlayedPlayer(action.getPlayerSource());
          action.setFinalDestinationCardPlayed(Zone.HAND);
          action.setEndPocusTurn(false);

          return 0;
     }
}
