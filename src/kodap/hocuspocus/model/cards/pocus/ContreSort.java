package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;

/**
 * ContreSort Class:
 * 
 * ContreSort card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class ContreSort extends PocusCard implements Serializable {

    private static final long serialVersionUID = 2126985633832895868L;

    /**
     * Constructor ContreSort card
     * @param idCard
     */
    public ContreSort(int idCard) {
        super(idCard);
        this.setImage("contresort.png");
        this.setDescription("La carte Hocus, sur laquelle vous jouez la carte Contre-sort, est détruite. Placez-la, "
            + "face visible, sur la pile de Défausse. Toutes les cartes Pocus présentes dans la Zone "
            + "de jeu sont détruites. Placez-les, face visible, sur la pile de Défausse.");
    }

    public int UpdateAction(Action action, String player) {
        action.setActionVerb(ActionVerb.NONE);
        action.setEndPocusTurn(true);
        return 0;
    }
}
