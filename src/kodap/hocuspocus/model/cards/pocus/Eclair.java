package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;
import kodap.hocuspocus.model.cards.actions.Action;

/**
 * Eclair Class:
 * 
 * Eclair card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class Eclair extends PocusCard implements Serializable{

    private static final long serialVersionUID = 1767782453128801869L;

    /**
     * Constructor Eclair card.
     * @param idCard
     */
    public Eclair(int idCard) {
        super(idCard);
        this.setImage("eclair.png");
        this.setDescription("La carte Hocus, sur laquelle vous jouez la carte Eclair; est protégée contre toutes les "
            + "autres cartes Pocus à venir; celles-ci seront sans effet. De plus, toutes les cartes "
            + "Pocus déjà jouées sur la carte Hocus sont annulées. Placez-les, face visible, sur la "
            + "pile de Défausse. Seule la carte Baguette Magique fonctionne sur une carte Hocus sur "
            + "laquelle est jouée une carte Eclair: Astuce : cette carte Pocus est la plus puissante "
            + "car elle empêche une carte Hocus d’être arrêtée, même par des cartes Pocus déjà jouées avant!");
    }

    public int UpdateAction(Action action, String player) {
        //Cards list to disable
        
    	/*Board board = ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();
        List<Card> cardsToDisable = new ArrayList<Card>();
        for (Player onePlayer : board.getPlayerList()) {
            for (Card spellBookCard : onePlayer.getSpellbook().getCardList()) {
                if ((spellBookCard instanceof PocusCard) && !(spellBookCard instanceof BaguetteMagique) ) {
                    cardsToDisable.add(spellBookCard);
                }
            }
            for (Card handCard : onePlayer.getHand().getCardList()) {
                if ((handCard instanceof PocusCard) && !(handCard instanceof BaguetteMagique) ) {
                    cardsToDisable.add(handCard);
                }
            }
        }

        action.setListCardsToDisable(cardsToDisable);*/
    	action.setImmunized(true);
        action.setResetAction(true);
        action.setEndPocusTurn(false);

        return 0;
    }
}
