package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;

/**
 * MiroirEnchante Class:
 * 
 * MiroirEnchante card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class MiroirEnchante extends PocusCard implements Serializable {

    private static final long serialVersionUID = 7893379783867994026L;

    /**
     * Constructor MiroirEnchante card
     * @param idCard
     */
    public MiroirEnchante(int idCard) {
        super(idCard);
        this.setImage("miroirenchante.png");
        this.setDescription("Renvoyez le Pouvoir de la carte Hocus dont vous êtes la cible. Désignez un"
            + " joueur, autre que celui qui a joue la carte Hocus sur laquelle vous"
            + " jouez la carte Miroir Enchante. Ce joueur devient la cible du Pouvoir"
            + " de la carte dont vous étiez la cible. Plusieurs cartes Miroir Enchante"
            + " peuvent être jouées sur une seule carte Hocus. Mais, au final, elles ne"
            + " peuvent jamais renvoyer la carte Hocus sur le joueur qui l'a jouée.");
    }

    /**
     * @param action
     * @param player
     * @param playerTargeted
     * @return integer
     */
    public int UpdateAction(Action action, String player, String playerTargeted) {
        //The mirror can not take the Hocus car player as target.
        if(action.getPlayerSource().equals(playerTargeted))
            return -1;
        else
        {
            //Ask who is the target
            action.setTargetPlayer(playerTargeted);
            action.setResetAction(true);
            action.setEndPocusTurn(false);
            return 0;
        }    
    }

    public int UpdateAction(Action action, String player) {
        action.setTargetPlayer("");
        return 0;
    }
}
