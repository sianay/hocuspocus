package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Action;

/**
 * PocusCard class :
 * 
 * Abstract class to specify Pocus card characteristics.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public abstract class PocusCard extends Card implements Serializable {

    private static final long serialVersionUID = 6882173026232022955L;

    /**
     * Constructor PocusCard
     * @param idCard int
     */
    public PocusCard(int idCard) {
        super(idCard);
    }

    /**
     * Method UpdateAction
     * @param action
     * @param player
     * @return integer
     */
    public int UpdateAction(Action action, String player) {
        return 0;
    }
}
