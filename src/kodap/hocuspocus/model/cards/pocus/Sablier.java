package kodap.hocuspocus.model.cards.pocus;

import java.io.Serializable;

import kodap.hocuspocus.model.cards.actions.Action;

/**
 * Sablier Class :
 * 
 * Sablier card description.
 * 
 * @author Jorys <jorys.gaillard@gmail.com>
 * @version 1.0
 */
public class Sablier extends PocusCard implements Serializable {

    private static final long serialVersionUID = 4163428967764835944L;

    /**
     * Constructor Sablier card
     * @param idCard
     */
    public Sablier(int idCard) {
        super(idCard);
        this.setImage("sablier.png");
        this.setDescription("Mettez fin au Tour du joueur qui a pose la carte Hocus sur laquelle vous jouez"
            + " la carte Sablier. Des que le Pouvoir de cette carte Hocus est résolu, le"
            + " Tour du joueur en cours est termine. Il pioche tout de même 2 cartes de la"
            + " Bibliothèque ou 1 Gemme du Chaudron. Astuce : jouez un Sablier lorsque vous"
            + " croyez qu’un adversaire est sur le point de jouer plus d’une carte Hocus."
            + " Cela détruira tous ses plans.");
    }

    public int UpdateAction(Action action, String player) {
        action.setEndHocusTurn(true);
        action.setEndPocusTurn(false);
        return 0;
    }
}
