package kodap.hocuspocus.model.gems;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The Cauldron Class.
 * 
 * This class will represent the gems present in the cauldron game.
 * 
 * @author Vlad <vlad.marin@icloud.com>
 * @version 1.0
 */

public class Cauldron implements Serializable {
    // ATTRIBUTS
    private List<Gem> gemList;
    private static final long serialVersionUID = 87806090493681201L;

    // CONSTRUCTOR
    /**
     * Default class constructor. This constructor builds an empty ArrayList of
     * gems. It must be manually populated afterwards.
     */
    public Cauldron() {
        this.gemList = new ArrayList<Gem>();
    }

    /**
     * Full class constructor. This constructor build the Cauldron based on the
     * number of players passed as parameter. To be used only once in the Game.
     * 
     * @param numberOfPlayers
     */
    public Cauldron(int numberOfPlayers) {
        this.gemList = new ArrayList<Gem>();
        this.initializeGems(numberOfPlayers);
    }

    /**
     * Full class constructor. This constructor an full ArrayList of gems based
     * on a collection of gems passed by parameter.
     * 
     * @param gemList
     */
    public Cauldron(ArrayList<Gem> gemList) {
        this.gemList = new ArrayList<Gem>();
        for (int i = 0; i < gemList.size(); i++) {
            this.gemList.add(gemList.get(i));
        }
    }

    // GETTERS
    /**
     * Returns an integer value of the total Gems contained by the Cauldron.
     * 
     * @return int
     */
    public int getSize() {
        return this.gemList.size();
    }

    // METHODS
    /**
     * Initializes all the necessary Gem in the Game.
     * 
     * @param numberOfPlayers
     */
    private void initializeGems(int numberOfPlayers) {
        for (int i = 1; i <= calculateInitialCauldronSize(numberOfPlayers); i++) {
            this.gemList.add(new Gem(i));
        }
    }

    /**
     * Calculates and returns the total number of Gems as a function of the
     * players number.
     * 
     * @param numberOfPlayers
     * @return int 
     */
    private int calculateInitialCauldronSize(int numberOfPlayers) {
        return (10 + (numberOfPlayers * 5));
    }

    /**
     * Adds one Gem to the Cauldron and increases its total size.
     * 
     * @param oneGem
     */
    public void addGem(Gem oneGem) {
        this.gemList.add(oneGem);
    }

    /**
     * Removes the first Gem of the Cauldron and decreases its total size.
     * @return oneGem (returns the removed Gem) or null if the cauldron is empty
     * 
     */
    public Gem removeOneGem() {
        return (this.gemList.size()>0) ? this.gemList.remove(0) : null;
    }
}
