package kodap.hocuspocus.model.gems;

import java.io.Serializable;

/**
 * The Gem Class.
 * 
 * This class represents the model of a gem.
 * 
 * @author Vlad <vlad.marin@icloud.com>
 * @version 1.0
 */
public class Gem implements Serializable {
    // ATTRIBUTS
    private int ID;
    private static final long serialVersionUID = -7117863862113793074L;

    // CONSTRUCTOR
    /**
     * Default class constructor. The Gem needs an unique ID.
     * 
     * @param ID
     */
    public Gem(int ID) {
        this.setID(ID);
    }

    // GETTERS
    /**
     * This method returns the ID of the class.
     * 
     * @return unique ID of the Gem
     */
    public int getID() {
        return this.ID;
    }

    // SETTERS
    /**
     * This method sets a new ID for the class.
     * 
     * @param iD
     */
    public void setID(int iD) {
        ID = iD;
    }

    // METHODS
}
