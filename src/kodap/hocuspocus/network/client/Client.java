package kodap.hocuspocus.network.client;

import java.io.IOException;
import java.io.ObjectOutputStream;

import kodap.hocuspocus.model.Board;

/**
 * The Client class is used to send messages to the server
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class Client {
    /**
     * clientThread : ClientThread
     */
    public static ClientThread clientThread;

    /**
     * Is Host : boolean
     */
    public static boolean isHost;

    /**
     * Constructor
     * 
     * @param ip
     * @param pseudo
     */
    public Client(String ip, String pseudo) {
        Client.isHost = false;
        Client.clientThread = new ClientThread(ip, pseudo);
        Client.clientThread.start();
    }

    /**
     * Constructor
     * 
     * @param ip
     * @param pseudo
     * @param isHost
     */
    public Client(String ip, String pseudo, boolean isHost) {
        Client.isHost = isHost;
        Client.clientThread = new ClientThread(ip, pseudo);
        Client.clientThread.start();
    }

    /**
     * Get the pseudo of the client
     * 
     * @return String pseudo
     */
    public static String getPseudo() {
        return Client.clientThread.getPseudo();
    }

    /**
     * Set the pseudo of the client
     * 
     * @param pseudo
     */
    public static void setPseudo(String pseudo) {
        Client.clientThread.setPseudo(pseudo);
    }

    /**
     * Check whether or not if the client is connected at the given ip
     * 
     * @param ip
     * @return boolean
     */
    public static boolean isConnected(String ip) {
        if (ip.equals(Client.clientThread.getIp())
                && Client.clientThread.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Disconnect
     */
    public static void disconnect() {
        Client.clientThread.disconnect();
    }

    /**
     * Get the pseudo of the client
     * 
     * @param message
     */
    synchronized public static void sendMessage(String message) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                    Client.clientThread.getSocket().getOutputStream());
            writer.writeObject(message);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send the model to the server
     * 
     * @param board
     */
    synchronized public static void sendModel(Board board) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                    Client.clientThread.getSocket().getOutputStream());
            writer.writeObject(board);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
