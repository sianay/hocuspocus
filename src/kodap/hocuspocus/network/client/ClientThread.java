package kodap.hocuspocus.network.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

import kodap.hocuspocus.controller.client.ObjectReceiver;
import kodap.hocuspocus.exceptions.MessageNotUnderstoodException;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;

/**
 * The ClientThread is a thread which
 * <ul>
 * <li>own a socket to read/write to/from the server</li>
 * <li>has a 'pseudo' that a reel user will choose</li>
 * </ul>
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class ClientThread extends Thread {
    private Socket socket;
    private String pseudo;
    private String ip;

    /**
     * Constructor Ask for a connection to a server at 'ip' address and set the
     * pseudoHocusPocusClientManager
     * 
     * @param ip
     * @param pseudo
     */
    public ClientThread(String ip, String pseudo) {
        this.ip = ip;
        this.pseudo = pseudo;
        this.connect(ip);
    }

    /**
     * Connect a clientThread to the server
     * 
     * @param ip
     */
    public void connect(String ip) {
        try {
            SocketAddress sockaddr = new InetSocketAddress(InetAddress
                    .getByName(ip).getHostAddress(), 2013);
            this.socket = new Socket();
            this.socket.connect(sockaddr, 3000); // Timeout 3 seconds
        } catch (java.net.SocketException e) {
            this.socket = null;
            Tools.displayErrorWindow("Connexion impossible au serveur"); // server closed
        } catch (SocketTimeoutException e) {
            this.socket = null;
            Tools.displayErrorWindow("Connexion impossible au serveur"); // time out
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the socket of the ClientThread
     * 
     * @return Socket : the socket of the ClientThread
     */
    public Socket getSocket() {
        return this.socket;
    }

    /**
     * Check if a Client is successfully connected or not
     * 
     * @return boolean
     */
    public boolean isConnected() {
        return (this.socket != null) ? true : false;
    }

    /**
     * Disconnect
     */
    public void disconnect() {
        this.socket = null;
    }

    /**
     * Get the pseudo of the ClientThread
     * 
     * @return String : the pseudo of the ClientThread
     */
    public String getPseudo() {
        return this.pseudo;
    }

    /**
     * Set the pseudo of the ClientThread
     * 
     * @param pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     * Get the ip of the ClientThread
     * 
     * @return String : the ip of the ClientThread
     */
    public String getIp() {
        return this.ip;
    }

    @Override
    public void run() {
        if (this.socket == null) {
            // connection failed
        } else {
            // connection succeeded
            Object message = null;
            do {
                try {
                    ObjectInputStream objectInputStream = new ObjectInputStream(
                            this.socket.getInputStream());
                    message = objectInputStream.readObject();
                    if (message == null) {
                        this.serverCrash();
                        return;
                    }
                    try {
                        String stringMessage = (String) message;
                        try {
                            this.handle(stringMessage);
                        } catch (MessageNotUnderstoodException e) {
                            e.printStackTrace();
                        }
                    } catch (java.lang.ClassCastException e) {
                        Board board = (Board) message;
                        if (WindowsContainer.contains("BoardWindow")) {
                            ObjectReceiver.updateBoardWindow(board);
                        } else {
                            ObjectReceiver.initBoardWindow(board);
                        }
                    }
                } catch (IOException e) {
                    this.serverCrash();
                    return;
                } catch (ClassNotFoundException e) {
                    this.serverCrash();
                    return;
                }
            } while (message != null);
            this.serverCrash();
            return;
        }
    }

    /**
     * Things to do if the server crashed It displays an error and return to the
     * first window
     */
    private void serverCrash() {
        WindowsContainer.hideAllWindows();
        WindowsContainer.get("ChoiceWindow").setVisible(true);
        Tools.displayErrorWindow("La partie est finie!");
    }

    /**
     * Handle a message
     * 
     * @param String
     * @throws MessageNotUnderstoodException
     */
    private synchronized void handle(String message) throws MessageNotUnderstoodException {
        String messageParts[] = message.split("#");
        if (messageParts[0].equals("Error")) {
            messageParts = messageParts[1].split(":");
            Tools.displayErrorWindow(messageParts[1]);
            this.invokeMethod(messageParts, "Error");
        } else if (messageParts[0].equals("Info")) {
            Tools.displayInfoWindow(messageParts[1]);
        } else if (messageParts[0].equals("Action")) {
            messageParts = messageParts[1].split(":");
            this.invokeMethod(messageParts, "Action");
        } else {
            throw new MessageNotUnderstoodException(message);
        }
    }

    /**
     * Invoke a method after an error or an action message
     */
    private void invokeMethod(String[] messageParts, String type) {
        try {
            Method method;
            if (messageParts.length > 1) {
                // the method has 1 String argument
                method = ObjectReceiver.class.getMethod(
                        messageParts[0] + type, messageParts[1].getClass());
                method.invoke(null, messageParts[1]);
            } else {
                // the method has no argument
                method = ObjectReceiver.class
                        .getMethod(messageParts[0] + type);
                method.invoke(null);
            }
        } catch (NoSuchMethodException e) {
            if (type.equals("Error")) {
                // no method invoked bu the error
            } else {
                System.err
                        .println("The method "
                                + messageParts[0]
                                + type
                                + "() does not exist or is private or has not enough arguments");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
