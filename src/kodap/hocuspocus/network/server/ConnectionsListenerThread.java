package kodap.hocuspocus.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The ConnectionsListenerThread class take care of listening to client
 * connections requests, and initializing serverThreads with the clientPseudo
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class ConnectionsListenerThread extends Thread {
    private ServerSocket serverConnection;
    private boolean active;

    /**
     * Constructor
     * 
     * @param serverConnection
     */
    public ConnectionsListenerThread(ServerSocket serverConnection) {
        this.serverConnection = serverConnection;
        this.active = true;
    }

    @Override
    public void run() {
        while (active) {
            try {
                Socket socket = this.serverConnection.accept();
                if (active) {
                    ServerThread serverThread = this.createServerThread(socket);
                    this.askForPseudo(serverThread);
                } else {
                    Server.sendMessage(
                            "serverClosed:Connexion impossible au serveur",
                            MessageType.ERROR, socket);
                    this.serverConnection.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create a serverThread and start it
     * 
     * @param Socket
     */
    private ServerThread createServerThread(Socket socket) {
        ServerThread serverThread = new ServerThread(socket);
        serverThread.start();

        return serverThread;
    }

    /**
     * Send a message to the client to get his pseudo
     * 
     * @param ServerThread
     */
    private void askForPseudo(ServerThread serverThread) {
        Server.sendMessage("sendPseudo", MessageType.ACTION, serverThread);
    }

    /**
     * Close the listener
     */
    public void close() {
        this.active = false;
    }
}
