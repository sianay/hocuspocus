package kodap.hocuspocus.network.server;

/**
 * The MessagesType enum contains types of messages to be handle by the
 * MessagesManagers
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public enum MessageType {
    /**
     * ERROR
     * 
     * A message of type 'ERROR' will display an error window and can triggered
     * an action
     */
    ERROR,

    /**
     * ACTION
     * 
     * A message of type 'ACTION' must trigger an action
     */
    ACTION,

    /**
     * INFO
     * 
     * A message of type 'INFO' will display an info window
     */
    INFO
}
