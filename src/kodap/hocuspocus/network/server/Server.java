package kodap.hocuspocus.network.server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.exceptions.PortAlreadyUsedException;
import kodap.hocuspocus.model.Board;

/**
 * The server class take care of :
 * <ul>
 * <li>listening to connection requests (as soon as a Server object is
 * instantiated)</li>
 * <li>closing the connections</li>
 * <li>sending messages to one or all the clients</li>
 * <li>making a list of ServerThread available to communicate with the clients</li>
 * </ul>
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class Server {
    private ServerSocket serverConnection;

    /**
     * connectionListener ConnectionsListenerThread
     */
    public static ConnectionsListenerThread connectionListener;

    /**
     * serverThreads List<ServerThread>
     */
    public static List<ServerThread> serverThreads;

    /**
     * Constructor
     * 
     * @throws Exception
     */
    public Server() throws Exception {
        Server.serverThreads = new ArrayList<ServerThread>();
        try {
            this.launch();
        } catch (Exception e) {
            throw new PortAlreadyUsedException(e.getMessage());
        }
    }

    /**
     * Get the client pseudo list
     * 
     * @return List<String> : the client pseudo list
     */
    public static List<String> getClientPseudoList() {
        List<String> list = new ArrayList<String>();
        for (ServerThread serverThread : Server.serverThreads) {
            list.add(serverThread.getClientPseudo());
        }
    
        return list;
    }

    /**
     * Launch the server by opening a ServerSocket and listening to connection
     * requests
     * 
     * @throws Exception
     */
    private void launch() throws Exception {
        try {
            this.serverConnection = new ServerSocket(2013);
            Server.connectionListener = new ConnectionsListenerThread(
                this.serverConnection);
            Server.connectionListener.start();
    
        } catch (java.net.BindException e) {
            throw new PortAlreadyUsedException(2013);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a text message to all connected clients
     * 
     * @param text
     * @param messageType
     */
    synchronized public static void sendMessageToAll(String text, MessageType messageType) {
        String message = prepareMessage(text, messageType);
        for (ServerThread serverThread : Server.serverThreads) {
            Socket socket = serverThread.getSocket();
            try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(message);
            writer.flush();
            } catch (IOException e) {
            e.printStackTrace();
            }
        }
    }

    /**
     * Send a text message to a specific client
     * 
     * @param text
     * @param clientPseudo
     * @param messageType
     */
    synchronized public static void sendMessage(String text, MessageType messageType,
        String clientPseudo) {
        ServerThread serverThread = getServerThread(clientPseudo);
        Socket socket = serverThread.getSocket();
        String message = prepareMessage(text, messageType);
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(message);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a text message to a specific client
     * 
     * @param text
     * @param serverThread
     * @param messageType
     */
    synchronized public static void sendMessage(String text, MessageType messageType,
        ServerThread serverThread) {
        Socket socket = serverThread.getSocket();
        String message = prepareMessage(text, messageType);
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(message);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a text message to a specific client
     * 
     * @param text
     * @param socket
     * @param messageType
     */
    synchronized public static void sendMessage(String text, MessageType messageType,
        Socket socket) {
        String message = prepareMessage(text, messageType);
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(message);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a the model to a specific client
     * 
     * @param board
     */
     synchronized public static void sendModelToAll(Board board) {
        for (ServerThread serverThread : Server.serverThreads) {
            Socket socket = serverThread.getSocket();
            try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(board);
            writer.flush();
            } catch (IOException e) {
            e.printStackTrace();
            }
        }
    }

    /**
     * Send a the model to a specific client
     * 
     * @param board
     * @param clientPseudo
     */
    synchronized public static void sendModel(Board board, String clientPseudo) {
        ServerThread serverThread = getServerThread(clientPseudo);
        Socket socket = serverThread.getSocket();
        try {
            ObjectOutputStream writer = new ObjectOutputStream(
                socket.getOutputStream());
            writer.writeObject(board);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepare the message to be sent It adds a string specifying the message
     * type. It will be decoded afterwards, and forward to the good controller
     * 
     * @param String
     * @param MessageType
     */
    private static String prepareMessage(String message, MessageType messageType) {
        switch (messageType) {
        case ERROR:
            return "Error#" + message;
        case ACTION:
            return "Action#" + message;
        case INFO:
            return "Info#" + message;
        default:
            return "UNKNOWN ENUM";
        }
    }

    /**
     * Get a serverThread from a client pseudo
     * 
     * @param String
     * @return ServerThread serverThread
     */
    private static ServerThread getServerThread(String clientPseudo) {
        for (ServerThread serverThread : Server.serverThreads) {
            if (serverThread.getClientPseudo().equals(clientPseudo)) {
            return serverThread;
            }
        }

        return null;
    }

    /**
     * Get the pseudo list as a string
     * 
     * @return String pseudosList : the pseudos list, pseudos split by /
     */
    public static String getStringPseudoList() {
        String pseudos = "";
        List<String> pseudoList = Server.getClientPseudoList();
        for (String pseudo : pseudoList) {
            pseudos += pseudo + "/";
        }
        return pseudos;
    }

    /**
     * Close the server by closing the ServerSocket
     */
    public void close() {
        try {
            this.serverConnection.setReuseAddress(true);
            this.serverConnection.close();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the InetAddress of the server
     * 
     * @return InetAddress : the InetAddress of the server
     */
    public InetAddress getInetAddress() {
        return this.serverConnection.getInetAddress();
    }

    /**
     * Get the serverConnection of the server
     * 
     * @return ServerSocket : the serverConnection of the server
     */
    public ServerSocket getServerConnection() {
        return this.serverConnection;
    }
}
