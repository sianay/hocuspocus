package kodap.hocuspocus.network.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

import kodap.hocuspocus.controller.server.HocusPocusServerManager;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.tools.WindowsContainer;

/**
 * The ServerThread is a thread which
 * <ul>
 * <li>own a socket to read/write to/from the client</li>
 * <li>has a 'clientPseudo' as identifiant</li>
 * </ul>
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class ServerThread extends Thread {
    private Socket socket;
    private String clientPseudo;

    /**
     * Constructor
     * 
     * @param socket
     */
    public ServerThread(Socket socket) {
        this.clientPseudo = "Unkown client at "
                + socket.getInetAddress().getHostAddress();
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Object message = null;
            do {
                ObjectInputStream objectInputStream = new ObjectInputStream(
                        this.socket.getInputStream());
                message = objectInputStream.readObject();
                if (message == null) {
                    throw new IOException();
                }
                try {
                    String stringMessage = (String) message;
                    if (!handlePseudoRequestCommunication(stringMessage)) {
                        this.handle(stringMessage, this.getClientPseudo());
                    }
                } catch (java.lang.ClassCastException e) {
                    
                    if (message instanceof Board){
                        Board board = (Board) message;
                        HocusPocusServerManager.handleBoard(board);
                    }

                }
                // We let the controller handle the message if it's not about
                // pseudo requests
            } while (message != null);
        } catch (IOException e) {
            // The client disconnected. We inform others if we are waiting to play
            if (!WindowsContainer.contains("BoardWindow") || !WindowsContainer.get("BoardWindow").isVisible()) {
                if (Server.serverThreads.contains(this)) {
                    // TODO take care if in game(check if connexion is closed for example)
                    Server.serverThreads.remove(this);
                    Server.sendMessageToAll(
                        "updatePlayerList:" + Server.getStringPseudoList(),
                        MessageType.ACTION);
                }
            } else {
                // else we stop the server and the game is over
                Server.sendMessageToAll("deconnectedClient:"+this.getClientPseudo(), MessageType.ACTION);
            }
            return;
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    /**
     * Get the socket of the ServerThread
     * 
     * @return Socket : the socket of the ServerThread
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * Get the clientPseudo of the ServerThread
     * 
     * @return String : the clientPseudo of the ServerThread
     */
    public String getClientPseudo() {
        return clientPseudo;
    }

    /**
     * This fonction take care of starting a communication : the goal is to
     * retrieve the clientPseudo It won't be in a controller because at this
     * moment, the clientPseudo is unknown, but it will be needed after in the
     * controller
     * 
     * @param String
     * @return boolean : true if the message is a response to a pseudo request
     */
    private boolean handlePseudoRequestCommunication(String message) {
        String messageParts[] = message.split("#");
        if (messageParts[0].equals("pseudo_answer")) {
            if (Server.getClientPseudoList().contains(messageParts[1])) {
                Server.sendMessage(
                        "pseudoTaken:Votre pseudo est deja pris. Choisissez en un autre",
                        MessageType.ERROR, this);
            } else {
                this.clientPseudo = messageParts[1];
                Server.serverThreads.add(this);
                Server.sendMessageToAll(
                        "updatePlayerList:" + Server.getStringPseudoList(),
                        MessageType.ACTION);
            }
            return true;
        }
        return false;
    }

    /**
     * Handle a message
     * 
     * @param String
     * @param ServerThread
     */
    private void handle(String message, String clientPseudo) {
        String messageParts[] = message.split(":");
        try {
            Method method;
            if (messageParts.length > 1) {
                method = HocusPocusServerManager.class.getMethod(
                        messageParts[0] + "Action", messageParts[1].getClass(),
                        clientPseudo.getClass());
                method.invoke(null, messageParts[1], clientPseudo);
            } else {
                method = HocusPocusServerManager.class.getMethod(
                        messageParts[0] + "Action", clientPseudo.getClass());
                method.invoke(null, clientPseudo);
            }
        } catch (NoSuchMethodException e) {
            System.err
                    .println("The method "
                            + messageParts[0]
                            + "Action() does not exist or is private or has not enough arguments");
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
