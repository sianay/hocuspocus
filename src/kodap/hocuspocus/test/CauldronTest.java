/**
 * 
 */
package kodap.hocuspocus.test;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.gems.Cauldron;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class CauldronTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.gems.Cauldron#Cauldron(int)}.
	 */
	@Test
	public void testCauldronInt() {
		Cauldron cauldron = new Cauldron(4);
		//10 + (numberOfPlayers * 5)
		assertEquals("4 players = 30 gems", cauldron.getSize(), 30);
		Cauldron cauldron2 = new Cauldron(3);
		assertEquals("3 players = 25 gems", cauldron2.getSize(), 25);		
	}

}
