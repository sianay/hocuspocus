package kodap.hocuspocus.test;

import static org.junit.Assert.*;

import kodap.hocuspocus.model.cards.container.GameZone;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.hocus.HocusCard;
import kodap.hocuspocus.model.cards.pocus.Amulette;
import kodap.hocuspocus.model.cards.pocus.BaguetteMagique;
import kodap.hocuspocus.model.cards.pocus.PocusCard;

import org.junit.Test;

/**
 * GameZoneTest class is the test class of GameZone
 * 
 * @author Francois
 * 
 */

public class GameZoneTest {

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.cards.container.GameZone#getCardById(int)}.
     */
    @Test
    public void testGetCardById() {

        // Initialize
        GameZone gz = new GameZone();

        // Fill the list
        HocusCard hibou = new Hibou(1, 2);
        gz.addCard(hibou);
        PocusCard amulette = new Amulette (2);
        gz.addCard(amulette);

        assertTrue("top pocus card = amulette", gz.getTopPocusCard().equals(amulette));
        PocusCard baguette = new BaguetteMagique(3);
        gz.addCard(baguette);
        assertTrue("top pocus card =  baguette", gz.getTopPocusCard().equals(baguette));
    }

}
