package kodap.hocuspocus.test;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.pocus.Amulette;
import kodap.hocuspocus.model.cards.pocus.BaguetteMagique;


import org.junit.Test;

/**
 * @author Francois
 *
 */
public class HocusPocusClientManagerTest {

    /**
     */
    @Test
    public void testThrowPocusCardsToGraveyard() {
        Board board = new Board(2);
        board.getGameZone().addCard(new Hibou(1,0));
        board.getGameZone().addCard(new Amulette(1));
        board.getGameZone().addCard(new BaguetteMagique(1));

        assertTrue("If we search an other card", board.getGraveyard().getNumberOfCards() == 0);
    }
}
