package kodap.hocuspocus.test;

import java.util.Arrays;

import kodap.hocuspocus.controller.server.HocusPocusServerManager;
import kodap.hocuspocus.model.Board;

/**
 * HocusPocusServerManagerTest class is the test class of
 * HocusPocusServerManager
 * 
 * @author Francois
 * 
 */
public class HocusPocusServerManagerTest {

    /**
     * @return board
     */
    public Board getBoard() {
        Board board = new Board(7);
        return board;
    }

    /**
     * @return hpsm
     */
    public HocusPocusServerManager getHPServerManager() {
        HocusPocusServerManager hpsm = new HocusPocusServerManager(4,
                Arrays.asList("Pierre", "Marie", "Didier", "Mateo"));
        return hpsm;
    }

}
