package kodap.hocuspocus.test;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.container.Library;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.hocus.Malediction;
import kodap.hocuspocus.model.cards.hocus.Voleur;

import org.junit.Test;

/**
 * LibraryTest class is the test class of Library
 * 
 * @author Francois
 * 
 */
public class LibraryTest {

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.cards.container.Library#getCardById(int)}.
     */
    @Test
    public void testGetCardById() {
        // Initialize
        Library library = new Library(4);

        // Fill the list
        library.addCard(new Hibou(1, 2));
        library.addCard(new Malediction(2, 2));
        library.addCard(new Voleur(3, 2));

        // We want the card with the id 1
        Card card = library.getCardById(1);
        assertTrue("Classic case", card.getId() == 1);

        // If the card id does not exist in the list
        card = library.getCardById(5);
        assertTrue("If the card id does not exist in the list", card == null);
    }

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.cards.container.Library#removeTopCard()}.
     */
    @Test
    public void testRemoveTopCard() {

        // Initialize
        Library library = new Library(4);

        // Fill the list
        library.addCard(new Hibou(1, 2));
        library.addCard(new Malediction(2, 2));
        library.addCard(new Voleur(3, 2));

        library.removeTopCard();
        assertTrue("If we search the card deleted",
                library.getCardById(3) == null);
        assertTrue("If we search an other card", library.getCardById(2) != null);
    }

}
