package kodap.hocuspocus.test;

import java.util.ArrayList;
import java.util.List;

import kodap.hocuspocus.controller.server.HocusPocusServerManager;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * @author Anaïs Payet
 * 
 */
public class MainBoardTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        List<String> pseudo = new ArrayList<String>();
        pseudo.add("Anaïs");
        pseudo.add("Jorys");
        pseudo.add("François");
        pseudo.add("VladetBa");

        new HocusPocusServerManager(4, pseudo);

        new WindowsContainer();
        BoardWindow boardWindow = (BoardWindow) WindowsContainer
                .get("BoardWindow");
        boardWindow.init(HocusPocusServerManager.board);
        WindowsContainer.hideAllWindows();
        boardWindow.setVisible(true);
    }
}
