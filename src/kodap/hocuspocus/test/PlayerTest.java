package kodap.hocuspocus.test;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.container.Hand;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.hocus.Malediction;
import kodap.hocuspocus.model.cards.hocus.Voleur;

import org.junit.Test;

/**
 * PlayerTest class is the test class of Player
 * 
 * @author Francois
 * 
 */

public class PlayerTest {

    /**
     * Private method which returns a filled hand
     * 
     * @return
     */
    private Hand getHand() {
        // Initialize
        Hand ha = new Hand();

        // We add cards in the Hand
        ha.addCard(new Hibou(1, 2));
        ha.addCard(new Malediction(2, 2));
        ha.addCard(new Voleur(3, 2));

        return ha;
    }

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.Player#setHand(kodap.hocuspocus.model.cards.container.Hand)}
     * .
     */
    @Test
    public void testSetPlayerHand() {
        Hand hand = getHand();
        Player player = new Player();
        player.setHand(hand);
        assertTrue("The classic case", player.getHand().getCardById(1) != null);

        // Second Hand
        Hand handSecond = new Hand();
        handSecond.addCard(new Hibou(5, 2));
        handSecond.addCard(new Malediction(6, 3));
        handSecond.addCard(new Voleur(8, 2));
        player.setHand(handSecond);

        assertTrue("Get an old card", player.getHand().getCardById(1) == null);
        assertTrue("Get a new card (new hand)",
                player.getHand().getCardById(5) != null);
    }

}
