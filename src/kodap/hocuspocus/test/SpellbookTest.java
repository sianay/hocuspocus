package kodap.hocuspocus.test;

import static org.junit.Assert.*;

//import java.util.ArrayList;

//import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.container.Spellbook;
import kodap.hocuspocus.model.cards.hocus.Hibou;
import kodap.hocuspocus.model.cards.hocus.Malediction;
import kodap.hocuspocus.model.cards.hocus.Voleur;

import org.junit.Test;

/**
 * SpellBookTest class is the test class of Spellbook
 * 
 * @author Francois
 * 
 */
public class SpellbookTest {

    private Spellbook getSpellbook() {
        // Initialize
        Spellbook sp = new Spellbook();

        // We add cards in the spellbook
        sp.addCard(new Hibou(1, 2));
        sp.addCard(new Malediction(2, 2));
        sp.addCard(new Voleur(3, 2));

        return sp;
    }

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.cards.container.Spellbook#removeCardById(int)}
     * .
     */
    @Test
    public void testRemoveCardById() {
        Spellbook sp = getSpellbook();

        // Remove the card Hibou
        sp.removeCardById(1);

        assertTrue("The classic case", sp.getCardById(1) == null);
    }

    /**
     * Test method for
     * {@link kodap.hocuspocus.model.cards.container.Spellbook#getCardById(int)}
     * .
     */
    @Test
    public void testGetCardById() {
        Spellbook sp = getSpellbook();

        assertTrue("The classic case", sp.getCardById(1).getId() == 1);
    }

}
