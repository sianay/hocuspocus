package kodap.hocuspocus.test;

import kodap.hocuspocus.model.cards.hocus.*;
import kodap.hocuspocus.model.cards.pocus.*;

/**
 * @author Jorys
 * 
 */
public class TestCards {
    /**
     * @param args
     */
    public static void main(String[] args) {
        // Test all card getters
        Malediction c_MaledictionTest = new Malediction(20, 5);
        System.out.println("Création de la carte Malediction (id=20): ");
        System.out.println("   Id: " + c_MaledictionTest.getId());
        System.out.println("   Desc: " + c_MaledictionTest.getDescription());
        System.out.println("   Nb: " + c_MaledictionTest.getPowerNumber());
        System.out.println("   Im: " + c_MaledictionTest.getImage());
        System.out.println("");

        // Test all Pocus cards
        Amulette c_Amulette = new Amulette(1);
        System.out.println("Création de la carte Amulette (id=1): ");
        System.out.println("   Id: " + c_Amulette.getId());
        System.out.println("");

        BaguetteMagique c_BaguetteMagique = new BaguetteMagique(2);
        System.out.println("Création de la carte Baguette Magique (id=2): ");
        System.out.println("   Id: " + c_BaguetteMagique.getId());
        System.out.println("");

        ChatNoir c_ChatNoir = new ChatNoir(3);
        System.out.println("Création de la carte Chat Noir (id=3): ");
        System.out.println("   Id: " + c_ChatNoir.getId());
        System.out.println("");

        Citrouille c_Citrouille = new Citrouille(4);
        System.out.println("Création de la carte Citrouille (id=4): ");
        System.out.println("   Id: " + c_Citrouille.getId());
        System.out.println("");

        ContreSort c_ContreSort = new ContreSort(5);
        System.out.println("Création de la carte ContreSort (id=5): ");
        System.out.println("   Id: " + c_ContreSort.getId());
        System.out.println("");

        Eclair c_Eclair = new Eclair(6);
        System.out.println("Création de la carte Eclair (id=6): ");
        System.out.println("   Id: " + c_Eclair.getId());
        System.out.println("");

        MiroirEnchante c_MiroirEnchante = new MiroirEnchante(7);
        System.out.println("Création de la carte MiroirEnchanté (id=7): ");
        System.out.println("   Id: " + c_MiroirEnchante.getId());
        System.out.println("");

        Sablier c_Sablier = new Sablier(8);
        System.out.println("Création de la carte Sablier (id=8): ");
        System.out.println("   Id: " + c_Sablier.getId());
        System.out.println("");

        // Test all Hocus cards
        Abracadabra c_Abracadabra = new Abracadabra(9);
        System.out.println("Création de la carte Abracadabra (id=9): ");
        System.out.println("   Id: " + c_Abracadabra.getId());
        System.out.println("");

        BouleDeCristal c_BouleDeCristal = new BouleDeCristal(10);
        System.out.println("Création de la carte BouleDeCristal (id=10): ");
        System.out.println("   Id: " + c_BouleDeCristal.getId());
        System.out.println("");

        Hibou c_Hibou = new Hibou(11, 5);
        System.out.println("Création de la carte Hibou (id=11): ");
        System.out.println("   Id: " + c_Hibou.getId());
        System.out.println("");

        Inspiration c_Inspiration = new Inspiration(12, 5);
        System.out.println("Création de la carte Inspiration (id=12): ");
        System.out.println("   Id: " + c_Inspiration.getId());
        System.out.println("");

        Malediction c_Malediction = new Malediction(13, 5);
        System.out.println("Création de la carte Malediction (id=13): ");
        System.out.println("   Id: " + c_Malediction.getId());
        System.out.println("");

        Sacrifice c_Sacrifice = new Sacrifice(14, 5);
        System.out.println("Création de la carte Sacrifice (id=14): ");
        System.out.println("   Id: " + c_Sacrifice.getId());
        System.out.println("");

        Sortilege c_Sortilege = new Sortilege(15, 5);
        System.out.println("Création de la carte Sortilege (id=15): ");
        System.out.println("   Id: " + c_Sortilege.getId());
        System.out.println("");

        Voleur c_Voleur = new Voleur(16, 5);
        System.out.println("Création de la carte Voleur (id=16): ");
        System.out.println("   Id: " + c_Voleur.getId());
        System.out.println("");
    }
}
