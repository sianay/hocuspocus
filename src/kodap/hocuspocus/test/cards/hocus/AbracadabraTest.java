/**
 * 
 */
package kodap.hocuspocus.test.cards.hocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.Abracadabra;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class AbracadabraTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.Abracadabra#createAction(int, int, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCreateAction() {
        // Initialize
		Abracadabra abracadabra = new Abracadabra(50);
		Action actionAbra = abracadabra.createAction(60, 5, "John", "Eleven");
		
		assertTrue("playerSource = John", actionAbra.getPlayerSource() == "John");
		assertTrue("idCard = 60", actionAbra.getCardPlayed() == 60);
		assertTrue("number power = 0 and not 5", actionAbra.getNumber() == 0);
		assertTrue("playerTargeted = Eleven", actionAbra.getTargetPlayer() == "Eleven");
		
		assertTrue("target zone = Hand", actionAbra.getTargetZone() == Zone.HAND);		
		assertTrue("TypeTargeted = card", actionAbra.getTypeTargeted() == ObjectType.CARD);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionAbra.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionAbra.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionAbra.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionAbra.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionAbra.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionAbra.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = Eleven", actionAbra.getSourceContainerPlayer() == "Eleven");
		assertTrue("source container = hand", actionAbra.getSourceContainer() == Zone.HAND);
		assertTrue("destinationContainerPlayer = John", actionAbra.getDestinationContainerPlayer() == "John");
		assertTrue("getDestinationContainer = HAND", actionAbra.getDestinationContainer() == Zone.HAND);
		assertTrue("getDestinationContainer = ActionVerb.EXCHANGE", actionAbra.getActionVerb() == ActionVerb.EXCHANGE);
	}

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.Abracadabra#Abracadabra(int)}.
	 */
	@Test
	public void testAbracadabra() {
		Abracadabra abracadabra = new Abracadabra(50);
		//Check if the action initialized in the constructor is correct
		Action actionAbra = abracadabra.getAction();
		assertTrue("playerSource = null", actionAbra.getPlayerSource() == null);
		assertTrue("idCard = 50", actionAbra.getCardPlayed() == 50);
		assertTrue("playerTargeted = null", actionAbra.getTargetPlayer() == null);
		assertTrue("target zone = Hand", actionAbra.getTargetZone() == Zone.HAND);
		assertTrue("TypeTargeted = card", actionAbra.getTypeTargeted() == ObjectType.CARD);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionAbra.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionAbra.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionAbra.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionAbra.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionAbra.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionAbra.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = null", actionAbra.getSourceContainerPlayer() == null);
		assertTrue("source container", actionAbra.getSourceContainer() == Zone.HAND);
		assertTrue("number power = 0", actionAbra.getNumber() == 0);
		assertTrue("destinationContainerPlayer = null", actionAbra.getDestinationContainerPlayer() == null);
		assertTrue("getDestinationContainer = HAND", actionAbra.getDestinationContainer() == Zone.HAND);
		assertTrue("getDestinationContainer = ActionVerb.EXCHANGE", actionAbra.getActionVerb() == ActionVerb.EXCHANGE);
	}

}
