/**
 * 
 */
package kodap.hocuspocus.test.cards.hocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.BouleDeCristal;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class BouleDeCristalTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.BouleDeCristal#createAction(int, int, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCreateAction() {
        // Initialize
		BouleDeCristal bdc = new BouleDeCristal(50);
		Action actionBdc = bdc.createAction(60, 5, "John", "Eleven");
		
		assertTrue("playerSource = null", actionBdc.getPlayerSource() == "John");
		assertTrue("idCard = 60", actionBdc.getCardPlayed() == 60);

		assertTrue("playerTargeted = null", actionBdc.getTargetPlayer() == "_NO_");
		assertTrue("target zone = LIBRARY", actionBdc.getTargetZone() == Zone.LIBRARY);
		assertTrue("TypeTargeted = card", actionBdc.getTypeTargeted() == ObjectType.CARD);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionBdc.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionBdc.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionBdc.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionBdc.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionBdc.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionBdc.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = no", actionBdc.getSourceContainerPlayer() == "_NO_");
		assertTrue("source container = LIBRARY", actionBdc.getSourceContainer() == Zone.LIBRARY);
		assertTrue("number power = 4", actionBdc.getNumber() == 4);
		assertTrue("destinationContainerPlayer = no", actionBdc.getDestinationContainerPlayer() == "_NO_");
		assertTrue("getDestinationContainer = LIBRARY", actionBdc.getDestinationContainer() == Zone.LIBRARY);
		assertTrue("getDestinationContainer = ActionVerb.CHANGE_ORDER", actionBdc.getActionVerb() == ActionVerb.CHANGE_ORDER);
	}

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.BouleDeCristal#BouleDeCristal(int)}.
	 */
	@Test
	public void testBouleDeCristal() {
		BouleDeCristal bdc = new BouleDeCristal(50);
		//Check if the action initialized in the constructor is correct
		Action actionBdc = bdc.getAction();
		assertTrue("playerTargeted = null", actionBdc.getTargetPlayer() == "_NO_");
		assertTrue("target zone = LIBRARY", actionBdc.getTargetZone() == Zone.LIBRARY);
		assertTrue("TypeTargeted = card", actionBdc.getTypeTargeted() == ObjectType.CARD);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionBdc.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionBdc.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionBdc.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionBdc.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionBdc.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionBdc.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = no", actionBdc.getSourceContainerPlayer() == "_NO_");
		assertTrue("source container = LIBRARY", actionBdc.getSourceContainer() == Zone.LIBRARY);
		assertTrue("number power = 4", actionBdc.getNumber() == 4);
		assertTrue("destinationContainerPlayer = no", actionBdc.getDestinationContainerPlayer() == "_NO_");
		assertTrue("getDestinationContainer = LIBRARY", actionBdc.getDestinationContainer() == Zone.LIBRARY);
		assertTrue("getDestinationContainer = ActionVerb.CHANGE_ORDER", actionBdc.getActionVerb() == ActionVerb.CHANGE_ORDER);
	}

}
