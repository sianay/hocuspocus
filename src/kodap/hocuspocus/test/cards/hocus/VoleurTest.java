/**
 * 
 */
package kodap.hocuspocus.test.cards.hocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.ObjectType;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.Voleur;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class VoleurTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.Voleur#createAction(int, int, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCreateAction() {
        // Initialize
		Voleur voleur = new Voleur(50, 3);
		Action actionVol = voleur.createAction(60, 5, "John", "Eleven");
		
		assertTrue("playerSource = John", actionVol.getPlayerSource() == "John");
		assertTrue("idCard = 60", actionVol.getCardPlayed() == 60);
		assertTrue("number power =5 and not 3", actionVol.getNumber() == 5);
		assertTrue("playerTargeted = Eleven", actionVol.getTargetPlayer() == "Eleven");
		
		assertTrue("TypeTargeted = GEM", actionVol.getTypeTargeted() == ObjectType.GEM);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionVol.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionVol.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionVol.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionVol.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionVol.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionVol.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = Eleven", actionVol.getSourceContainerPlayer() == "Eleven");
		assertTrue("source container = CAULDRON", actionVol.getSourceContainer() == Zone.CAULDRON);

		assertTrue("destinationContainerPlayer = JOHN", actionVol.getDestinationContainerPlayer() == "John");
		assertTrue("getDestinationContainer = CAULDRON", actionVol.getDestinationContainer() == Zone.CAULDRON);
		assertTrue("getDestinationContainer = ActionVerb.TAKE", actionVol.getActionVerb() == ActionVerb.TAKE);
	}

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.hocus.Voleur#Voleur(int, int)}.
	 */
	@Test
	public void testVoleur() {
		Voleur voleur = new Voleur(50, 4);
		//Check if the action initialized in the constructor is correct
		Action actionVol = voleur.getAction();
		assertTrue("playerSource = null", actionVol.getPlayerSource() == null);
		assertTrue("idCard = 50", actionVol.getCardPlayed() == 50);
		assertTrue("playerTargeted = null", actionVol.getTargetPlayer() == null);
		assertTrue("target zone = CAULDRON", actionVol.getTargetZone() == Zone.CAULDRON);
		assertTrue("TypeTargeted = GEM", actionVol.getTypeTargeted() == ObjectType.GEM);
		assertTrue("finalDestinationCardPlayedPlayer = _NO_ ", actionVol.getFinalDestinationCardPlayedPlayer()== "_NO_");
		assertTrue("finalDestinationCardPlayed = graveyard", actionVol.getFinalDestinationCardPlayed() == Zone.GRAVEYARD);
		assertTrue("endPocusTurn = false", actionVol.isEndPocusTurn() == false);
		assertTrue("endHocusTurn = false", actionVol.isEndHocusTurn() == false);
		assertTrue("reset action = false", actionVol.isResetAction() == false);
		assertTrue("ListCardsToDisable = null", actionVol.getListCardsToDisable() == null);
		assertTrue("nameTargetedPlayer = null", actionVol.getSourceContainerPlayer() == null);
		assertTrue("source container = CAULDRON", actionVol.getSourceContainer() == Zone.CAULDRON);
		assertTrue("number power = 4", actionVol.getNumber() == 4);
		assertTrue("destinationContainerPlayer = null", actionVol.getDestinationContainerPlayer() == null);
		assertTrue("getDestinationContainer = CAULDRON", actionVol.getDestinationContainer() == Zone.CAULDRON);
		assertTrue("getDestinationContainer = ActionVerb.TAKE", actionVol.getActionVerb() == ActionVerb.TAKE);

	}

}
