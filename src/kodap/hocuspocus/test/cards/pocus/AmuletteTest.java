/**
 * 
 */
package kodap.hocuspocus.test.cards.pocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.hocus.Voleur;
import kodap.hocuspocus.model.cards.pocus.Amulette;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class AmuletteTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.pocus.Amulette#UpdateAction(kodap.hocuspocus.model.cards.actions.Action, java.lang.String)}.
	 */
	@Test
	public void testUpdateAction() {
		Voleur voleur = new Voleur(50, 4);
		Action actionVol = voleur.getAction();
		Player p1 = new Player();
		actionVol.setTargetPlayer(p1.getPseudo());
		
		Amulette amulette = new Amulette(17);
		
		//Check if the action initialized in the constructor is correct

		assertTrue("UpdateAction returns 0", amulette.UpdateAction(actionVol, p1.getPseudo()) == 0);
		assertFalse("UpdateAction returns -1", amulette.UpdateAction(actionVol, p1.getPseudo()) == -1);
	}

}
