/**
 * 
 */
package kodap.hocuspocus.test.cards.pocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.hocus.Voleur;
import kodap.hocuspocus.model.cards.pocus.BaguetteMagique;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class BaguetteMagiqueTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.pocus.BaguetteMagique#UpdateAction(kodap.hocuspocus.model.cards.actions.Action, java.lang.String)}.
	 */
	@Test
	public void testUpdateAction() {
		Voleur voleur = new Voleur(50, 4);
		Action actionVol = voleur.getAction();
		Player p1 = new Player();
		actionVol.setTargetPlayer(p1.getPseudo());
		
		BaguetteMagique bm = new BaguetteMagique(17);
		
		assertTrue("UpdateAction returns 0", bm.UpdateAction(actionVol, p1.getPseudo()) == 0);
		assertEquals("actionVol number = 8", actionVol.getNumber(), 8);
		assertEquals("actionVol EndPocusTurn updated to FALSE", actionVol.isEndPocusTurn(), true);
	}

}
