/**
 * 
 */
package kodap.hocuspocus.test.cards.pocus;

import static org.junit.Assert.*;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.actions.Action;
import kodap.hocuspocus.model.cards.actions.ActionVerb;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.Voleur;
import kodap.hocuspocus.model.cards.pocus.ChatNoir;

import org.junit.Test;

/**
 * @author Francois
 *
 */
public class ChatNoirTest {

	/**
	 * Test method for {@link kodap.hocuspocus.model.cards.pocus.ChatNoir#UpdateAction(kodap.hocuspocus.model.cards.actions.Action, java.lang.String)}.
	 */
	@Test
	public void testUpdateAction() {
		Voleur voleur = new Voleur(50, 4);
		Action actionVol = voleur.getAction();
		Player p1 = new Player();
		actionVol.setTargetPlayer(p1.getPseudo());
		
		ChatNoir cn = new ChatNoir(17);
		
		assertTrue("UpdateAction returns 0", cn.UpdateAction(actionVol, p1.getPseudo()) == 0);
		assertEquals("actionVol updated to TAKE", actionVol.getActionVerb(), ActionVerb.TAKE);
		assertEquals("actionVol FinalDestinationCardPlayedPlayer updated to player", actionVol.getFinalDestinationCardPlayedPlayer(), p1.getPseudo());
		assertEquals("actionVol FinalDestinationCardPlayed updated to ZONE HAND", actionVol.getFinalDestinationCardPlayed(), Zone.HAND);
		assertEquals("actionVol EndPocusTurn updated to TRUE", actionVol.isEndPocusTurn(), true);
	}

}
