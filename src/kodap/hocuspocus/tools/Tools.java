package kodap.hocuspocus.tools;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * The Tools class offer a set of usefull methods such as :
 * <ul>
 * <li>setWindowFirstAttributes : setting general attributes often used when
 * creating Jframe objects</li>
 * <li>validateIp : check whether an ip is valid or not</li>
 * <li>displayInfoWindow : display an informative window</li>
 * <li>displayErrorWindow : display an error window</li>
 * <li>getImageIcon : get an imageIcone object from the image's name</li>
 * </ul>
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class Tools {
    /**
     * Validate an ip Check whether an ip is valid or not
     * 
     * @param ip
     * @return boolean
     */
    public static boolean validateIp(String ip) {
        return ip
                .matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z");
    }

    /**
     * Hide all the windows The Join Server windows must be initialized with a
     * pseudo before calling this function
     * 
     * @param frame
     */
    public static void setWindowFirstAttributes(JFrame frame) {
        frame.setSize(1024, 768);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(Color.white);
    }

    /**
     * Display an informative window
     * 
     * @param text
     */
    public static void displayInfoWindow(String text) {
        JOptionPane.showMessageDialog(new JFrame(), text, "Information",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Display an error window
     * 
     * @param error
     */
    public static void displayErrorWindow(String error) {
        JOptionPane.showMessageDialog(new JPanel(), error, "Erreur",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Get an imageIcone object from an image's name The image must be placed in
     * the kodap.hocuspocus.view.images directory
     * 
     * @param imageName
     * @return ImageIcon
     */
    public static ImageIcon getImageIcon(String imageName) {
        BufferedImage myPicture;
        try {
            myPicture = ImageIO.read(Tools.class
                    .getResource("/kodap/hocuspocus/view/images/" + imageName));
            return new ImageIcon(myPicture);
        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Get an imageIcone object from an image's name, with possibility to resize
     * it The image must be placed in the kodap.hocuspocus.view.images directory
     * 
     * @param imageName
     * @param width
     * @param height
     * @return ImageIcon
     */
    public static ImageIcon getImageIcon(String imageName, int width, int height) {
        BufferedImage myPicture;
        try {
            myPicture = ImageIO.read(Tools.class
                    .getResource("/kodap/hocuspocus/view/images/" + imageName));
            myPicture = resizeImage(myPicture, width, height,
                    BufferedImage.TYPE_INT_ARGB);

            return new ImageIcon(myPicture);
        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Resize a BufferedImage
     * 
     * @param originalImage
     * @param width
     * @param height
     * @param type
     * @return BufferedImage
     * @throws IOException
     */
    public static BufferedImage resizeImage(BufferedImage originalImage,
            int width, int height, int type) throws IOException {
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY));
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return resizedImage;
    }

    /**
     * Remove mouse listeners from a component
     * 
     * @param c
     */
    public static void removeMouseListeners(Component c) {
         MouseListener[] mouseListeners = c.getMouseListeners();
         for (MouseListener ml : mouseListeners) {
             c.removeMouseListener(ml);
         }
    }
}