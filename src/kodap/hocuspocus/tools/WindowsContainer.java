package kodap.hocuspocus.tools;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

/**
 * The WindowsContainer class is used to retrieve JFrame and instantiate them
 * only once.
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class WindowsContainer {
	private static List<JFrame> windows;

	/**
	 * Constructor
	 */
	public WindowsContainer() {
		windows = new ArrayList<JFrame>();
	}

	/**
	 * Check whether or not a jFrame is part of the windowContainer
	 * 
	 * @param className
	 * @return true or false
	 */
	public static boolean contains(String className) {
		for (JFrame window : windows) {
			if (window.getClass().getSimpleName().equals(className)) {
				// Logo
				Image img = Tools.getImageIcon("icon.png", 48, 48).getImage();
				window.setIconImage(img);
				return true;
			}
		}

		return false;
	}

	/**
	 * Get a frame from the WindowsContainer
	 * 
	 * @param className
	 *            String
	 * @return JFrame
	 */
	public static JFrame get(String className) {
		// Loop on every windows. If a window with the class "className", we
		// return it.
		for (JFrame window : windows) {
			if (window.getClass().toString().contains(className)) {

				// Logo
				Image img = Tools.getImageIcon("icon.png", 48, 48).getImage();
				window.setIconImage(img);

				return window;
			}
		}

		// Try to instanciate a window with the class name.
		// That method does not allow to instance JFrames with parameters for
		// the constructor
		// In that cas, instanciate it then add it to the container
		try {
			JFrame window = (JFrame) Class
					.forName("kodap.hocuspocus.view.window." + className)
					.getConstructor().newInstance();
			WindowsContainer.add(window);

			// Logo
			Image img = Tools.getImageIcon("icon.png", 48, 48).getImage();
			window.setIconImage(img);

			return window;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Add a JFrame to the container
	 * 
	 * @param window
	 *            JFrame
	 */
	public static void add(JFrame window) {
		windows.add(window);
	}

	/**
	 * Hide all windows
	 */
	public static void hideAllWindows() {
		for (JFrame window : windows) {
			window.setVisible(false);
		}
	}

	/**
	 * 
	 * @param className
	 */
	public static void remove(String className) {
		int i = -1;
		for (JFrame window : windows) {
			if (window.getClass().getSimpleName().equals(className)) {
				i = windows.indexOf(window);
			}
		}
		windows.remove(i);
	}
}
