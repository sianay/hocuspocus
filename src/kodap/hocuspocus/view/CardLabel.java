package kodap.hocuspocus.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.AbstractBorder;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.view.listener.MouseCardListener;
import kodap.hocuspocus.view.listener.MouseMotionCardListener;

/**
 * A card label
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
public class CardLabel extends JLabel  {
    /**
     * Serial
     */
    private static final long serialVersionUID = 4289370112419947497L;
    private Card card;
    private int width;
    private int height;
    private JFrame window;

    /**
     * Constructor for cardLabel with default icon and size but no card
     * @param window : the window the cardLabel is in
     */
    public CardLabel(JFrame window) {
        this(null,window);
    }

    /**
     * Constructor for cardLabel with a card with a default size
     * 
     * @param card
     * @param window : the window the cardLabel is in
     */
    public CardLabel(Card card, JFrame window) {
        this(card,95,130, window);
    }

    /**
     * Constructor for cardLabel with no card with a custom size
     * 
     * @param width
     * @param height
     * @param window
     */
    public CardLabel(int width, int height, JFrame window) {
        this(null,width,height, window);
    }

    /**
     * Constructor for cardLabel with a card with a custom size
     * 
     * @param card
     * @param width
     * @param height
     * @param window
     */
    public CardLabel(Card card, int width, int height, JFrame window) {
        super(Tools.getImageIcon(card == null ? "missing-card.png":card.getImage(), width, height));
        this.setBorder(card == null ?  new DashedBorder() : BorderFactory.createLineBorder(Color.WHITE, 3));
        this.width = width;
        this.height = height;
        this.card = card;
        this.window = window;
        this.addMouseListener(new MouseCardListener());
        this.addMouseMotionListener(new MouseMotionCardListener(this.window));
    }

    /**
     * Get a card
     * 
     * @return Card
     */
    public Card getCard() {
        return card;
    }

    /**
     * Set card
     * 
     * @param card
     */
    public void setCard(Card card) {
        this.card = card;
        this.setIcon((card == null) ? Tools.getImageIcon("missing-card.png",this.width,this.height) : Tools.getImageIcon(card.getImage(),this.width,this.height));
        this.setBorder(card == null ?  new DashedBorder() : BorderFactory.createLineBorder(Color.WHITE, 3));
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height
     */
    public void setHeight(int height) {
        this.height = height;
    }
    

    @Override
    public String toString() {
        if (this.card != null) {
            return "CardLabel [card=" + this.card.getImage() + "]";
        } else {
            return "missing-card";
        }
    }
    
    @SuppressWarnings("serial")
    class DashedBorder extends AbstractBorder {
        @Override
        public void paintBorder(Component comp, Graphics g, int x, int y, int w, int h) {
            Graphics2D gg = (Graphics2D) g;
            gg.setColor(Color.WHITE);
            gg.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0));
            gg.drawRect(x, y, w - 1, h - 1);
        }
    }


}

