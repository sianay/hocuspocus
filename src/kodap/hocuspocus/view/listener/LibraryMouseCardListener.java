package kodap.hocuspocus.view.listener;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.window.MoveLibraryCardsWindow;

/**
 * LibraryLibraryMouseCardListener
 * 
 * This class includes the actions to be done when a mouse interact with a Card of the library (to change the order)
 * 
 * @author baptiste
 * @version 1.0
 */
public class LibraryMouseCardListener implements MouseListener {

    private MoveLibraryCardsWindow moveLibraryCardsWindow;
    private BufferedImage image;

    /**
     * Source card : must be static because shared by all instances of cardLabel
     * with a mouseCardsEvent
     */
    public static CardLabel sourceCardLabel;

    /**
     * Destination card : must be static because shared by all instances of
     * cardLabel with a mouseCardsEvent
     */
    public static CardLabel destinationCardLabel;

    /**
     * Constructor
     * 
     * @param window 
     */
    public LibraryMouseCardListener(MoveLibraryCardsWindow window) {
        this.moveLibraryCardsWindow = window;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        LibraryMouseCardListener.sourceCardLabel = (CardLabel) e.getComponent();

        if (sourceCardLabel.getCard() != null) {
            this.moveLibraryCardsWindow.setCursor(new Cursor(Cursor.MOVE_CURSOR));

            // Do DragNDrop representation
            Point location = (Point) e.getPoint().clone();
            SwingUtilities.convertPointToScreen(location, sourceCardLabel);
            SwingUtilities.convertPointFromScreen(location, MoveLibraryCardsWindow.glass);

            image = new BufferedImage(sourceCardLabel.getWidth(),
                sourceCardLabel.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics g = image.getGraphics();
            sourceCardLabel.paint(g);

            MoveLibraryCardsWindow.glass.setLocation(location);
            MoveLibraryCardsWindow.glass.setImage(image);
            MoveLibraryCardsWindow.glass.setVisible(true);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        // switch cards
        Card tempCard = LibraryMouseCardListener.destinationCardLabel.getCard();
        LibraryMouseCardListener.destinationCardLabel.setCard(LibraryMouseCardListener.sourceCardLabel.getCard());
        LibraryMouseCardListener.sourceCardLabel.setCard(tempCard);

        // Remove DragNDrop representation
        Component composant = e.getComponent();
        Point location = (Point) e.getPoint().clone();
        SwingUtilities.convertPointToScreen(location, composant);
        SwingUtilities.convertPointFromScreen(location, MoveLibraryCardsWindow.glass);
    
        MoveLibraryCardsWindow.glass.setLocation(location);
        MoveLibraryCardsWindow.glass.setImage(null);
        MoveLibraryCardsWindow.glass.setVisible(false);

        this.moveLibraryCardsWindow.setCursor(Cursor
            .getPredefinedCursor(Cursor.DEFAULT_CURSOR));

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        CardLabel cardLabel = (CardLabel) e.getComponent();
        LibraryMouseCardListener.destinationCardLabel = cardLabel;
    }

    @Override
    public void mouseExited(MouseEvent e) {}
}
