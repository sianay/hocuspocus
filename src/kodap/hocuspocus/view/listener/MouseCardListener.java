package kodap.hocuspocus.view.listener;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.HocusCard;
import kodap.hocuspocus.model.cards.pocus.PocusCard;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * MouseCardsEvents
 * 
 * This class includes the actions to be done when a mouse interact with a Card
 * (a CardLabel object)
 * 
 * @author baptiste
 * @author anais
 * @version 1.0
 */
public class MouseCardListener implements MouseListener {

    private BoardWindow boardWindow;
    private BufferedImage image;

    /**
     * Source card : must be static because shared by all instances of cardLabel
     * with a mouseCardsEvent
     */
    public static CardLabel sourceCardLabel;

    /**
     * Destination card : must be static because shared by all instances of
     * cardLabel with a mouseCardsEvent
     */
    public static CardLabel destinationCardLabel;
    
    /**
     * targetedPlayer : must be static because shared by all instances 
     */
    public static Player targetedPlayer;

    //For the case PocusCard MiroirEnchante => need to select a targeted player
    /**
     * MiroirEnchantePlaying : must be static because shared by all instances, note if the card played is M.E
     */
    public static boolean MiroirEnchantePlaying = false;
    /**
     * PocusPlayerPlaying: the player who is playing a pocus card
     */
    public static Player PocusPlayerPlaying;
    
    /**
     * Constructor
     */
    public MouseCardListener() {
        this.boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
        MouseCardListener.sourceCardLabel = (CardLabel) e.getComponent();

        
        if (sourceCardLabel.getCard() != null) {
            // check if the player is playing a card of his own game
            Zone sourceZone = this.boardWindow
                        .findZone(MouseCardListener.sourceCardLabel);
            Player sourcePlayer = this.getPlayer(
                        MouseCardListener.sourceCardLabel, sourceZone);
            if (sourcePlayer == null || !sourcePlayer.getPseudo().equals(Client.getPseudo())) {
                MouseCardListener.sourceCardLabel = null;
                return;
            }
            this.boardWindow.setCursor(new Cursor(Cursor.MOVE_CURSOR));

            // Do DragNDrop representation
            Point location = (Point) e.getPoint().clone();
            SwingUtilities.convertPointToScreen(location, sourceCardLabel);
            SwingUtilities.convertPointFromScreen(location, BoardWindow.glass);

            image = new BufferedImage(sourceCardLabel.getWidth(),
                    sourceCardLabel.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics g = image.getGraphics();
            sourceCardLabel.paint(g);

            BoardWindow.glass.setLocation(location);
            BoardWindow.glass.setImage(image);
            BoardWindow.glass.setVisible(true);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
        // Remove DragNDrop representation
        Component composant = e.getComponent();
        Point location = (Point) e.getPoint().clone();
        SwingUtilities.convertPointToScreen(location, composant);
        SwingUtilities.convertPointFromScreen(location, BoardWindow.glass);

        BoardWindow.glass.setLocation(location);
        BoardWindow.glass.setImage(null);
        BoardWindow.glass.setVisible(false);

        this.boardWindow.setCursor(Cursor
                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        // do nothing if no card were moved
        if (!this.noCardWereMoved()) {
            // do nothing if trying to move an unexisting card
            if (MouseCardListener.sourceCardLabel.getCard() != null) {
                Zone sourceZone = this.boardWindow.findZone(MouseCardListener.sourceCardLabel);
                Zone destinationZone = this.boardWindow.findZone(MouseCardListener.destinationCardLabel);
                Player sourcePlayer = this.getPlayer(MouseCardListener.sourceCardLabel, sourceZone);
                Player destinationPlayer = this.getPlayer(MouseCardListener.destinationCardLabel,destinationZone);

                // If trying to place a card, check if it's the current player or a hocus on the gamezone
                // a player not currently playing can place card on his spellbook or his hand
                if (!destinationZone.equals(Zone.GAMEZONE_POCUS) &&
                    !this.boardWindow.getBoard().getPlayingPlayer().getPseudo().equals(Client.getPseudo()) && 
                    (!destinationZone.equals(Zone.SPELLBOOK)&&(!destinationZone.equals(Zone.HAND)))) {
                        Tools.displayErrorWindow("Ce n'est pas à votre tour de jouer cette carte, mais vous pouvez jouer des pocus");
                        return;
                }
                // If trying to place a hocus, check if a hocus is already in place
                if (destinationZone.equals(Zone.GAMEZONE_HOCUS) &&
                    this.boardWindow.getBoard().getGameZone().getHocusCard() != null) {
                        Tools.displayErrorWindow("Il y a déjà une carte Hocus sur la zone de jeu");
                        return;
                }
                // check if the spellbook can be filled
                if (this.boardWindow.spellbookCanBeFilled()) {
                        // if so the only authorized action is to add a card to the spellbook
                        if (!(destinationZone.equals(Zone.SPELLBOOK) && destinationPlayer.getPseudo().equals(Client.getPseudo()))) {
                            Tools.displayErrorWindow("Vous devez remplir votre grimoire avant de faire une action");
                            return;
                        }
                }
                // check if the destination is not of another player
                if (destinationZone.equals(Zone.SPELLBOOK) && !destinationPlayer.getPseudo().equals(Client.getPseudo())) {
                    Tools.displayErrorWindow("Vous ne pouvez pas déplacer vos cartes sur celle d'un autre joueur");
                    return;
                }
                // If trying to place a pocus, check if a hocus is in place
                if (destinationZone.equals(Zone.GAMEZONE_POCUS)) {
                    if (this.boardWindow.getBoard().getGameZone().getHocusCard() == null) {
                        Tools.displayErrorWindow("Une carte Hocus doit etre placée avant de pouvoir placer une Pocus");
                        return;
                    }
                 // If the Pocus Card in the action of Hocus Card is null, we have to set the targetedPlayer
                        PocusCard pc = ((PocusCard)(MouseCardListener.sourceCardLabel.getCard()));
                        if (pc.getClass().getSimpleName().equals("MiroirEnchante"))
                        {
                        	if (!HocusPocusClientManager.board.getEvaluatedAction().getTargetPlayer().equals(Client.getPseudo())) {
                                Tools.displayErrorWindow("Vous ne pouvez pas jouer cette carte car vous n'etes pas attaqué");
                                return;
                            }
                            MiroirEnchantePlaying = true;
                            PocusPlayerPlaying = sourcePlayer;
                            HocusPocusClientManager.chooseTargetedPlayer();
                            if (targetedPlayer != null)
                                destinationPlayer = targetedPlayer;
                        }
                        if (pc.getClass().getSimpleName().equals("Amulette"))
                        {
                            targetedPlayer = HocusPocusClientManager.board.getPlayer(HocusPocusClientManager.board.getEvaluatedAction().getTargetPlayer());
                            System.out.println(HocusPocusClientManager.board.getEvaluatedAction().getTargetPlayer());
                            if (!HocusPocusClientManager.board.getEvaluatedAction().getTargetPlayer().equals(Client.getPseudo())) {
                                Tools.displayErrorWindow("Vous ne pouvez pas jouer cette carte car vous n'etes pas attaqué");
                                return;
                            }
                        }
                        if (pc.getClass().getSimpleName().equals("ChatNoir")||pc.getClass().getSimpleName().equals("Sablier"))
                        {
                            if (Client.getPseudo().equals(this.boardWindow.getBoard().getPlayingPlayer().getPseudo())){
                                Tools.displayErrorWindow("Vous ne pouvez pas jouer cette carte car c'est votre propre Hocus...");
                                return;
                            }
                        }
                }
                // If trying to place a hocus, check if a hocus is already in place
                if (destinationZone.equals(Zone.GAMEZONE_HOCUS)) {
                    if (this.boardWindow.getBoard().getGameZone().getHocusCard() != null) {
                        Tools.displayErrorWindow("Il y a déjà une carte Hocus sur la zone de jeu");
                        return;
                    }
                        // If targetPlayer in the action of Hocus Card is null, we have to set the targetedPlayer
                        HocusCard hc = ((HocusCard)(MouseCardListener.sourceCardLabel.getCard()));
                        if (hc.getAction().getTargetPlayer() == null)
                        {
                            HocusPocusClientManager.chooseTargetedPlayer();
                            if (targetedPlayer != null)
                            {
                                destinationPlayer = targetedPlayer;
                            }
                        }
                }

                String target = null;
                if (targetedPlayer != null) {
                    target = targetedPlayer.getPseudo();
                }
                HocusPocusClientManager.placeCard(sourcePlayer, destinationPlayer,
                        MouseCardListener.sourceCardLabel.getCard(), MouseCardListener.destinationCardLabel.getCard(),
                        sourceZone, destinationZone, target);
                         
                //set null for next actions
                destinationPlayer = null;
                MiroirEnchantePlaying = false;
                
                // TO DEBUG
                /*String sourceLocation = this.getStringLocation(sourcePlayer,
                        sourceZone);
                String destinationLocation = this.getStringLocation(
                        destinationPlayer, destinationZone);
                Tools.displayInfoWindow("Source : " + sourceLocation
                        + " & Destination : " + destinationLocation);*/
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        CardLabel cardLabel = (CardLabel) e.getComponent();
        MouseCardListener.destinationCardLabel = cardLabel;
        this.updateCardOnZoomPanel(cardLabel);
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    private void updateCardOnZoomPanel(CardLabel cardLabel) {
        try {
            this.boardWindow
                    .getZoomPanel()
                    .getCardLabel()
                    .setIcon(
                            Tools.getImageIcon(cardLabel.getCard().getImage(),
                                    180, 263));
        } catch (NullPointerException e) {
            // do nothing
        }
    }

    private Player getPlayer(CardLabel cardLabel, Zone zone) {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer
                .get("BoardWindow");
        return boardWindow.findPlayer(cardLabel, zone);
    }

    @SuppressWarnings("unused")
    private String getStringLocation(Player player, Zone zone) {
        if (player != null) {
            return zone.toString() + " - " + player.getPseudo();
        } else {
            return zone.toString();
        }
    }

    private boolean noCardWereMoved() {
        if (MouseCardListener.sourceCardLabel == null) {
            return true;
        }
        if (MouseCardListener.sourceCardLabel.getLocationOnScreen().equals(
            MouseCardListener.destinationCardLabel.getLocationOnScreen())) {
            return true;
        } else {
            return false;
        }
    }
}
