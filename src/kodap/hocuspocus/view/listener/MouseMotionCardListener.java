package kodap.hocuspocus.view.listener;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import kodap.hocuspocus.view.window.BoardWindow;
import kodap.hocuspocus.view.window.MoveLibraryCardsWindow;

/**
 * @author Anaïs Payet
 * @version 1.0
 */
public class MouseMotionCardListener extends MouseAdapter {

    private JFrame window;
    
    /**
     * Constructor
     * 
     * @param window
     */
    public MouseMotionCardListener(JFrame window) {
        this.window = window;
    }

    public void mouseDragged(MouseEvent event) {
        Component c = event.getComponent();
    
        Point p = (Point) event.getPoint().clone();
        SwingUtilities.convertPointToScreen(p, c);
        if (this.window instanceof BoardWindow) {
            SwingUtilities.convertPointFromScreen(p, BoardWindow.glass);
            BoardWindow.glass.setLocation(p);
            BoardWindow.glass.repaint();
        } else if (this.window instanceof MoveLibraryCardsWindow) {
            if (this.window instanceof MoveLibraryCardsWindow) {
                SwingUtilities.convertPointFromScreen(p, MoveLibraryCardsWindow.glass);
                MoveLibraryCardsWindow.glass.setLocation(p);
                MoveLibraryCardsWindow.glass.repaint();
            }
        }
    }

}