package kodap.hocuspocus.view.panel;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.container.Spellbook;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * BottomPlayerPanel
 * 
 * @author baptiste
 */
public class BottomPlayerPanel extends PlayerPanel implements ActionListener {

    private static final long serialVersionUID = 4465207651081685028L;
    private List<CardPanel> handCards;
    private JPanel buttonPanel;
    private JButton endTurnButton;
    private JButton declinePocusButton;

    /**
     * Decline pocus
     */
    public final static String NO_POCUS = "nopocus";
    /**
     * End turn
     */
    public final static String TERMINER = "terminer";

    /**
     * Constructor
     * 
     * @param player
     */
    public BottomPlayerPanel(Player player) {
        super(player);
    }

    protected void initVariables(Player player) {
        JFrame boardWindow = WindowsContainer.get("BoardWindow");
        super.initVariables(player);
        this.handCards = new ArrayList<CardPanel>();
        this.handCards.add(new CardPanel(new CardLabel(player.getHand()
                .getCardList().get(0), boardWindow)));
        this.handCards.add(new CardPanel(new CardLabel(player.getHand()
                .getCardList().get(1), boardWindow)));
        this.handCards.add(new CardPanel(new CardLabel(player.getHand()
                .getCardList().get(2), boardWindow)));
        this.handCards.add(new CardPanel(new CardLabel(boardWindow)));
        this.handCards.add(new CardPanel(new CardLabel(boardWindow)));
        this.initButtonPanel();
    }

    /**
     * Initializes the button panel
     */
    protected void initButtonPanel() {
        this.buttonPanel = new JPanel();

        this.endTurnButton = new JButton();
        enableButton(TERMINER, true);
        endTurnButton.setBorder(BorderFactory.createEmptyBorder());
        endTurnButton.setContentAreaFilled(false);

        this.setEndTurnButtonActivity();
        this.declinePocusButton = new JButton();
        enableButton(NO_POCUS, false);
        this.declinePocusButton.setBackground(new Color(0, 0, 0, 0));
        declinePocusButton.setBorder(BorderFactory.createEmptyBorder());
        declinePocusButton.setContentAreaFilled(false);
    }

    /**
     * Enable or disable the endTurnButton
     */
    public void setEndTurnButtonActivity() {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer
                .get("BoardWindow");
        // if the player is not the playing player, we disable the button
        if (!Client.getPseudo().equals(
                boardWindow.getBoard().getPlayingPlayer().getPseudo())) {
            enableButton(TERMINER, false);
            // if there are cards on the gamezone, we disable the button
        } else if (boardWindow.getBoard().getGameZone().containsCards()) {
            enableButton(TERMINER, false);
        } else {
            enableButton(TERMINER, true);
        }
    }

    protected void initFrameComponents() {
        super.initFrameComponents();

        for (CardPanel handCard : this.handCards) {
            handCard.setBackground(new Color(0, 0, 0, 0));
        }

        // Button
        this.buttonPanel.setLayout(new MigLayout("insets 0"));
        this.buttonPanel.setBackground(new Color(0, 0, 0, 0));

        this.endTurnButton.setMargin(new Insets(0, 0, 0, 0));
        this.declinePocusButton.setMargin(new Insets(0, 0, 0, 0));

        this.endTurnButton.setFont(new java.awt.Font("Geneva", 0, 12));
        this.declinePocusButton.setFont(new java.awt.Font("Geneva", 0, 12));

        this.endTurnButton.addActionListener(this);
        this.declinePocusButton.addActionListener(this);
    }

    protected void dispose() {
        String pseudoGap = Double.toString(50 - (this.playerPseudo.getText()
                .length() * 5));
        this.add(this.playerPanel, "width 16%, height 50%");
        this.playerPanel.add(this.playerPseudo,
                "width 100%, height 20%, gapleft " + pseudoGap + "%, wrap");
        this.playerPanel.add(this.playerIcon,
                "width 100%, height 80%, align center");

        this.add(this.gemPanel, "width 16%, height 50%");
        this.gemPanel.add(this.gemNumber,
                "width 100%, height 20%, gapleft 45%, wrap");
        this.gemPanel.add(this.gemIcon, "width 100%, height 80%, align center");

        this.add(this.handCardsPanel, "width 16%, height 50%");
        this.handCardsPanel.add(this.handCardsNumber,
                "width 100%, height 20%, gapleft 45%, wrap");
        this.handCardsPanel.add(this.handCardsIcon,
                "width 100%, height 80%, align center");
        int i = 0;
        for (CardPanel spellBookCardPanel : this.spellBookCards) {
            i++;
            if (i == 3) {
                this.add(spellBookCardPanel, "width 18%, height 50%, wrap");
            } else {
                this.add(spellBookCardPanel, "width 17%, height 50%");
            }
        }
        for (CardPanel handCard : this.handCards) {
            this.add(handCard, "width 17%, height 50%");
        }

        this.add(buttonPanel, "width 10%,height 50%");

        this.buttonPanel.add(this.endTurnButton, "width 100%,height 40%,wrap");
        this.buttonPanel.add(this.declinePocusButton, "width 100%,height 50%");
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.endTurnButton) {
            HocusPocusClientManager.chooseGemsOrCards();
        }

        if (arg0.getSource() == this.declinePocusButton) {
            if (HocusPocusClientManager.board.getPlayer(Client.getPseudo()).getSpellbook().getNumberOfCards() < Spellbook.SPELLBOOK_NUMBER 
                    && HocusPocusClientManager.board.getPlayer(Client.getPseudo()).getHand().getNumberOfCards()!=0){
                Tools.displayInfoWindow("Veuillez completer votre grimoire avant de cliquer sur ce bouton");
                return;
            }
            Client.sendMessage("declinePocus");
            enableButton(NO_POCUS, false);
        }
    }

    /**
     * Get the hand cards list
     * 
     * @return the handCards
     */
    public List<CardPanel> getHandCards() {
        return handCards;
    }

    /**
     * Set the hands cards list
     * 
     * @param handCards
     *            the handCards to set
     */
    public void setHandCards(List<CardPanel> handCards) {
        this.handCards = handCards;
    }

    /**
     * Change STATE of button 
     * @param buttonName
     * @param enable
     */
    public void enableButton(String buttonName, Boolean enable) {
        int w = 121;
        int h = 75;

        if (buttonName.equals(TERMINER)) {
            this.endTurnButton.setEnabled(enable);
            if (enable) {
                this.endTurnButton.setIcon(Tools.getImageIcon("terminer.png",
                        w, h));
            } else {
                this.endTurnButton.setIcon(Tools.getImageIcon(
                        "terminer_hover.png", w, h));
            }

        } else if (buttonName.equals(NO_POCUS)) {
            this.declinePocusButton.setEnabled(enable);
            if (enable) {
                this.declinePocusButton.setIcon(Tools.getImageIcon(
                        "nopocus.png", w, h));
            } else {
                this.declinePocusButton.setIcon(Tools.getImageIcon(
                        "nopocus_hover.png", w, h));
            }
        }
    }
}
