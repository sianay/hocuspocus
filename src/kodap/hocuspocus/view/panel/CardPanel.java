package kodap.hocuspocus.view.panel;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import kodap.hocuspocus.view.CardLabel;

/**
 * Panel for a card
 * 
 * @author baptiste
 */
public class CardPanel extends JPanel {
    private static final long serialVersionUID = -1375594870430041071L;

    private CardLabel cardLabel;

    /**
     * Constructor
     * 
     * @param cardLabel
     */
    public CardPanel(CardLabel cardLabel) {
        super();
        this.cardLabel = cardLabel;
        this.setLayout(new MigLayout("insets 0"));
        this.add(this.cardLabel, "span,height 100%, align center, wrap");
    }

    /**
     * Get the card label
     * 
     * @return the cardLabel
     */
    public CardLabel getCardLabel() {
        return cardLabel;
    }

    /**
     * Set the card label
     * 
     * @param cardLabel the cardLabel to set
     */
    public void setCardLabel(CardLabel cardLabel) {
        this.cardLabel = cardLabel;
    }
}
