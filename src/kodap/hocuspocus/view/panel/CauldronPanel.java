package kodap.hocuspocus.view.panel;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.model.gems.Cauldron;
import kodap.hocuspocus.tools.Tools;

/**
 * CauldronPanel
 * 
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class CauldronPanel extends JPanel {
    private static final long serialVersionUID = -4899416168502460618L;
    private TitleBarPanel titleBar;
    private JPanel zone;
    private JLabel gemNumber;
    private JLabel cauldronImg;
    private JLabel gemImg;

    /**
     * Constructor
     * 
     * @param cauldron 
     */
    public CauldronPanel(Cauldron cauldron) {
        super();
        initVariables(cauldron);
        setProperties();
        addComponents();
    }

    private void initVariables(Cauldron cauldron) {
        this.titleBar = new TitleBarPanel("CHAUDRON");
        this.zone = new JPanel();
        this.gemNumber = new JLabel(String.valueOf(cauldron.getSize()));
        this.cauldronImg = new JLabel(
                Tools.getImageIcon("cauldron.png", 80, 80));
        this.gemImg = new JLabel(Tools.getImageIcon("gem.png", 30, 40));
    }

    private void setProperties() {
        this.setLayout(new MigLayout("gap rel 1,insets 0", "grow"));
        this.setBackground(Color.BLACK);

        this.zone.setBackground(Color.WHITE);
        this.zone.setLayout(new MigLayout("gap rel 0,insets 0 10 0 0")); // T,
                                                                            // L,
                                                                            // B,
                                                                            // R

    }

    private void addComponents() {
        add(this.titleBar, "w 100%, h 10%, wrap");
        add(this.zone, "w 100%, h 90%");

        this.zone.add(cauldronImg, "w 60%, h 100%,span 1 2,center");
        this.zone.add(gemNumber, "align label,wrap,center,h 40%,gaptop 10");
        this.zone.add(gemImg, "h 60%,gapbottom 25");

    }

    /**
     * Get the gem number
     * 
     * @return the gem number
     */
    public JLabel getGemNumber() {
         return gemNumber;
    }

    /**
     * Set the gem number
     * 
     * @param gemNumber : the gem number
     */
    public void setGemNumber(JLabel gemNumber) {
        this.gemNumber = gemNumber;
    }
}
