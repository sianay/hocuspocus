package kodap.hocuspocus.view.panel;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;

/**
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class GameZonePanel extends JPanel {

    private static final long serialVersionUID = 8117748956923929620L;
    private TitleBarPanel titleBar;
    private JPanel zone;
    private CardLabel hocusLabel;
    private CardLabel pocusLabel;

    /**
     * Constructor
     */
    public GameZonePanel() {
        super();
        initVariables();
        setProperties();
        addComponents();
    }

    private void initVariables() {
        JFrame boardWindow = WindowsContainer.get("BoardWindow");
        this.titleBar = new TitleBarPanel("ZONE DE JEU");
        this.zone = new JPanel();
        this.hocusLabel = new CardLabel(110, 160, boardWindow);
        this.pocusLabel = new CardLabel(110, 160, boardWindow);
    }

    private void setProperties() {
        this.setLayout(new MigLayout("gap rel 1,insets 0", "grow"));
        this.setBackground(Color.BLACK);

        this.zone.setBackground(Color.WHITE);
        this.zone.setLayout(new MigLayout("gap rel 0,insets 20"));

    }

    private void addComponents() {
        add(this.titleBar, "w 100%, h 10%, wrap");
        add(this.zone, "w 100%, h 90%");

        this.zone.add(this.hocusLabel, "w 50%, h 100%, gapleft 10%");
        this.zone.add(this.pocusLabel, "w 50%, h 100%, center");
    }

    /**
     * Get the hocusLabel
     * 
     * @return the hocusLabel
     */
    public CardLabel getHocusLabel() {
        return hocusLabel;
    }

    /**
     * Set the hocusLabel
     * 
     * @param hocusLabel the hocusLabel to set
     */
    public void setHocusLabel(CardLabel hocusLabel) {
        this.hocusLabel = hocusLabel;
    }

    /**
     * Get the pocusLabel
     * 
     * @return the pocusLabel
     */
    public CardLabel getPocusLabel() {
        return pocusLabel;
    }

    /**
     * Set the pocusLabel
     * 
     * @param pocusLabel the pocusLabel to set
     */
    public void setPocusLabel(CardLabel pocusLabel) {
        this.pocusLabel = pocusLabel;
    }
}
