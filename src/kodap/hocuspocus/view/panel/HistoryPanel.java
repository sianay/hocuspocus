package kodap.hocuspocus.view.panel;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class HistoryPanel extends JPanel {

    private static final long serialVersionUID = -994510270645810624L;
    private JScrollPane scrollPane;
    @SuppressWarnings("rawtypes")
    private JList historyList;
    @SuppressWarnings("rawtypes")
    private DefaultListModel historyListModel;
    private TitleBarPanel titleBar;

    /**
     * Constructor
     */
    public HistoryPanel() {
        initVariables();
        setProperties();
        addComponents();
    }
   
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initVariables() {
        this.titleBar = new TitleBarPanel("HISTORIQUE DES ACTIONS");
        this.historyListModel = new DefaultListModel();
        this.historyListModel.addElement("La partie commence");
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        this.historyListModel.addElement(
            "C'est au tour de "+boardWindow.getBoard().getPlayingPlayer().getPseudo()+" de jouer !"
        );
        this.historyList = new JList(this.historyListModel);
        this.scrollPane = new JScrollPane(historyList);
    }

    private void setProperties() {
        this.setLayout(new MigLayout("gap rel 1,insets 0", "grow"));
    }

    private void addComponents() {
        add(this.titleBar, "w 100%, h 10%,wrap");
        add(this.scrollPane, "w 100%, h 90%");
    }

    /**
     * @return the historyList
     */
    @SuppressWarnings("rawtypes")
    public JList getHistoryList() {
        return historyList;
    }

    /**
     * @param historyList the historyList to set
     */
    @SuppressWarnings("rawtypes")
    public void setHistoryList(JList historyList) {
        this.historyList = historyList;
    }

    /**
     * @return the historyListModel
     */
    @SuppressWarnings("rawtypes")
    public DefaultListModel getHistoryListModel() {
        return historyListModel;
    }

    /**
     * @param historyListModel the historyListModel to set
     */
    @SuppressWarnings("rawtypes")
    public void setHistoryListModel(DefaultListModel historyListModel) {
        this.historyListModel = historyListModel;
    }
}
