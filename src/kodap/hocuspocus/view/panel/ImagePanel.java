package kodap.hocuspocus.view.panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @author Anaïs Payet
 * 
 */
public class ImagePanel extends JPanel {

    private static final long serialVersionUID = 8090662446103967169L;
    private Image img;

    /**
     * @param img
     */
    public ImagePanel(String img) {
        this(new ImageIcon(img).getImage());
    }

    /**
     * @param img
     */
    public ImagePanel(Image img) {
        this.img = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

}