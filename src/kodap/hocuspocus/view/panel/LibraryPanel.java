package kodap.hocuspocus.view.panel;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class LibraryPanel extends JPanel {

    private static final long serialVersionUID = 936902326308667192L;
    private TitleBarPanel titleBar;
    private JPanel zone;
    private JLabel pile;
    private JLabel numberOfCards;

    /**
     * Constructor
     */
    public LibraryPanel() {
        super();
        this.setLayout(new MigLayout("gap rel 0,insets 0", "grow"));
        initVariables();
        setProperties();
        addComponents();
    }

    private void initVariables() {
        this.titleBar = new TitleBarPanel("BIBLIOTHEQUE");
        this.zone = new JPanel();
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        this.numberOfCards = new JLabel(Integer.toString(boardWindow.getBoard().getLibrary().getNumberOfCards()));
        this.pile = new JLabel(
                Tools.getImageIcon("pile.png", 80, 112));
    }

    private void setProperties() {
        this.setLayout(new MigLayout("gap rel 1,insets 0"));
        this.setBackground(Color.BLACK);
        this.zone.setBackground(Color.WHITE);
        this.zone.setLayout(new MigLayout("gap rel 0,insets 0 10 0 0"));
    }

    private void addComponents() {
        add(this.titleBar, "w 100%, h 10%, wrap");
        add(this.zone, "w 100%, h 90%");
        this.zone.add(this.pile, "w 70%, h 100%");
        this.zone.add(this.numberOfCards, "w 30%, h 100%,align label,center");
    }

    /**
     * @return the numberOfCards
     */
    public JLabel getNumberOfCards() {
        return numberOfCards;
    }
}
