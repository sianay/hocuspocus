package kodap.hocuspocus.view.panel;


import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.window.BoardWindow;

/**
 * Represent a player Panel
 * 
 * @author baptiste
 */
public class PlayerPanel extends JPanel {
    /**
     * Serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * spellBookCards
     */
    protected List<CardPanel> spellBookCards;

    /**
     * playerPanel
     */
    protected JPanel playerPanel;

    /**
     * playerPseudo
     */
    protected JLabel playerPseudo;

    /**
     * playerIcon
     */
    protected JLabel playerIcon;

    /**
     * gemPanel
     */
    protected JPanel gemPanel;

    /**
     * gemNumber
     */
    protected JLabel gemNumber;

    /**
     * gemIcon
     */
    protected JLabel gemIcon;

    /**
     * handCardsPanel
     */
    protected JPanel handCardsPanel;

    /**
     * handCardsNumber
     */
    protected JLabel handCardsNumber;

    /**
     * handCardsIcon
     */
    protected JLabel handCardsIcon;

    /**
     * backGround Color
     */
    protected Color backgroundColor;

    /**
     * Constructor
     * 
     * @param player
     */
    public PlayerPanel(Player player) {
        super();
        this.setBackgroundColor(player);
        this.initVariables(player);
        this.initFrameComponents();
        this.dispose();
    
    }

    /**
     * Set background color
     * 
     * @param player 
     */
    public void setBackgroundColor(Player player) {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        if (player.getPseudo().equals(boardWindow.getBoard().getPlayingPlayer().getPseudo())) {
            this.backgroundColor = new Color(138, 43, 228,170);
             //current player
        } else {
            this.backgroundColor = new Color(45, 134,89,115);
        }
    }
    
    /**
     * Initializes variables
     */
    protected void initVariables(Player player) {
        JFrame boardWindow = WindowsContainer.get("BoardWindow");
        this.spellBookCards = new ArrayList<CardPanel>();
        this.spellBookCards.add(
            new CardPanel(new CardLabel(player.getSpellbook().getCardList().get(0), boardWindow))
        );
        this.spellBookCards.add(
            new CardPanel(new CardLabel(player.getSpellbook().getCardList().get(1), boardWindow))
        );
        this.spellBookCards.add(
            new CardPanel(new CardLabel(player.getSpellbook().getCardList().get(2), boardWindow))
        );
        this.playerPanel = new JPanel();
        this.playerPseudo = new JLabel(player.getPseudo());
        this.playerIcon = new JLabel(Tools.getImageIcon("b1.png", 80, 80));
        this.gemPanel = new JPanel();
        this.gemNumber = new JLabel(Integer.toString(player.getCauldron()
                .getSize()));
        this.gemIcon = new JLabel(Tools.getImageIcon("gem.png", 50, 60));
        this.handCardsPanel = new JPanel();
        this.handCardsNumber = new JLabel(Integer.toString(player.getHand()
                .getNumberOfCards()));
        this.handCardsIcon = new JLabel(Tools.getImageIcon("cartes.png", 99, 70));
    }

    /**
     * Initializes the components of the frame (setters, add action listeners,
     * etc.)
     */
    protected void initFrameComponents() {
        this.setBackground(this.backgroundColor);
        this.setLayout(new MigLayout("insets 5 0 0 0"));
        this.playerPanel.setLayout(new MigLayout("insets 0"));
        this.gemPanel.setLayout(new MigLayout("insets 0"));
        this.handCardsPanel.setLayout(new MigLayout("insets 0"));
        for (CardPanel spellBookCardPanel : this.spellBookCards) {
            spellBookCardPanel.setLayout(new MigLayout("insets 0"));
            spellBookCardPanel.setBackground(new Color(0,0,0,0));
        }
        
        this.gemPanel.setBackground(new Color(0,0,0,0));
        this.playerPanel.setBackground(new Color(0,0,0,0));
        this.handCardsPanel.setBackground(new Color(0,0,0,0));
    }

    /**
     * Disposes the components on the frame
     */
    protected void dispose() {
        String pseudoGap = Double.toString(50 - (this.playerPseudo.getText()
                .length() * 5));
        this.add(this.playerPanel, "width 16%, height 100%");
        this.playerPanel.add(this.playerPseudo,
                "width 100%, height 20%, gapleft " + pseudoGap + "%, wrap");
        this.playerPanel.add(this.playerIcon,
                "width 100%, height 80%, align center");

        this.add(this.gemPanel, "width 16%, height 100%");
        this.gemPanel.add(this.gemNumber,
                "width 100%, height 20%, gapleft 45%, wrap");
        this.gemPanel.add(this.gemIcon, "width 100%, height 80%, align center");

        this.add(this.handCardsPanel, "width 16%, height 100%");
        this.handCardsPanel.add(this.handCardsNumber,
                "width 100%, height 20%, gapleft 45%, wrap");
        this.handCardsPanel.add(this.handCardsIcon,
                "width 100%, height 80%, align center");
        int i = 0;
        for (CardPanel spellBookCardPanel : this.spellBookCards) {
            i++;
            if (i == 3) {
                this.add(spellBookCardPanel, "width 18%, height 100%, wrap");
            } else {
                this.add(spellBookCardPanel, "width 17%, height 100%");
            }
        }
    }

    // GETTERS & SETTERS

    /**
     * Get player Pseudo
     * 
     * @return playerPseudo
     */
    public JLabel getPlayerPseudo() {
        return playerPseudo;
    }

    /**
     * Set player Pseudo
     * 
     * @param playerPseudo
     */
    public void setPlayerPseudo(JLabel playerPseudo) {
        this.playerPseudo = playerPseudo;
    }

    /**
     * Get the gem number
     * 
     * @return the gemNumber
     */
    public JLabel getGemNumber() {
        return gemNumber;
    }

    /**
     * Set the gem number
     * 
     * @param gemNumber the gemNumber to set
     */
    public void setGemNumber(JLabel gemNumber) {
        this.gemNumber = gemNumber;
    }

    /**
     * Get the hand cards number
     * 
     * @return the handCardsNumber
     */
    public JLabel getHandCardsNumber() {
        return handCardsNumber;
    }

    /**
     * Set the hand cards number
     * 
     * @param handCardsNumber the handCardsNumber to set
     */
    public void setHandCardsNumber(JLabel handCardsNumber) {
        this.handCardsNumber = handCardsNumber;
    }

    /**
     *  Get the spell books cards
     * 
     * @return the spellBookCards
     */
    public List<CardPanel> getSpellBookCards() {
        return spellBookCards;
    }

    /**
     * Set the spell books cards
     * 
     * @param spellBookCards the spellBookCards to set
     */
    public void setSpellBookCards(List<CardPanel> spellBookCards) {
        this.spellBookCards = spellBookCards;
    }

    /**
     * @return the backgroundColor
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
