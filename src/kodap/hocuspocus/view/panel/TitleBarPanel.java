package kodap.hocuspocus.view.panel;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class for title bar graphic component
 * 
 * @author Anaïs Payet
 * @version 1.0
 */
public class TitleBarPanel extends JPanel {

    private static final long serialVersionUID = -8668923178674106340L;
    private JLabel titleLabel;

    /**
     * Constructor
     * 
     * @param title
     * 
     */
    public TitleBarPanel(String title) {

        setBackground(new Color(218,236,198));
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        // Init and add component
        this.titleLabel = new JLabel(title);
        add(this.titleLabel);
    }

    /**
     * Getter Title
     * @return String Title 
     */
    public String getTitleBar() {
        return titleLabel.getText();
    }
    
    /**
     * Change the background color 
     * @param bg 
     */
    public void changeColor(Color bg){
        setBackground(bg);
    }
}
