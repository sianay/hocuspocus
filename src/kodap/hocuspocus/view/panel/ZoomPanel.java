package kodap.hocuspocus.view.panel;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.tools.Tools;

/**
 * @author Anaïs Payet
 * @version 1.0
 * 
 */
public class ZoomPanel extends JPanel {

    private static final long serialVersionUID = -5697142134404823361L;
    private TitleBarPanel titleBar;
    private JPanel zone;
    private JLabel cardLabel;

    /**
     * Constructor
     */
    public ZoomPanel() {
        super();
        initVariables();
        setProperties();
        addComponents();
    }

    private void initVariables() {
        this.titleBar = new TitleBarPanel("ZONE DE ZOOM");
        this.zone = new JPanel();
        this.cardLabel = new JLabel(Tools.getImageIcon("missing-card.png", 180,
                263));
    }

    private void setProperties() {
        this.setLayout(new MigLayout("gap rel 1,insets 0", "grow"));
        this.setBackground(Color.BLACK);
        this.zone.setBackground(Color.WHITE);
        this.zone.setLayout(new MigLayout("gap rel 0,insets 5 15 5 15")); // T,
                                                                            // L,
                                                                            // B,
                                                                            // R.
    }

    private void addComponents() {
        add(this.titleBar,"w 100%, h 5%,wrap");
        add(this.zone,"w 100%, h 95%");
        this.zone.add(this.cardLabel,"w 100%,h 100%,split, span, align center");
    }

    /**
     * @return the cardLabel
     */
    public JLabel getCardLabel() {
        return cardLabel;
    }

    /**
     * @param cardLabel
     */
    public void setCardLabel(JLabel cardLabel) {
        this.cardLabel = cardLabel;
    }
}
