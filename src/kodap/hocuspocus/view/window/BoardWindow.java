package kodap.hocuspocus.view.window;

import java.awt.Color;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.actions.Zone;
import kodap.hocuspocus.model.cards.hocus.HocusCard;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.listener.MouseCardListener;
import kodap.hocuspocus.view.panel.BottomPlayerPanel;
import kodap.hocuspocus.view.panel.CardPanel;
import kodap.hocuspocus.view.panel.CauldronPanel;
import kodap.hocuspocus.view.panel.GameZonePanel;
import kodap.hocuspocus.view.panel.GlassCardPane;
import kodap.hocuspocus.view.panel.HistoryPanel;
import kodap.hocuspocus.view.panel.ImagePanel;
import kodap.hocuspocus.view.panel.LibraryPanel;
import kodap.hocuspocus.view.panel.PlayerPanel;
import kodap.hocuspocus.view.panel.TitleBarPanel;
import kodap.hocuspocus.view.panel.ZoomPanel;
import net.miginfocom.swing.MigLayout;

/**
 * Work in progress...
 */
@SuppressWarnings("serial")
public class BoardWindow extends JFrame implements MouseListener {
    private Board board;
    private JPanel globalPanel;
    private ImagePanel leftPanel;
    private JPanel rightPanel;
    private List<PlayerPanel> playerPanelList;
    private BottomPlayerPanel bottomPlayerPanel;

    /**
     * Card representation for DragNDrop
     */
    public static GlassCardPane glass = new GlassCardPane();

    // Right panel components
    private CauldronPanel cauldronPanel;
    private HistoryPanel historyPanel;
    private LibraryPanel libraryPanel;
    private ZoomPanel zoomPanel;
    private GameZonePanel gameZonePanel;
    private TitleBarPanel helpBar;
    private TitleBarPanel rulesBar;

    /**
     * Constructor
     */
    public BoardWindow() {
        Tools.setWindowFirstAttributes(this);
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.globalPanel = new JPanel();
        this.leftPanel = new ImagePanel(Tools.getImageIcon("fond-player.jpg",
                665, 745).getImage());
        this.rightPanel = new JPanel();
        initLeftPanelVariables();
        initRightPanelVariables();
    }

    /**
     * Init the boardWindow with a board
     * 
     * @param board
     */
    public void init(Board board) {
        this.board = board;
        this.initVariables();
        this.initFrameComponents();
        this.disposeComponents();
        this.pack();
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }


    /**
     * Initializes variables for the right panel
     */
    private void initRightPanelVariables() {
        this.cauldronPanel = new CauldronPanel(this.board.getCauldron());
        this.historyPanel = new HistoryPanel();
        this.libraryPanel = new LibraryPanel();
        this.zoomPanel = new ZoomPanel();
        this.gameZonePanel = new GameZonePanel();
        this.helpBar = new TitleBarPanel("AIDE");
        this.rulesBar = new TitleBarPanel("RÈGLES DU JEU");
        this.helpBar.changeColor(new Color(234,174,174));
        this.helpBar.addMouseListener(this);
        this.rulesBar.changeColor(new Color(234,174,174));
        this.rulesBar.addMouseListener(this);
    }

    /**
     * Initializes variables for the left panel
     */
    private void initLeftPanelVariables() {
        this.playerPanelList = new ArrayList<PlayerPanel>();
        for (Player player : this.board.getPlayerList()) {
            if (!player.getPseudo().equals(Client.getPseudo())) {
                this.playerPanelList.add(new PlayerPanel(player));
            } else {
                this.bottomPlayerPanel = new BottomPlayerPanel(player);
            }
        }
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        String playerType = (Client.isHost) ? "Hote" : "Joueur";
        this.setTitle("Fenetre de jeu - " + Client.getPseudo() + " - "
                + playerType);
        this.setContentPane(this.globalPanel);
        this.globalPanel.setLayout(new MigLayout("insets 0"));
        this.leftPanel.setLayout(new MigLayout("insets 0"));
        this.rightPanel.setLayout(new MigLayout("gap rel 1,insets 1"));
        this.globalPanel.setBackground(Color.BLACK);
        this.rightPanel.setBackground(Color.BLACK);
    }

    /**
     * Dispose the components on the frame
     */
    private void disposeComponents() {
        this.globalPanel.add(this.leftPanel, "w 65%, h 100%");
        this.globalPanel.add(this.rightPanel, "w 35%, h 100%");
        setGlassPane(glass);
        disposeLeftComponents();
        disposeRightComponents();
    }

    /**
     * Dispose the components on the frame for the right panel
     */
    private void disposeRightComponents() {
        this.rightPanel.add(this.helpBar, "w 40%,h 3%");
        this.rightPanel.add(this.rulesBar, "w 60%,h 3%,wrap");
        this.rightPanel.add(this.cauldronPanel, "w 40%, h 20%");
        this.rightPanel.add(this.zoomPanel, "w 60%, h 40%,wrap,span 1 2");
        this.rightPanel.add(this.libraryPanel, "w 40%, h 20%,wrap");
        this.rightPanel.add(this.gameZonePanel, "w 100%, h 30%, wrap,span");
        this.rightPanel.add(this.historyPanel, "w 100%, h 27%,span ,wrap");
    }

    /**
     * Dispose the components on the frame for the left panel
     */
    private void disposeLeftComponents() {
        int i = 0;
        for (PlayerPanel playerPanel : this.playerPanelList) {
            if (!playerPanel.getPlayerPseudo().getText()
                    .equals(Client.getPseudo())) {
                i++;
                this.leftPanel.add(playerPanel, "width 100%, height 15%, wrap");
            }
        }
        this.leftPanel.add(bottomPlayerPanel, "width 100%, height 40%, gaptop"
                + Integer.toString(((3 - i) * 19)-(i*2)) + "%");
    }

    /**
     * Update the boardWindow with a board
     * 
     * @param board
     */
    public void update(Board board) {
        this.board = board;
        this.cauldronPanel.getGemNumber().setText(
                Integer.toString(this.board.getCauldron().getSize()));
        this.updateGameZonePanel();
        this.updateLibraryPanel();
        for (PlayerPanel playerPanel : this.playerPanelList) {
            this.updatePlayerPanel(playerPanel);
        }
        this.updateBottomPlayerPanel();
        
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }

    /**
     * Update the gameZonePanel with the board data
     */
    private void updateGameZonePanel() {
        this.gameZonePanel.getHocusLabel().setCard(
                this.board.getGameZone().getHocusCard());
        this.gameZonePanel.getPocusLabel().setCard(
                this.board.getGameZone().getTopPocusCard());
    }

    /**
     * Update the libraryPanel with the board data
     */
    private void updateLibraryPanel() {
        this.libraryPanel.getNumberOfCards().setText(
                Integer.toString(this.board.getLibrary().getNumberOfCards()));
    }

    /**
     * Update the playerPanel for competitors with the board data
     */
    private void updatePlayerPanel(PlayerPanel playerPanel) {
        Player player = board
                .getPlayer(playerPanel.getPlayerPseudo().getText());
        playerPanel.setBackgroundColor(player);
        playerPanel.setBackground(playerPanel.getBackgroundColor());
        // update gem number
        playerPanel.getGemNumber().setText(
                Integer.toString(player.getCauldron().getSize()));
        // update card in hand number
        playerPanel.getHandCardsNumber().setText(
                Integer.toString(player.getHand().getNumberOfCards()));
        // update spellbook
        int i = 0;
        for (i = 0; i < player.getSpellbook().getCardList().size(); i++) {
            Card card = player.getSpellbook().getCardList().get(i);
            playerPanel.getSpellBookCards().get(i).getCardLabel().setCard(card);
        }
        // if there are less than 3 cards in the spell book, we set the rest
        // with missing cards
        while (i < 3) {
            playerPanel.getSpellBookCards().get(i).getCardLabel().setCard(null);
            i++;
        }
    }

    /**
     * Update the main playerPanel with the board data
     */
    private void updateBottomPlayerPanel() {
        Player player = board.getPlayer(Client.getPseudo());
        this.updatePlayerPanel(this.bottomPlayerPanel);
        // update hand cards
        int i = 0;
        for (i = 0; i < player.getHand().getCardList().size(); i++) {
            Card card = player.getHand().getCardList().get(i);
            this.bottomPlayerPanel.getHandCards().get(i).getCardLabel()
                    .setCard(card);
        }
        // if there are less than 5 cards in the spell book, we set the rest
        // with missing cards
        while (i < 5) {
            this.bottomPlayerPanel.getHandCards().get(i).getCardLabel()
                    .setCard(null);
            i++;
        }
        this.bottomPlayerPanel.setEndTurnButtonActivity();
        if (!board.getGameZone().containsCards()) {
            this.bottomPlayerPanel.enableButton("nopocus", false);
        }/* else {
            this.bottomPlayerPanel.enableButton("nopocus", true);
        }*/
    }

    /**
     * Find the player who has a cardLabel
     * 
     * @param cardLabel
     * @param zone
     * @return the player
     */
    public Player findPlayer(CardLabel cardLabel, Zone zone) {
        if (zone.toString().equals(Zone.HAND.toString())) {
            return this.board.getPlayer(Client.getPseudo());
        }
        for (PlayerPanel playerPanel : this.playerPanelList) {
            for (CardPanel cardPanel : playerPanel.getSpellBookCards()) {
                if (cardPanel.getCardLabel().equals(cardLabel)) {
                    return this.board.getPlayer(playerPanel.getPlayerPseudo()
                            .getText());
                }
            }
        }
        // we did not find the player but the zone is SpellBook so it's the
        // current player
        if (zone.toString().equals(Zone.SPELLBOOK.toString())) {
            return this.board.getPlayer(Client.getPseudo());
        }

        return null;
    }

    /**
     * Find the zone containing the card label
     * 
     * The cardLabel can be in a SPELLBOOK, a HAND or the GAMEZONE
     * 
     * @param cardLabel
     * @return the zone
     */
    public Zone findZone(CardLabel cardLabel) {
        // look in the Spellbooks
        for (PlayerPanel playerPanel : this.playerPanelList) {
            for (CardPanel cardPanel : playerPanel.getSpellBookCards()) {
                if (cardPanel.getCardLabel().equals(cardLabel)) {
                    return Zone.SPELLBOOK;
                }
            }
        }
        for (CardPanel cardPanel : this.bottomPlayerPanel.getSpellBookCards()) {
            if (cardPanel.getCardLabel().equals(cardLabel)) {
                return Zone.SPELLBOOK;
            }
        }
        // look in the hand
        for (CardPanel cardPanel : this.bottomPlayerPanel.getHandCards()) {
            if (cardPanel.getCardLabel().equals(cardLabel)) {
                return Zone.HAND;
            }
        }
        // look in the game zone
        if (this.gameZonePanel.getHocusLabel().equals(cardLabel)
                || this.gameZonePanel.getPocusLabel().equals(cardLabel)) {
            if (MouseCardListener.sourceCardLabel != null) {
                if (MouseCardListener.sourceCardLabel.getCard() instanceof HocusCard) {
                    return Zone.GAMEZONE_HOCUS;
                } else {
                    return Zone.GAMEZONE_POCUS;
                }
            } else {
                return null;
            }
        }

        return null;
    }

    /**
     * Check whether or not the spellbook of the player can be filled
     * 
     * @return true or false
     */
    public boolean spellbookCanBeFilled() {
        if (this.board.getPlayer(Client.getPseudo()).getSpellbook()
                .getNumberOfCards() < 3
                && this.board.getPlayer(Client.getPseudo()).getHand()
                        .getNumberOfCards() > 0) {
            return true;
        }
        return false;
    }

    // GETTERS

    /**
     * Get board
     * 
     * @return board
     */
    public Board getBoard() {
        return this.board;
    }

    /**
     * Get gameZonePanel
     * 
     * @return gameZonePanel
     */
    public GameZonePanel getGameZonePanel() {
        return this.gameZonePanel;
    }

    /**
     * Get zoomPanel
     * 
     * @return zoomPanel
     */
    public ZoomPanel getZoomPanel() {
        return this.zoomPanel;
    }

    /**
     * Get the history panel
     * 
     * @return the historyPanel
     */
    public HistoryPanel getHistoryPanel() {
        return historyPanel;
    }

    /**
     * Set the history panel
     * 
     * @param historyPanel
     *            the historyPanel to set
     */
    public void setHistoryPanel(HistoryPanel historyPanel) {
        this.historyPanel = historyPanel;
    }

    /**
     * Get the bottomPlayerPanel
     * 
     * @return the bottomPlayerPanel
     */
    public BottomPlayerPanel getBottomPlayerPanel() {
        return bottomPlayerPanel;
    }

    /*
     * @Override public void actionPerformed(ActionEvent e) { if
     * (e.getSource()instanceof JMenuItem)// gestion des événements liés aux
     * menus { String ChoixOption = e.getActionCommand(); if
     * (ChoixOption.equals("Règles du jeu")) {
     * WindowsContainer.get("RulesWindow").setVisible(true); } if
     * (ChoixOption.equals("Aide au jeu")) {
     * WindowsContainer.get("HelpWindow").setVisible(true); } else if
     * (ChoixOption.equals("A propos de")) { JOptionPane.showMessageDialog(new
     * JFrame(),
     * "Jeu Hocus-Pocus développé par KodAp pour le projet CPE IRC 2013/2014. \n"
     * + "Version 1.0.1 \n" +
     * "Auteurs : Anaïs Payet, Vlad Marin, Bouchereau Baptiste, Jorys Gaillard, François Huynh"
     * , "A propos de", JOptionPane.INFORMATION_MESSAGE); } }
     * 
     * }
     */

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e) {
        
        if (e.getSource() instanceof TitleBarPanel){
            
            TitleBarPanel bar = (TitleBarPanel) e.getSource();
            if (bar.getTitleBar().equals("AIDE")){
                WindowsContainer.get("HelpWindow").setVisible(true);
            }
            else if (bar.getTitleBar().equals("RÈGLES DU JEU")){
                WindowsContainer.get("RulesWindow").setVisible(true);
            }
        }

    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e) {
        // TODO Auto-generated method stub

    }

}
