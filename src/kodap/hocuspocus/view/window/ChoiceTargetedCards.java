/**
 * 
 */
package kodap.hocuspocus.view.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.cards.Card;
import kodap.hocuspocus.model.cards.hocus.HocusCard;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.panel.ImagePanel;
import net.miginfocom.swing.MigLayout;

/**
 * @author Baptiste
 */
public class ChoiceTargetedCards extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel globalPanel;
    private List<Card> cardList;
    private JButton validationButton;
    private int power;
    private List<JCheckBox> checkBoxList;

    /**
     * Constructor
     * 
     * @param cardList
     * @param power
     */
    public ChoiceTargetedCards(List<Card> cardList, int power) {
        this.checkBoxList = new ArrayList<JCheckBox>();
        this.power = power;
        this.cardList = cardList;
        HocusPocusClientManager.targetedCards.clear();
        this.initVariables();
        this.initFrameComponents();
        this.initFrame();
    }

    /**
     * Initializes the frame
     */
    private void initFrame() {
        this.setTitle("Choix des cartes ciblées");
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setContentPane(this.globalPanel);

        for (Card card : this.cardList) {
            String title = "";
            try {
                title = card.toString()+" - puissance "+((HocusCard) card).getPowerNumber();
            } catch (ClassCastException e) {
                title = card.toString();
            }
            JCheckBox checkBox = new JCheckBox(title);
            checkBox.setName(Integer.toString(card.getId()));
            checkBox.setSelected(false);
            this.checkBoxList.add(checkBox);
            this.globalPanel.add(checkBox, "align center");
        }
        this.globalPanel.add(this.validationButton);
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        this.globalPanel.setLayout(new MigLayout("wrap 1"));
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.validationButton = new JButton("Valider");
        this.validationButton.addActionListener(this);
        this.globalPanel = new ImagePanel(Tools.getImageIcon("fond-player.jpg", 300, 300).getImage());
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()instanceof JButton) {
            int numberOfSelectedCards = 0;
            for (JCheckBox checkbox : this.checkBoxList) {
                if(checkbox.isSelected()) {
                    numberOfSelectedCards++;
                }
            }
            if (this.power < numberOfSelectedCards) {
                Tools.displayErrorWindow("Vous ne pouvez pas sélectionner autant de cartes");
                return;
            }
            if (this.power > numberOfSelectedCards && this.cardList.size() != numberOfSelectedCards) {
                Tools.displayErrorWindow("Vous devez sélectionner plus de cartes");
                return;
            }
            for (JCheckBox checkbox : this.checkBoxList) {
                if(checkbox.isSelected()) {
                    for (Card card : this.cardList) {
                        if (card.getId() == Integer.parseInt(checkbox.getName())) {
                            HocusPocusClientManager.targetedCards.add(card);
                        }
                    }
                }
            }
            HocusPocusClientManager.sendCardsTargetedToChat(HocusPocusClientManager.targetedCards);
            // close the JFrame
            this.dispose();
            WindowsContainer.get("BoardWindow").setEnabled(true);
        }
     }
}