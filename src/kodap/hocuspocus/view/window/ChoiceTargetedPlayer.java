/**
 * 
 */
package kodap.hocuspocus.view.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.listener.MouseCardListener;
import kodap.hocuspocus.view.panel.ImagePanel;
import net.miginfocom.swing.MigLayout;

/**
 * @author Francois
 *
 */
public class ChoiceTargetedPlayer extends JFrame implements ActionListener {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel globalPanel;

    /**
     * Constructor
     */
    public ChoiceTargetedPlayer() {
        this.initVariables();
        this.initFrameComponents();
        this.initFrame();
    }

    /**
     * Initializes the frame
     */
    private void initFrame() {
        this.setTitle("Choix du joueur ciblé");
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setContentPane(this.globalPanel);
        Board board = ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();

        for (Player player : board.getPlayersPocus()) {
            //if the pocusCard is MiroirEnchante, we ask the playerTargeted except the hocusPlayer and the pocusPlayer 
            if (MouseCardListener.MiroirEnchantePlaying){
                if (player == MouseCardListener.PocusPlayerPlaying){
                    continue;
                }
            }
            JButton jb = new JButton();
            jb.setText(player.getPseudo());
            jb.addActionListener(this);
            this.globalPanel.add(jb);
        }
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        this.globalPanel.setLayout(new MigLayout("wrap 2"));
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //Display the window.
        this.pack();
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.globalPanel = new ImagePanel(Tools.getImageIcon("fond-attack.jpg",
                300, 300).getImage());
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()instanceof JButton)
        { 
            Board board = HocusPocusClientManager.board;
            String ChoixJoueur = e.getActionCommand();
            for (Player player : board.getPlayersPocus()) {
                if (ChoixJoueur.equals(player.getPseudo())){
                    HocusPocusClientManager.sendPlayerTargetedToChat(player.getPseudo());
                    MouseCardListener.targetedPlayer = player;
                    HocusPocusClientManager.board.getEvaluatedAction().setTargetPlayer(player.getPseudo());
                    Client.sendModel(board);
                    // close the JFrame and remove it from the container
                    WindowsContainer.remove(this.getClass().getSimpleName());
                    this.dispose();
                    WindowsContainer.get("BoardWindow").setEnabled(true);
                    // if hibou or malediction, launch the ChoiceTargetedCardsWindow
                    String cardPlayed = HocusPocusClientManager.board.getCardById(HocusPocusClientManager.board.getEvaluatedAction().getCardPlayed()).toString();
                    if (cardPlayed.equals("Hibou") || cardPlayed.equals("Malediction")) {
                        if (player.getSpellbook().getCardList().isEmpty()) {
                            Tools.displayErrorWindow("Le grimoire de "+player.getPseudo()+" est vide.");
                            return;
                        }
                        HocusPocusClientManager.chooseTargetedCards(
                            player.getSpellbook().getCardList(),
                            HocusPocusClientManager.board.getEvaluatedAction().getNumber()
                        );
                    }
                }
            }
        }
     }
}
