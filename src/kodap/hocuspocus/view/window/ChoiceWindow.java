package kodap.hocuspocus.view.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.network.server.Server;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.view.panel.ImagePanel;

/**
 * The ChoiceWindow class represents the first window. Choices are
 * <ul>
 * <li>Creating a server</li>
 * <li>Joining a server</li>
 * </ul>
 * 
 * @author Baptiste <baptiste.bouchereau@gmail.com>
 * @version 1.0
 */
@SuppressWarnings("serial")
public class ChoiceWindow extends JFrame implements ActionListener {
    private JButton launchServerButton;
    private ImagePanel globalPanel;
    private JPanel picturesPanel;
    private JPanel labelAndFieldNamePanel;
    private JPanel ipPanel;
    private JButton joinServerButton;
    private JTextField nameField;
    private JTextField ipField;
    private JLabel hocusPocusBoxPicture;
    private JLabel hocusPocusCardsPicture;
    private JLabel welcomeLabelPart1;
    private JLabel welcomeLabelPart2;
    private JLabel nameLabel;
    private JLabel ou;

    /**
     * Constructor
     */
    public ChoiceWindow() {
        this.initVariables();
        this.initFrameComponents();
        this.disposeFrameComponents();
        Tools.setWindowFirstAttributes(this);
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.globalPanel =  new ImagePanel(Tools.getImageIcon("fond-global.png", 1024, 768).getImage());
        this.labelAndFieldNamePanel = new JPanel();
        this.ipPanel = new JPanel();
        this.picturesPanel = new JPanel(); 
        this.launchServerButton = new JButton();  
        this.joinServerButton = new JButton();
        
        String[] prenoms ={"Zlatan","Zidane","Bambi","Oui-oui","Babar", "Loulou", "Chouchou"};
        Random r = new Random();
        this.nameField = new JTextField(prenoms[r.nextInt(prenoms.length)]);
        this.hocusPocusBoxPicture = new JLabel();
        this.hocusPocusCardsPicture = new JLabel();
        this.welcomeLabelPart1 = new JLabel("Bienvenue au jeu magique");
        this.welcomeLabelPart2 = new JLabel("Hocus-Pocus!");
        this.ou = new JLabel("ou");
        this.nameLabel = new JLabel("Entrez votre pseudo : ");
        this.ipField = new JTextField();
    }

    /**
     * Initializes the components of the frame (setters, add action listeners,
     * etc.)
     */
    private void initFrameComponents() {
        this.setTitle("Hocus Pocus");
        this.setContentPane(this.globalPanel);
        this.globalPanel.setLayout(new MigLayout());
        
        this.labelAndFieldNamePanel.setLayout(new MigLayout());
        this.labelAndFieldNamePanel.setBackground(new Color(0,0,0,0));
        
        this.ipPanel.setLayout(new MigLayout());
        this.ipPanel.setBackground(new Color(0,0,0,0));
        
        this.picturesPanel.setLayout(new MigLayout());
        this.picturesPanel.setBackground(new Color(0,0,0,0));
        
        this.joinServerButton.addActionListener(this);
        this.launchServerButton.addActionListener(this);
        this.hocusPocusBoxPicture.setIcon(Tools.getImageIcon(
                "hocus-pocus-box.png", 300, 250));
        this.hocusPocusCardsPicture.setIcon(Tools.getImageIcon(
                "header.png", 400, 200));
        this.welcomeLabelPart1.setSize(150, 50);
        Font font = this.welcomeLabelPart1.getFont();
        Font boldFont = new Font(font.getFontName(), Font.PLAIN, font
                .deriveFont(30.0f).getSize());
        this.welcomeLabelPart1.setFont(boldFont);
        this.welcomeLabelPart2.setFont(boldFont);
        
        this.nameField.setPreferredSize(new Dimension( 222, 45 ));
        
        this.ipField.setText("127.0.0.1");
        this.ipField.setPreferredSize( new Dimension( 222, 45 ));
        
        this.ou.setFont(ou.getFont().deriveFont(20f)); 
        //this.nameLabel.setFont(ou.getFont().deriveFont(10f));
        
        this.launchServerButton.setBorder(BorderFactory.createEmptyBorder());
        this.launchServerButton.setContentAreaFilled(false);
        this.launchServerButton.setIcon(Tools.getImageIcon("btn_creer.png", 222, 50));
        
        this.joinServerButton.setBorder(BorderFactory.createEmptyBorder());
        this.joinServerButton.setContentAreaFilled(false);
        this.joinServerButton.setIcon(Tools.getImageIcon("btn_rejoindre.png", 222, 50));
    }

    /**
     * Disposes the components on the frame
     */
    private void disposeFrameComponents() {
        this.globalPanel.add(this.picturesPanel, "span");
        this.picturesPanel.add(this.hocusPocusBoxPicture,"gapleft 40");
        this.picturesPanel.add(this.hocusPocusCardsPicture,"gapright 100");
        this.globalPanel.add(this.welcomeLabelPart1, "align center,wrap,span");
        this.globalPanel.add(this.welcomeLabelPart2,
                "span, gapbottom 40, align center");
        
       // this.globalPanel.add(this.infoNameLabel,
          //      "span, gapbottom 10, align center");
        this.globalPanel.add(this.labelAndFieldNamePanel, "span");
        this.labelAndFieldNamePanel.add(this.nameLabel, "gapleft 300");
        this.labelAndFieldNamePanel.add(this.nameField, "width 250,gapbottom 30");
        
        //this.globalPanel.add(this.optionLabel,
         //       "span, align center, gaptop 15, gapbottom 10");
        this.globalPanel.add(this.launchServerButton,"gapleft 80");
        this.globalPanel.add(this.ou,"gapleft 80,gapRight 80");
        this.globalPanel.add(this.ipPanel, "span");
        
        this.ipPanel.add(this.ipField);
        this.ipPanel.add(this.joinServerButton);
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (this.nameField.getText().equals("")) {
            Tools.displayErrorWindow("Vous devez indiquer un nom de Joueur pour continuer");

            return;
        }

        if (this.nameField.getText().length() > 8) {
            Tools.displayErrorWindow("Votre nom de joueur ne doit pas dépasser 8 caractères");

            return;
        }

        if (arg0.getSource() == this.joinServerButton) {
            if (Tools.validateIp(this.ipField.getText())) {
                new Client(this.ipField.getText(), this.nameField.getText());
            } else {
                Tools.displayErrorWindow("L'adresse IP n'est pas valide");
            }
        }

        if (arg0.getSource() == this.launchServerButton) {
            try {
                Server server = new Server();
                new Client(server.getInetAddress().getHostAddress(),
                        this.nameField.getText(), true);
                this.setVisible(false);
            } catch (Exception e) {
                Tools.displayErrorWindow("Il semblerait qu'un serveur soit deja en route sur votre machine ou que le port 2013 soit occupe par une autre application.");
            }
        }
    }
}
