package kodap.hocuspocus.view.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import kodap.hocuspocus.controller.client.HocusPocusClientManager;
import kodap.hocuspocus.model.Player;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.panel.ImagePanel;

/**
 * GemOrCardsWindow
 * 
 * This window is used to let the player choose between a gem or 2 cards at the end of his turn.
 * @author baptiste
 */
public class GemOrCardsWindow extends JFrame implements ActionListener {

    private static final long serialVersionUID = -7338669528792049592L;
    private JButton gemButton;
    private JButton cardsButton;
    private JLabel descriptionLabel;
    private JPanel globalPanel;

    /**
     * Constructor
     */
    public GemOrCardsWindow() {
        this.initVariables();
        this.initFrameComponents();
        this.disposeFrameComponents();
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.cardsButton = new JButton("Piocher 2 cartes");
        this.gemButton = new JButton("Prendre une gemme");
        
        this.globalPanel = new ImagePanel(Tools.getImageIcon("fond-fintour.jpg",
                665, 400).getImage());
        this.descriptionLabel = new JLabel("Fin de votre tour");
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        this.setSize(500, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //this.getContentPane().setBackground(Color.white);
        this.setContentPane(this.globalPanel);
        
        this.getContentPane().setLayout(new MigLayout());
        this.gemButton.addActionListener(this);
        this.cardsButton.addActionListener(this);
    }

    /**
     * Disposes the components on the frame
     */
    private void disposeFrameComponents() {
        this.getContentPane().add(this.descriptionLabel, "span");
        this.getContentPane().add(this.cardsButton);
        this.getContentPane().add(this.gemButton, "wrap");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BoardWindow boardWindow = (BoardWindow) WindowsContainer.get("BoardWindow");
        Player player = boardWindow.getBoard().getPlayer(Client.getPseudo());
        int choiceNumber = -1;
        if (e.getSource() == this.gemButton) {
            choiceNumber = 1;
        } else {
            choiceNumber = 2;
        }
        HocusPocusClientManager.endOfTour(player);
        HocusPocusClientManager.pickAGemOrTwoCards(player, choiceNumber);
        this.setVisible(false);
        WindowsContainer.get("BoardWindow").setEnabled(true);
    }
}
