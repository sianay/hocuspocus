/**
 * 
 */
package kodap.hocuspocus.view.window;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import kodap.hocuspocus.tools.Tools;

/**
 * The HelpWindow class represents the windows witch we find clues and tips to play the game. 
 * 
 * @author François 
 * @version 1.0
 */
@SuppressWarnings("serial")
public class HelpWindow extends JFrame{
    private JLabel legend1, legend2, legend3,legend4, legend5, legend6, legend7, legend8;
    private JPanel globalPanel;
    private JLabel imgModel;
    private JPanel modelPanel;
    private JScrollPane scrollPane;

    /**
     * Constructor
     */
    public HelpWindow() {
        this.initVariables();
        this.initFrameComponents();
        this.disposeFrameComponents();
        this.initFrame();
        this.scrollPane = new JScrollPane(globalPanel);
        add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * Initializes the frame
     */
    private void initFrame() {
        this.setTitle("Aide au jeu");
        this.setSize(840, 600);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
    }

    /**
     * Dispose the components on the frame
     */
    private void disposeFrameComponents() {
        this.globalPanel.add(this.modelPanel, "span");
        this.modelPanel.add(this.imgModel, "span, align center");
        this.globalPanel.add(this.legend1, "span, align left");
        this.globalPanel.add(this.legend2, "span, align left");
        this.globalPanel.add(this.legend3, "span, align left");
        this.globalPanel.add(this.legend4, "span, align left");
        this.globalPanel.add(this.legend5, "span, align left");
        this.globalPanel.add(this.legend6, "span, align left");
        this.globalPanel.add(this.legend7, "span, align left");
        this.globalPanel.add(this.legend8, "span, align left");
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {    
        //this.setContentPane(this.globalPanel);
        this.globalPanel.setLayout(new MigLayout("wrap 2"));
        this.modelPanel.setBorder(BorderFactory.createEmptyBorder(0, 80, 0, 0));
        this.modelPanel.setLayout(new MigLayout());
        this.imgModel.setIcon(Tools.getImageIcon(
                "model_image.png", 600, 427));
        
        //Display the window.
        this.pack();
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.imgModel= new JLabel();
        this.modelPanel = new JPanel();
        this.globalPanel = new JPanel();
                
        this.legend1 = new JLabel(
                "<html><font color=\"#ff0000\">(1)</font> Les informations sur les autres joueurs : sous forme de lignes, on retrouvera à chaque fois, le nom du joueur, son nombre de gemmes,<br>" +
                "le nombre de cartes en main et les trois cartes visibles du grimoire. <br>" +
                "Seules les cartes du grimoire du joueur courant autorisent les clics au moment de son tour.<br></html>"
                );

        this.legend2 = new JLabel(
                "<html><font color=\"#ff0000\">(2)</font> On retrouve la main du joueur courant. Les cartes du joueur ne sont visibles que par lui et peuvent être utilisées, si <br>" +
                "les conditions le permettent, en cliquant dessus.<br> </html>");
        this.legend3 = new JLabel(
                "<html><font color=\"#ff0000\">(3)</font> À droite de la main du joueur, on trouve les deux boutons d’actions. Lorsqu’une carte Hocus est jouée, chacun des joueurs à un <br>" +
                " temps pour décider de jouer une carte Pocus. <br> Pour éviter d’attendre, un bouton permet d’indiquer que l’on ne souhaite pas jouer de carte de ce type.<br> " +
                "Le second bouton est utile lors du tour du joueur courant, afin d’indiquer qu’il souhaite finir son tour.<br> </html>"
                );
        this.legend4 = new JLabel(
                "<html><font color=\"#ff0000\">(4)</font> Sur la partie de droite, tout en haut, une zone indique le nombre de gemmes restantes dans le chaudron.<br> </html>");
        this.legend5 = new JLabel(
                "<html><font color=\"#ff0000\">(5)</font> La pioche dite Bibliothèque est représentée.<br></html>");
        this.legend6 = new JLabel(
                "<html><font color=\"#ff0000\">(6)</font> En haut à droite, une zone de zoom est là pour permettre d’afficher n’importe quelle carte visible par le joueur en plus grand<br>" +
                ". Pour ce faire, il lui suffit de faire un clic-droit sur la carte qu’il souhaite afficher.<br></html>");
        this.legend7 = new JLabel(
                "<html><font color=\"#ff0000\">(7)</font> La zone de jeu affiche les cartes qui sont jouées.<br> " +
                "Pour simplifier, seule la carte Hocus et la dernière carte Pocus jouée sont affichées.<br></html>");
        this.legend8 = new JLabel(
                "<html><font color=\"#ff0000\">(8)</font> Pour faciliter la compréhension du jeu, une zone de texte, située en bas à droite de l’écran reprend toutes les actions jouées<br>" +
                "Elle spécifie notamment quelle est la personne visée par la carte jouée. Une boite de dialogue sert à certaines interactions de pouvoir de carte<br> par exemple" +
                "pour spécifier le nom d’un joueur visé.<br></html>");        
    }

    /**
     * @return the globalPanel
     */
    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    /**
     * @param globalPanel the globalPanel to set
     */
    public void setGlobalPanel(JPanel globalPanel) {
        this.globalPanel = globalPanel;
    }

    /**
     * @return the imgModel
     */
    public JLabel getImgModel() {
        return imgModel;
    }

    /**
     * @param imgModel the imgModel to set
     */
    public void setImgModel(JLabel imgModel) {
        this.imgModel = imgModel;
    }

    /**
     * @return the modelPanel
     */
    public JPanel getModelPanel() {
        return modelPanel;
    }

    /**
     * @param modelPanel the modelPanel to set
     */
    public void setModelPanel(JPanel modelPanel) {
        this.modelPanel = modelPanel;
    }
}
