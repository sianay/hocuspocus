package kodap.hocuspocus.view.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.controller.server.HocusPocusServerManager;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.network.server.Server;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.view.panel.ImagePanel;

/**
 * Work in progress......
 */
@SuppressWarnings("serial")
public class LaunchGameWindow extends JFrame implements ActionListener {
    private ImagePanel panel;
    private JTable table;
    private DefaultTableModel tableModel;
    private JScrollPane clientsPane;
    private JButton launchButton;

    /**
     * Constructor
     */
    public LaunchGameWindow() {
        this.initVariables();
        this.initFrameComponents();
        Tools.setWindowFirstAttributes(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.launchButton) {
            int playerNumber = Server.getClientPseudoList().size();
            if ( playerNumber > 4 || playerNumber < 2) {
                Tools.displayErrorWindow("Il faut entre 2 et 4 joueurs pour lancer une partie"); return;
            }
            // Server.sendMessageToAll("La partie est lancee le serveur n'accepte plus de connexions",
            // MessageType.INFO);
            Server.connectionListener.close();
            new HocusPocusServerManager(Server.getClientPseudoList().size(), Server.getClientPseudoList());
            Server.sendModelToAll(HocusPocusServerManager.board);
        }
    }

    /**
     * Initialize variables
     */
    private void initVariables() {
        Vector<String> col = new Vector<String>();
        col.add("Joueurs en lice");
        Vector<Vector<String>> data = new Vector<Vector<String>>();

        this.tableModel = new DefaultTableModel(data, col);
        this.panel = new ImagePanel(Tools.getImageIcon("fond-global.png", 1024, 768).getImage());
        this.table = new JTable(tableModel);
        this.clientsPane = new JScrollPane(this.table);
        this.launchButton = new JButton();
        this.launchButton.setBorder(BorderFactory.createEmptyBorder());
        this.launchButton.setContentAreaFilled(false);
        this.launchButton.setIcon(Tools.getImageIcon("btn_lancer.png", 222, 50));
        this.launchButton.addActionListener(this);
    }

    /**
     * Initialize the components of the frame (set components on the frame, add
     * action listeners, etc.)
     */
    private void initFrameComponents() {
       
        this.setTitle("Hocus Pocus - Lancement du jeu - " + Client.getPseudo());
        this.setContentPane(this.panel);
        this.panel.setLayout(new MigLayout("center"));
        this.panel.add(this.clientsPane);
        if (Client.isHost) {
            this.panel.add(this.launchButton,"top");
        }
        
    }

    /**
     * Get the table model of the window
     * 
     * @return DefaultTableModel
     */
    public DefaultTableModel getTableModel() {
        return tableModel;
    }

    /**
     * Set the table model of the window
     * 
     * @param tableModel
     *            DefaultTableModel
     */
    public void setTableModel(DefaultTableModel tableModel) {
        this.tableModel = tableModel;
    }
}
