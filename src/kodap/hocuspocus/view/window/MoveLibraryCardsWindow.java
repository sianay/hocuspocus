package kodap.hocuspocus.view.window;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import kodap.hocuspocus.model.Board;
import kodap.hocuspocus.network.client.Client;
import kodap.hocuspocus.tools.Tools;
import kodap.hocuspocus.tools.WindowsContainer;
import kodap.hocuspocus.view.CardLabel;
import kodap.hocuspocus.view.listener.LibraryMouseCardListener;
import kodap.hocuspocus.view.panel.CardPanel;
import kodap.hocuspocus.view.panel.GlassCardPane;

/**
 * MoveLibraryCardsWindow
 * 
 * @author baptiste
 */
public class MoveLibraryCardsWindow extends JFrame implements ActionListener {

    private static final long serialVersionUID = -8590405352956477306L;
    private JPanel cardsPanel;
    private List<CardPanel> libraryTopCardsPanel;
    private JButton validationButton;
    private JLabel descriptionLabel;
    private JLabel explanationLabel;

    /**
     * Card representation for DragNDrop
     */
    public static GlassCardPane glass = new GlassCardPane();

    /**
     * Constructor
     */
    public MoveLibraryCardsWindow() {
        this.initVariables();
        this.initFrameComponents();
        this.disposeFrameComponents();
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.cardsPanel = new JPanel();
        this.validationButton = new JButton("Valider");
        this.descriptionLabel = new JLabel("REORDONNER LES CARTES");
        this.explanationLabel = new JLabel("<html>Les cartes les plus à gauche sont les cartes<br /> les plus en haut de la pioche</html>");
        this.libraryTopCardsPanel = new ArrayList<CardPanel>();
        for (int i = 0; i<4; i++) {
            Board board =  ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();
            CardLabel cardLabel = new CardLabel(board.getLibrary().removeTopCard(), this);
            Tools.removeMouseListeners(cardLabel);
            cardLabel.addMouseListener(new LibraryMouseCardListener(this));
            this.libraryTopCardsPanel.add(i, new CardPanel(cardLabel));
        }
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        this.setSize(500, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
        this.getContentPane().setLayout(new MigLayout());
        this.validationButton.addActionListener(this);
        this.cardsPanel.setLayout(new MigLayout());
    }

    /**
     * Disposes the components on the frame
     */
    private void disposeFrameComponents() {
        setGlassPane(glass);
        this.getContentPane().add(this.descriptionLabel, "span");
        this.getContentPane().add(this.explanationLabel, "span");
        this.getContentPane().add(this.cardsPanel, "span, width 100%");
        for (CardPanel cardPanel : this.libraryTopCardsPanel) {
            this.cardsPanel.add(cardPanel);
        }
        this.getContentPane().add(this.validationButton, "span, align center");
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // update the board with the new top cards in the library
        Board board =  ((BoardWindow) WindowsContainer.get("BoardWindow")).getBoard();
        int i;
        for (i = 3; i >= 0; i--) {
            board.getLibrary().addCard(this.libraryTopCardsPanel.get(i).getCardLabel().getCard(), board.getLibrary().getNumberOfCards());
        }
       
        Client.sendModel(board); 

        // close the JFrame and remove it from the container
        WindowsContainer.remove(this.getClass().getSimpleName());
        this.dispose();
        WindowsContainer.get("BoardWindow").setEnabled(true);
    }
}
