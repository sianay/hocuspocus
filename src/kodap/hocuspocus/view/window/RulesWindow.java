/**
 * 
 */
package kodap.hocuspocus.view.window;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import kodap.hocuspocus.tools.Tools;
import net.miginfocom.swing.MigLayout;

/**
 * The RulesWindow class represents the windows witch we find the rules. 
 *
 * 
 * @author François 
 * @version 1.0
 */
@SuppressWarnings("serial")
public class RulesWindow extends JFrame{
    private JLabel imgRules1, imgRules2, imgRules3;
    private JPanel globalPanel;
    private JScrollPane scrollPane;
    
    /**
     * Constructor
     */
    public RulesWindow() {
        this.initVariables();
        this.initFrameComponents();
        this.disposeFrameComponents();
        this.initFrame();
        this.scrollPane = new JScrollPane(globalPanel);
        add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * Initializes the frame
     */
    private void initFrame() {
        this.setTitle("Règles du jeu");
        this.setSize(660, 650);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.getContentPane().setBackground(Color.white);
    }

    /**
     * Dispose the components on the frame
     */
    private void disposeFrameComponents() {
        this.globalPanel.add(this.imgRules1, "span, align center");
        this.globalPanel.add(this.imgRules2, "span, align center");
        this.globalPanel.add(this.imgRules3, "span, align center");
    }

    /**
     * Initializes frame components
     */
    private void initFrameComponents() {
        this.globalPanel.setLayout(new MigLayout("wrap 2"));
        this.imgRules1.setIcon(Tools.getImageIcon(
                "regles1.png", 607, 579));
        this.imgRules2.setIcon(Tools.getImageIcon(
                "regles2.png", 607, 579));
        this.imgRules3.setIcon(Tools.getImageIcon(
                "regles3.png", 607, 579));
        //Display the window.
        this.pack();
    }

    /**
     * Initializes variables
     */
    private void initVariables() {
        this.imgRules1= new JLabel();
        this.imgRules2= new JLabel();
        this.imgRules3= new JLabel();
        this.globalPanel = new JPanel();
    }
}